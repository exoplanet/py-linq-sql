# Changelog

<!-- markdownlint-disable-file MD024 -->

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.11.0-pre-release] - 2024-23-01

### Added

- Add `least()` functions on `MagicDotPath`
- Add `greatest()` functions on `MagicDotPath`

## [1.10.0-pre-release] - 2023-15-12

## Fixed

- Now join work with table in a schema of database.

## [1.9.3-pre-release] - 2023-30-10

### Fixed

- Fix config documentation, config file name was wrong it's not
  `.pylinqsql-config.[toml|yaml]` but `.pylinqsql-conf.[toml|yaml]`.
- Fix where with boolean.

## [1.9.2-pre-release] - 2023-07-05

### Fixed

- Fix request on table in schema other then public for join and group join.

## [1.9.1-pre-release] - 2023-07-05

### Fixed

- Fix request on table in schema other then public. Like `SELECT name FROM schema.table`.

## [1.9.0-pre-release] - 2023-07-05

### Changed

- Update dependencies.

## [1.8.3-pre-release] - 2023-03-30

### Fixed

- Fix a dependencies `strenum` for python3.10.

## [1.8.2-pre-release] - 2023-03-28

### Fixed

- Fix `SQLEnumerable().where()` when the second operand is a string,
in json when we have only one level of nesting.

```python
# Shape of the lambdas where the bug appeared
lambda x: x.data.name == "toto"
```

- Fix name of return column in dict in group_join with aggregate function.

## [1.8.1-pre-release] - 2023-02-15

### Added

- Add a method to `SQLEnumerable`, `simple_insert()` to make quick insertion in
  relational table.
- Add function `is_in` to be able to make `WHERE toto IN (titi, toto, tutu)`.
- Add `__getitem__` method to `MagicDotPath` to have access to attribute like a dict.

### Changed

- New logo and a new banner for the project.

### Fixed

- Fix typing in `BaseMagicDotPath._get_generic_operator()` and all operator methods.
It fix the mypy errors: no-any-return when wwe use lambda function with an operator and
2 MagicDotPath operand.

## [1.7.0-pre-release] - 2023-01-20

### Added

- Add a `__getattr__` method to `BaseMagicDotPath`. It can fix the mypy errors:
attr-defined and no-any-return when we use lambda function in `MagicDotPath`.
- Add documentation for string functions: `lower()`, `upper()`, `title()`.

### Changed

- Update ruff and refactor `__init__.py`

## [1.6.1-pre-release] - 2022-12-20

### Added

- Add `lower()`, `upper()` and `title()` functions on `MagicDotPath`
- Add ruff for code qualities.

### Changed

- Use ruff instead pydocstyle, flake8 and pylint

### Fixed

- Fix doctest of `_col_name_validator()` in
  'py_linq_sql.utils.functions.other_functions.py'

## [1.4.1-pre-release] - 2022-11-22

### Fixed

- Add the TypeAlias: `PyLinqSqlInsertType` in the file `__init__.py` to import it.
- Fix requirement in `doc/requirement.txt` for psycopg (3.1.14 to 3.1.4, typo).

## [1.4.0-pre-release] - 2022-11-18

### Added

- Add a TypeAlias for data in `SQLEnumerable.insert()`.

### Changed

- Improve conftest files with sql file to initialize all database for tests.
- Adapt the code to match new error of mypy.
- Update pytest, pytest-sugar, mypy, psycopg-pool and tmp-connection-psql.

## [1.3.0-pre-release] - 2022-11-08

### Added

- py-linq-sql now support python3.11.
- Add some dependencies for supporting python3.11:
  - six
  - wrapt
  - strenum
- Add new linter pep8-naming, following remove flakehell which is no longer maintained.

### Changed

|Name                | Old version  | New version |
|:-------------------|:------------:|:-----------:|
|dotmap              | 1.3.26       | 1.3.30      |
|psycopg             | 3.0.11       | 3.1.4       |
|types-toml          | 0.10.7       | 0.10.8      |
|types-PyYaml        | 6.0.9        | 6.0.12.1    |
|pytest-sugar        | 0.9.4        | 0.9.5       |
|pytest-cov          | 3.0.0        | 4.0.0       |
|pre-commit          | 2.17.0       | 2.20.2      |
|black               | 22.1.0       | 22.10.0     |
|xdoctest            | 0.15.10      | 1.1.0       |
|codepsell           | 2.1.0        | 2.2.2       |
|dlint               | 0.12.0       | 0.13.0      |
|import-linter       | 1.2.7        | 1.4.0       |
|mypy                | 0.942        | 0.982       |
|sqlfluff            | 0.12.0       | 1.4.1       |
|pylint              | 2.13.7       | 2.15.5      |
|plerr               | 2.0.0        | 3.0.0       |
|flake8              | 3.9          | 5.0.4       |
|flake8-bandit       | 3.0.0        | 4.1.1       |
|flake8-eradicate    | 1.2.0        | 1.4.0       |
|tmp-connection-psql | 1.0.1-beta.0 | 1.1.0       |
|psycopg-pool        | 3.1.1        | 3.1.3       |
|numpy               | 1.21.6       | 1.23.4      |

### Remove

- Remove some depandencies:
  - flakehell
  - flake8-quotes
  - flit
  - m2r2
  - dephell

## [1.2.0-pre-release] - 2022-10-10

### Added

- Add a way to insert 'Null' in table with None.

## [1.1.1-pre-release] - 2022-10-04

### Added

- Add a way to insert data in a table which contains relational data and json.

### Fixed

- Fix a bug in relational insert which prevented inserting a arrays.

## [1.0.0-pre-release] - 2022-08-11

### Added

- Add some test and clear the code for a full coverage (100%)!!!
- Add a gitlab ci files to run precommit, pytest coverage and pytest.
- Add readonly in the config.
- Add support for config files to have a white list and a black list.
- Add a detection for the Length mismatch between select lambda in `intersect`,
`union` and `except_`.
- Add a validator for column names.
- Add the support for dict in lambda to support renaming of columns.
- Add the possibility to have the same column name.
- Add right operator for:
  - add
  - sub
  - mul
  - trudive
  - mod
  - pow
  - and
  - or
- Add hyperbolic functions for MagicDotPath:
  - acosh
  - asinh
  - atanh
  - cosh
  - sinh
  - tanh
- Add trigonometric functions for MagicDotPath:
  - acos
  - acosd
  - asin
  - asind
  - atan
  - atand
  - atan2
  - atan2d
  - cos
  - cosd
  - cot
  - cotd
  - sin
  - sind
  - tan
  - tand
- Add more mathematical functions for MagicDotPath:
  - ceil
  - degrees
  - floor
  - gcd
  - lcm
  - exp
  - ln
  - log10
  - log
  - min_scale
  - radians
  - round
  - scale
  - sign
  - trim_scale
  - trunc
- Add Markdonlint for the project.
- Add `-` unary operator for MagicDotPath.
- Add `+` unary operator for MagicDotPath.
- Add `**` (puissance) operator for MagicDotPath.
- Add `sqrt` (square root) operator for MagicDotPath.
- Add `cbrt` (cube root) operator for MagicDotPath.
- Add `abs` (absolute value) operator for MagicDotPath.
- Add `&` (and) operator for MagicDotPath.
- Add `|` (or) operator for MagicDotPath.
- Add `~` (not) operator for MagicDotPath.
- Add `*` operator for MagicDotPath.
- Add `/` operator for MagicDotPath.
- Add `%` operator for MagicDotPath.
- Add `+` operator for MagicDotPath.
- Add `-` operator for MagicDotPath.
- Add the `group_join()` function to generate a GROUP_JOIN subcommand.
- Add the possibility to make other command after a join.
- Add the `delete()` function to generate a DELETE subcommand.
- Add the possibility to make a request from another request.
- Add `pretty_print()` function to get a pretty output in terminal with rich.
- Add the `group_by()` function to generate a GROUP_BY subcommand.
- Add `sum()`, `min()`, `max()`, `avg()`, `count()`, `concat()` aggregate functions for `group_by()`.
- Add a new linter: flakehell
- Add a new linter: pylint
- Add the `distinct()` function to generate a DISTINCT subcommand.
- Add the `intersect()` function to generate a INTERSECT subcommand.
- Add the `union()` function to generate a UNION subcommand.
- Add execution for `join()` (and tests of the execution).
- Add a new linter: SQLfluff.
- Add an Error if we try to reuse an SQL_Enumerable.
- Add eq, ne and copy in SQLEnumerable.
- Add eq, ne [operator](https://docs.python.org/fr/3/library/operator.html) in Command
  data class.
- Add the `join()` function to generate an JOIN subcommand.
- Add the `contains()` function to know if a table contains an element or validates a predicate.
- Add the `except_()` function to generate an EXCEPT subcommand.
- Add the `any()` function to know if at least one element in table verify the predicate.
- Add the `all()` function to know if all element in table verify the predicate.
- Add the `skip_last()` function to generate a SKIP LAST subcommand.
- Add the `take_last()` function to generate a TAKE LAST subcommand.
- Add the `last_or_default()` function to generate a LAST subcommand but if the request
  return None we return an error and don't raise an Exception.
- Add the `last()` function to generate a LAST subcommand.
- Add the `single_or_default()` function to verify if the request return exactly one
  objects but if the record is empty return None.
- Add the `single()` function to verify if the request return exactly one objects.
- Add a function the detect implicit `and` in where condition and replace all `WHERE`
  (except the first) by a `AND` in the request.
- Add the `first_or_default()` function to execute a LIMIT 1 OFFSET 0 subcommand of
  request but if the request return None we return an error and don't raise an
  Exception.
- Add the `element_at_or_default()` function to execute a LIMIT 1 OFFSET 'n' subcommand
  of request but if the request return None we return an error and don't raise an
  Exception.
- Add the `count()` function to execute a COUNT(*) subcommand of request.
- Add the `update()` function to execute a UPDATE subcommand of request.
- Add the `first()` function to execute a LIMIT 1 OFFSET 0 subcommand of request (Its
  equal to `element_at(0)`).
- Add the `element_at()` function to execute a LIMIT 1 OFFSET 'n' subcommand of request.
- Add a real README with a little documentation.
- Add the `skip()` function to execute a OFFSET subcommand of request.
- Add the `take()` function to execute a LIMIT [OFFSET] subcommand of request.
- Add the `order_by_descending()` function to generate the ORDER_BY [predicate] DESC
  subcommand of a request.
- Add the `order_by()` function to generate the ORDER_BY [predicate] ASC subcommand
  of a request.
- Add the `insert()` function to generate the INSERT INTO subcommand of a request.
- Add test for the already implemented functions
- Add the `execute()` function to execute a command of a Request objects.
- Add the `min()` function to generate the MIN subcommand of a request.
- Add the `max()` function to generate the MAX subcommand of a request.
- Add the `where()` function to generate the WHERE subcommand of a request.
- Add the `select()` function to generate the SELECT subcommand of a request.
- Add operator function (gt, lt, ge, le, eq, ne) to get the correct operator for request
  like : name = "toto" or mass > 50.
- Add some useful classes `Request`, `_Command`, `_CommandType` and `MagicDotPath`
  to construct the request command.
- Add functions to connect to a database.
