# Contributing to py-linq-sql

**First thanks for contributing.**

The following explains how to contribute to the project.

## Table of Content

- [Contributing to py-linq-sql](#contributing-to-py-linq-sql)
  - [Table of Content](#table-of-content)
  - [What do you need to install py-linq-sql on your computer](#what-do-you-need-to-install-py-linq-sql-on-your-computer)
    - [What version of python](#what-version-of-python)
    - [Dependencies](#dependencies)
      - [Install procedure for the dependencies](#install-procedure-for-the-dependencies)
      - [Install procedure for development dependencies](#install-procedure-for-development-dependencies)
  - [Project structure](#project-structure)
  - [How to](#how-to)
    - [How to use tests](#how-to-use-tests)
    - [How to report a bug](#how-to-report-a-bug)
    - [How to submit changes or enhancements](#how-to-submit-changes-or-enhancements)
    - [How to Merge Request](#how-to-merge-request)
  - [Pre-commit](#pre-commit)
  - [Style Guide](#style-guide)
    - [Formatter](#formatter)
      - [Black](#black)
      - [Isort](#isort)
    - [Linter](#linter)
      - [Pydocstyle](#pydocstyle)
      - [Mypy](#mypy)
      - [Pylint](#pylint)
      - [Flake8](#flake8)
      - [Flakehell](#flakehell)
  - [Contacts](#contacts)

## What do you need to install py-linq-sql on your computer

### What version of python

py-linq-sql use the python version 3.10. To install this version use:

```bash
sudo apt install python3.10
```

Verify the version.

```bash
$ python3 --version
Python 3.10.0
```

### Dependencies

py-linq-sql use some dependencies:

- [rich](https://rich.readthedocs.io/en/stable/index.html)
- [py-linq](https://viralogic.github.io/py-enumerable/)
- [six](https://github.com/benjaminp/six)
- [dotmap](https://github.com/drgrib/dotmap)
- [psycopg](https://www.psycopg.org/psycopg3/docs/)
- [toml](https://github.com/uiri/toml)
- [types-toml](https://github.com/uiri/toml)
- [PyYAML](https://pyyaml.org/wiki/PyYAMLDocumentation)
- [types-PyYaml](https://pyyaml.org/wiki/PyYAMLDocumentation)

and some dependencies for the development:

- [pytest](https://docs.pytest.org/en/7.1.x/)
- [pytest-sugar](https://github.com/Teemu/pytest-sugar/)
- [pytest-pudb](https://github.com/wronglink/pytest-pudb)
- [pytest-postgresql](https://github.com/ClearcodeHQ/pytest-postgresql)
- [pytest-cov](https://github.com/pytest-dev/pytest-cov)
- [xdoctest](https://xdoctest.readthedocs.io/en/latest/autoapi/xdoctest/index.html)
- [tmp-connection-psql](https://gitlab.obspm.fr/uchosson/tmp_connection_psql)
- [psycopg-pool](https://pypi.org/project/psycopg-pool/)
- [mpmath](https://mpmath.org/)
- [numpy](https://numpy.org/)
- [assertpy](https://github.com/assertpy/assertpy)
- [strenum](https://github.com/irgeek/StrEnum)
- [pre-commit](https://pre-commit.com/)
- [black](https://github.com/psf/black)
- [isort](https://pycqa.github.io/isort/)
- [mypy](https://mypy.readthedocs.io/en/stable/)
- [ruff](https://github.com/charliermarsh/ruff)
- [sniffio](https://github.com/python-trio/sniffio)
- [sqlfluff](https://www.sqlfluff.com/)
- [dlint](https://github.com/dlint-py/dlint)
- [bandit](https://bandit.readthedocs.io/en/latest/)
- [import-linter](https://import-linter.readthedocs.io/en/stable/)
- [codespell](https://github.com/codespell-project/codespell/)
- [mkdocs](https://www.mkdocs.org/)
- [mkdocstrings](https://mkdocstrings.github.io/)
- [Pygments](https://pygments.org/)
- [mkdocs-autorefs](https://mkdocstrings.github.io/autorefs/)
- [mdx_truly_sane_lists](https://github.com/radude/mdx_truly_sane_lists)

#### Install procedure for the dependencies

You only need the classic dependencies to use py-linq-sql, they are all available on Pypi.

*To install the dependencies you need [poetry](https://python-poetry.org/) and [just](https://github.com/casey/just)*

```bash
just install
```

Or you can directly use poetry:

```bash
poetry install --no-dev --remove-untracked
```

#### Install procedure for development dependencies

You need to install all this package to contribute to the project,
they are all available on Pypi.

*To install the dependencies you need [poetry](https://python-poetry.org/) and [just](https://github.com/casey/just)*

```bash
just install-all
```

Or you can directly use poetry (and npm for markdownlint)

```bash
poetry install --remove-untracked
sudo npm install
sudo npm install markdownlint-cli2 --global
```

## Project structure

py-linq-sql is structured with 2 principal folders;

- py-linq-sql
- tests

*py-linq-sql* contains all the code of the library separate into different folders
to find your way around the code.

*tests* contains all tests for the library (more than 1200).

## How to

### How to use tests

To launch test you can use the alias just:

```bash
just pytest
```

If all goes well you should have something like this:

![Tests](https://py-linq-sql.readthedocs.io/en/latest/doc/contribute/img/test.png)

### How to report a bug

This section guides you through submitting a bug report for py-linq-sql.
Before reporting check the [issues](https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues)
and [CHANGELOG.md](CHANGELOG.md) as you might find out that you don't need to create one.
If there is no trace of this bug create a issue [here](https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues)
following the [template](https://py-linq-sql.readthedocs.io/en/latest/doc/contribute/templates/issues/bug_report/),
**please include as many details as possible**.

### How to submit changes or enhancements

This section guides you through submitting changes or enhancements for py-linq-sql.
Before proposing changes or enhancements check the [issues](https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues)
and [CHANGELOG.md](CHANGELOG.md) as you might find out that you don't need to create one.
If the enhancement or change has never been proposed create a issue [here](https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues)
following the [template](https://py-linq-sql.readthedocs.io/en/latest/doc/contribute/templates/issues/feature/),
**please include as many details as possible**.

### How to Merge Request

This section guides you to summit a merge request for py-linq-sql.
Before create a merge request make sure you respond to an [issue](https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues)
or have opened one.
Please follow these steps to have your contribution considered by the maintainers:

1. Follow all instructions in the [template](https://py-linq-sql.readthedocs.io/en/latest/doc/contribute/templates/merge_request/merge_request/)
2. Follow the [style guides](#style-guide)

## Pre-commit

py-linq-sql use pre-commit before commit and merge.
Pre-commit is a git hook scripts are useful for identifying simple issues before
submission to code review. You must install pre-commit before commit on the git.

```bash
just preinstall
```

## Style Guide

py-linq-sql use some formatter and linter to improve understanding of the code by other developers.

### Formatter

You can run all formatter on all code and test files with:

```bash
just onmy31
```

#### Black

Black is a formatter to save time and mental energy for more important matters.
Black is run by pre-commit but you can run it manually.

```bash
black #filename
```

[black documentation](https://github.com/psf/black)

#### Isort

Isort is a formatter to sort imports alphabetically,
and automatically separated into sections and by type.
Isort is run by pre-commit but you can run it manually.

```bash
isort #filename
```

[isort documentation](https://pycqa.github.io/isort/)

### Linter

You can run all linter on code with:

```bash
just lint
```

#### Pydocstyle

Pydocstyle is a linter for checking compliance with Python docstring conventions.
Pydocstyle is run by pre-commit but you can run it manually.

```bash
just pydocstyle #path
```

or with just on all code with:

```bash
just pydocstyle
```

[Pydocstyle documentation](http://www.pydocstyle.org/en/stable/)

#### Mypy

Mypy is a static type checker for Python.
Mypy is run by pre-commit but you can run it manually.

```bash
just mypy
```

[Mypy documentation](https://mypy.readthedocs.io/en/stable/)

#### Pylint

Pylint is a static code analyse for Python.
Pylint is run by pre-commit but you can run it manually.

```bash
just pylint #path
```

or with just on all code with:

```bash
just pylint
```

[Pylint documentation](https://pylint.pycqa.org/en/latest/index.html)

For more information on pylint errors you can use [plerr](https://github.com/vald-phoenix/pylint-errors)
with error code.

```bash
plerr W013
```

#### Flake8

Flake8 us a wrapper of PyFlakes, pycodestyle and 'Ned Batchelder’s McCabe script'.
Flake8 is run by pre-commit.

[Flake8 documentation](https://flake8.pycqa.org/en/latest/index.html#)

#### Flakehell

Flakehell is an evolved version of flake8.
Flakehell is run by pre-commit but you can run it manually.

```bash
just flakehell #path
```

or with just on all code with:

```bash
just flakhell
```

[Flakehell documentation](https://flakehell.readthedocs.io/)

## Contacts

- Author: Ulysse CHOSSON (LESIA)
- Maintainer: Ulysse CHOSSON (LESIA)
- Email: <ulysse.chosson@obspm.fr>
- Contributors:
  - Pierre-Yves MARTIN (LESIA)
