# Build consult requests

## Selection requests

::: py_linq_sql.build_request.consult

## Context requests

::: py_linq_sql.build_request.consult_context

## Terminal requests

::: py_linq_sql.build_request.consult_terminal
