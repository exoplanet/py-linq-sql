# MagicDotPath classes

## BaseMagicDotPath

::: py_linq_sql.utils.classes.magicdotpath.BaseMagicDotPath

## MagicDotPath

::: py_linq_sql.utils.classes.magicdotpath.MagicDotPath

## MagicDotPathWithOp

::: py_linq_sql.utils.classes.magicdotpath.MagicDotPathWithOp

## MagicDotPathAggregate

::: py_linq_sql.utils.classes.magicdotpath.MagicDotPathAggregate
