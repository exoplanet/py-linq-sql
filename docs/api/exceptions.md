# Py-linq-sql Exceptions

::: py_linq_sql.exception.exception
    options:
        members_order: "source"

## Structure of Exceptions

```doc
.
├── Warning
|   └── ReturnEmptyEnumerable
├── PyLinqSQLError
|   ├── PSQLConnectionError
|   ├── ConfigPermissionError
|   |   ├── TablePermissionDeniedError
|   |   └── ReadOnlyPermissionDeniedError
|   ├── InputError
|   |   ├── ColumnNameError
|   |   ├── NegativeNumberError
|   |   ├── TableError
|   |   └── TypeOperatorError
|   ├── PreExecutionError
|   |   ├── ActionError
|   |   ├── EmptyInputError
|   |   └── EmptyRecordError
|   ├── BuildError
|   |   ├── DeleteError
|   |   ├── LenghtMismatchError
|   |   ├── NeedWhereError
|   |   └── TooManyReturnValueError
|   ├── ExecutionError
|   |   ├── CursorCloseError
|   |   ├── EmptyQueryError
|   |   ├── DatabError
|   |   └── FetchError
|   ├── LegalityError
|   |   ├── AlreadyExecutedError
|   |   ├── AlterError
|   |   ├── GroupByWithJoinError
|   |   ├── MoreThanZeroError
|   |   ├── NeedSelectError
|   |   ├── NoMaxOrMinAfterLimitOffsetError
|   |   ├── OneError
|   |   ├── OtherThanWhereError
|   |   ├── SelectError
|   |   └── TerminalError
|   └── UnknownCommandTypeError
```
