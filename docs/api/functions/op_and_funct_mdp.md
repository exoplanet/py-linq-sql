# Operators and functions for MagicDotPath

::: py_linq_sql.utils.classes.op_and_func_of_mdp
    options:
        members:
            - col_name_hyper
            - col_name_math
            - col_name_trigo
            - col_name_ope
            - col_name_str
            - json_path_hyper
            - json_path_math
            - json_path_trigo
            - json_path_ope
            - json_path_str
