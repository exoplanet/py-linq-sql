# SQLEnumerable alteration methods

::: py_linq_sql.sql_enumerable.sql_enumerable.SQLEnumerable
    options:
        members:
            - insert
            - update
            - delete
