# SQLEnumerable consultation methods

::: py_linq_sql.sql_enumerable.sql_enumerable.SQLEnumerable
    options:
        members:
            - select
            - where
            - all
            - any
            - contains
            - distinct
            - order_by
            - order_by_descending
            - count
            - max
            - min
            - single
            - single_or_default
            - first
            - first_or_default
            - last
            - last_or_default
            - element_at
            - element_at_or_default
            - take
            - take_last
            - skip
            - skip_last
            - except_
            - union
            - intersect
            - join
            - group_by
            - group_join
