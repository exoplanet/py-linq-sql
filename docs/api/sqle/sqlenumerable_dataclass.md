# SQLEnumerable data class

::: py_linq_sql.sql_enumerable.sql_enumerable.SQLEnumerable
    options:
        members:
            - __eq__
            - __ne__
            - __post_init__
            - get_command
            - execute
