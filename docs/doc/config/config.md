# The py-linq-sql configuration file

py-linq-sql supports three different configuration files:

- `.pylinqsql-conf.toml`
- `.pylinqsql-conf.yaml`
- `pyproject.toml`

The configuration file is load the first time you create an `SQLEnumerable`
in this priority order:

- toml config file
- yaml config file
- pyprojects

## Read only

The config file allows you to specify if you want or not make alteration commands.
By default, `readonly` configuration is `false` but you can pass it to `true` in the
config file to prohibit the alteration of tables.

You can see examples of this config [here](#examples-of-configuration-files).

## Black and white list

The config file allows you to specify a black list and a white list.
Those lists govern the tables to which you have access or not.
The black and white lists are optional if you do not want to specify one or both this
is possible.
If a list isn't specified this list will be equal to `None` by default.

If you haven't a config file (or no config section for py-linq-sql in pyprojects.toml)
by default black and white lists will be equal to `None`.

You can see examples of those list [here](#examples-of-configuration-files).

The rules for black and white lists are
**blacklist always has priority over the whitelist**.
Some specific cases are summarized in the table below.

*'/' means: no matter who you are.* <!-- markdownlint-disable MD036 -->

| Who I am | Whitelist | Blacklist | Results |
|:--------:|:----------|:----------|:--------|
| **/**    | None      | None      | True    |
| **/**    | None      | []        | True    |
| **/**    | []        | None      | False   |
| **/**    | []        | []        | False   |
| **/**    | []        | [a, b, c] | False   |
| **a**    | [a, b, c] | None      | True    |
| **d**    | [a, b, c] | None      | False   |
| **a**    | None      | [a, b, c] | False   |
| **d**    | None      | [a, b, c] | True    |
| **a**    | [a, b, c] | []        | True    |
| **d**    | [a, b, c] | []        | False   |
| **a**    | [a, b, c] | [a, b, c] | False   |
| **d**    | [a, b, c] | [a, b, c] | False   |
| **a**    | [a, b, c] | [b, c]    | True    |
| **b**    | [a, b, c] | [b, c]    | False   |
| **a**    | [b, c]    | [a, b, c] | False   |
| **b**    | [b, c]    | [a, b, c] | False   |

## Examples of configuration files

### Toml configuration file

.pylinqsql-conf.toml

```toml
[pylinqsql]
whitelist = ["toto", "titi"]
blacklist = ["tata", "tutu"]
readonly = true
```

### Yaml configuration file

.pylinqsql-conf.yaml

```yaml
pylinqsql:
    whitelist: ["toto", "titi"]
    blacklist: ["tata", "tutu"]
    readonly: true
```

### Pyproject configuration file

.pyproject.toml

```toml
...

[tool.pylinqsql]
whitelist = ["toto", "titi"]
blacklist = ["tata", "tutu"]
readonly = true
```
