# Reporting a bug to py-linq-sql

## Prerequisites

1. Check that you have the latest version of py-linq-sql.
2. Check if the bug was not corrected in a more recent version of py-linq-sql.
3. Check if the bug is not already reported.
4. Reproduce the bug in the latest stable version of py-linq-sql.

## Create the issue

### Names

*Named the issue by : "BUG" + '\_module\_' + 'little description'.*

*example : "BUG \_canonize\_ do not sort parameters".*

### Description

*Bug description, as many detailed as possible.*

### Steps to Reproduce

*Steps to reproduce the bug in a list, as many detailed as possible.*

1.
2.
3.

### Expected behavior

*Expected result by py-linq-sql.*

### Actual behavior

*Current result given by py-linq-sql.*

### Reproduces how often

*How many times have you reproduced the bug.*

### Versions

*In which version of py-linq-sql the bug is present.*

### Additional Information

*All other information that may be useful.*
