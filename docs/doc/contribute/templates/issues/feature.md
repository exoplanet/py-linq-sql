# Propose a Feature to py-linq-sql

## Prerequisites

1. Check if the feature was not in a more recent version of py-linq-sql.
2. Check if the feature has not already been added in the issues.

## Create the issue

### Names

*Named the issue by : "FEATURE" + '\_module name\_' + 'little description'.*

*example : "FEATURE \_export\_ export yaml file in csv".*

### Summary

*Little summary to navigate in the issue.*

### Motivation

*Why add this feature to py-linq-sql.*

### Describe alternatives you've considered or tested

*All the alternatives you may have considered or tested, as many detailed as possible.*

### Additional context

*All other information that may be useful.*
