# Propose a bug fix to py-linq-sql

## Prerequisites

- The merge request must only fix an existing bug. To contribute other changes,
you must use a different template. You can see all templates [here](merge_request.md)
- The merge request must update the test suite to demonstrate the changed functionality.
- The commit message must be clear and mention the issue of the reported bug.

## Create the merge request

Your merge request must contain **all** this information

### Identify the Bug

### Description of the Change

### Alternate Designs

### Possible Drawbacks

### Verification Process

### Release Notes

### Additional Information
