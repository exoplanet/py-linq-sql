# Propose a documentation improvement to py-linq-sql

## Prerequisites

The merge request must only contribute documentation (markdown files).
To contribute other changes, you must use a different template. You can see all templates
[here](merge_request.md).

## Create the merge request

Your merge request must contain **all** this information

### Description of the Change

### Release Notes

### Additional Information
