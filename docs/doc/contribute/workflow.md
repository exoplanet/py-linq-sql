# Git Workflow of py-linq-sql

A git workflow dictates how we work with our git, how to create new branches and how to
submit them before they are merged.
We will see here how we use our git workflow to allow everyone working on the project
to do not get stuck waiting for their branches to be validated.

## Table of Content

- [Git Workflow of py-linq-sql](#git-workflow-of-py-linq-sql)
  - [Table of Content](#table-of-content)
  - [Main branch](#main-branch)
  - [New branches](#new-branches)
    - [Name of your branches](#name-of-your-branches)
    - [Independent branch](#independent-branch)
    - [Branch dependent of branch not yet on main](#branch-dependent-of-branch-not-yet-on-main)
  - [Make a merge request](#make-a-merge-request)
    - [Rebase](#rebase)
    - [Merge request on gitlab](#merge-request-on-gitlab)

## Main branch

This project is managed with only one protected branch `main`.
The main branch is protected, which means that only the owner and maintainers can push
and merge on it.

***Its is our branch of production.***

![Fig 1](img/main_branch.svg)

## New branches

A branch should be created for each modification. Those branches will then be pushed
to the gitlab repo before being checked by a maintainer.
After validation, they will be merged on main.

### Name of your branches

To name your branch, prefix it with `FEATURE_`, `DOC_`, `FIX_`, ... then use a
snack case name (all lowercase words separated by `_`).

Examples:

- `FEATURE_addition_for_float`
- `DOC_workflow`
- `FIX_bug_0723_addition`

### Independent branch

If your new branch does not need any functionality that is not yet merged on the main,
you need to create directly your branch from the main.

```bash
git checkout main
git branch FEATURE_feature_name
git checkout FEATURE_feature_name
```

You can also make

```bash
git checkout main
git checkout -b FEATURE_feature_name
```

For more information on the git branch you can read the
[git-branch documentation](https://git-scm.com/docs/git-branch)

Don't forget to fill in the `CHANGELOG.md` and dump the version number
in `pyprojects.toml`. Modify the `README.md` if you deem it necessary.

![Fig 2](img/inde_branch.svg)

### Branch dependent of branch not yet on main

If your branch depends of a branch not yet pushed on main, you have to create your
branch from this other branch.

If your branch `FEATURE_2` depends on `FEATURE_1`

```bash
git checkout FEATURE_1
git branch FEATURE_2
git checkout FEATURE_2
```

You can also make

```bash
git checkout FEATURE_1
git checkout -b FEATURE_2
```

For more information on the git branch you can read the
[git-branch documentation](https://git-scm.com/docs/git-branch)

Don't forget to fill in the `CHANGELOG.md` and dump the version number
in `pyprojects.toml`. Modify the `README.md` if you deem it necessary.

![Fig 3](img/depends_branch.svg)

## Make a merge request

### Rebase

When you finish the code of your branch, don't forget the tests and fill in
the `CHANGELOG.md` and dump the version number in `pyprojects.toml`.
Modify the `README.md` if you deem it necessary.

The first step before make a merge request is auto rebase your branch
to clean the history.
To make it you need to make an interactive rebase with

```bash
git rebase -i $(git merge-base $(git branch --show-current) main)
```

For more information on this command you can read the documentation of:

- [git-rebase -i](https://git-scm.com/docs/git-rebase#Documentation/git-rebase.txt--i)
- [git merge-base](https://git-scm.com/docs/git-merge-base)
- [git branch --show-current](https://git-scm.com/docs/git-branch#Documentation/git-branch.txt---show-current)

When you have understood what this command corresponds to, you can use the alias just

```bash
just autorebase
```

![Fig_4.1](img/rebase_int_1.png)

Is the file of the rebase opened in your terminal. We will summarize the 3 most used
keywords but read the comment at the end of the file carefully.
The must useful keyword are:

- reword: As indicated by his name this keyword allows you to rename your commit.
- pick : Pick tells the rebase to keep the commit as is.
- squash: Squash allows to reuniting the commit with the previous one.

You have to squash all commits that do not come from your branch in one commit that
you will rename: 'All my past'

![Fig_4.2](img/rebase_int_2.png)

Then you will keep all important commits and squash the ones that were done just to
"save your work" (often prefixed with a `WIP`)

![Fig_4.3](img/rebase_int_3.png)

You may have some conflicts to manage, be careful to select the right parts of code
and check with precommit before each `git rebase --continue`
At the end run `git push --force-with-lease`.

If you relaunch `just autorebase` you may have something like this.

![Fig_4.4](img/rebase_int_4.png)

For more information, look at these documents:

- [git rebase](https://git-scm.com/docs/git-rebase)
- [git push --force-with-lease](https://git-scm.com/docs/git-push#Documentation/git-push.txt---no-force-with-lease)

One last step before you can make your merge request: you need to rebase your branch
on the main.
To do that use

```bash
git rebase main
```

Resolve all conflicts and don't forget to launch precommit before
all `git rebase --continue`.
At the end run `git push --force-with-lease`.

When you use rebase on main, all commits of this branch start at the end of the main
branch.

| ![Fig 7.1](img/rebase_main_1.svg) |
|:--:|
| *Before the rebase on main* |

| ![Fig 7.2](img/rebase_main_2.svg) |
|:--:|
| *After the rebase on main* |

### Merge request on gitlab

When all is good you need to make a merge request in the gitlab. In the field: 'Select
source branch', select your branch and click on 'Compare branches and continue'

![Fig 8](img/mr_1.png)

Fill the field: 'Title *' with your branch and the field: 'Description' with the part of
the changelog which corresponds to this branch.

![Fig 9](img/mr_2.png)

Don't forget to check the box 'Squash commits when merge request is accepted' and click
on 'Create merge request'.

When a maintainer will look at your code:

- If everything is correct it will merge it into the main branch.
- If there are things to fix, he will do a review and you will have to correct your code
  before a maintainer goes back to see your code and merges it if everything is correct.

Once your MR has been validated, the branch will be merged into main, which will give an
architecture as below.

| ![Fig 10.1](img/merge_branch_1.svg) |
|:--:|
| *With independent branches* |

| ![Fig 10.2](img/merge_branch_2.svg) |
|:--:|
| *With depending branches* |
