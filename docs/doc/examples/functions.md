# MagicDotPath functions

## Mathematical functions

- **cbrt**: Cube root.
- **sqrt**: Square root.
- **factorial**: factorial.
- **ceil**: Nearest integer greater than equal.
- **degrees**: Converts radians to degrees.
- **floor**: Nearest integer les than or equal.
- **gcd**: Greatest common divisor.
- **lcm**: Least common multiple.
- **exp**: Exponential.
- **ln**: Natural logarithm.
- **log10**: Base 10 logarithm.
- **log**: Logarithm of x to base b.
- **min_scale**: Minimum scale.
- **radians**: Converts degrees to radians.
- **round**:
  - **round(v)**: Rounds to nearest integer.
  - **rounds(v, s)**: Rounds v to s decimal places.
- **scale**: Scale of the argument. The number of decimal digits in the fractional part.
- **sign**: Sign of the argument.
- **trim_scale**: Reduces the value's scale (number of fractional decimal digits) by
removing trailing zeroes.
- **trunc**:
  - **trunc(v)**: Truncates to integer (towards zero).
  - **trunc(v, s)**: Truncates v to s decimal places.
- **greatest**: Returns the largest of the values.
- **least**: Returns the smallest of the values.

## Trigonometric functions

- **acos**: Inverse cosine, result in radians.
- **acosd**: Inverse cosine, result in degrees.
- **asin**: Inverse sine, result in radians.
- **asind**: Inverse sine, result in degrees.
- **atan**: Inverse tangent, result in radians.
- **atand**: Inverse tangent, result in degrees.
- **atan2**: Inverse tangent of y/x, result in radians.
- **atan2d**: Inverse tangent of y/x, result in degrees.
- **cos**: Cosine, argument in radians.
- **cosd**: Cosine, argument in degrees.
- **cot**: Cotangent, argument in radians.
- **cotd**: Cotangent, argument in degrees.
- **sin**: Sine, argument in radians.
- **sind**: Sine, argument in degrees.
- **tan**: Tangent, argument in radians.
- **tand**: Tangent, argument in degrees.

## Hyperbolical functions

- **acosh**: Inverse hyperbolic cosine.
- **asinh**: Inverse hyperbolic sine.
- **atanh**: Inverse hyperbolic tangent.
- **cosh**: Hyperbolic cosine.
- **sinh**: Hyperbolic sine.
- **tanh**: Hyperbolic tangent.

## String functions

- **lower**: All characters are lower case.
- **upper**: All characters are upper case.
- **title**: All characters are lower case except the first in upper case.

## Other functions

- **in_list**: Check if is in a list.
