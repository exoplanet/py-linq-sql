# MagicDotPath Operators

- **abs**: Absolute value representing by `abs()`.
- **add** and **radd**: Addition representing by `+`.
- **and** and **rand**: And representing by `&`.
- **eq**: Equality representing by `==`.
- **ge**: Greater than or equal representing by  `>=`.
- **gt**: Greater than representing by `>`.
- **invert**: Not representing by `~`.
- **le**: Lesser than or equal representing by `<=`.
- **lt**: Lesser than representing by `<`.
- **mod** and **rmod**: Modulo representing by `%`.
- **mul** and **rmul**: Multiplication representing by `*`.
- **ne**: Not equal representing by `!=`.
- **neg**: Negation representing by `-`.
- **or** and **ror**: Or representing by `|`.
- **pos**: Positive representing by `+`.
- **pow** and **rpow**: Power representing by `^`.
- **sub** and **rsub**: Subtract representing by `-`.
- **truediv** and **rtruediv**: Divide representing by `/`.
