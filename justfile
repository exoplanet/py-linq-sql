# Remove python cache files
clean:
    find . \( -name __pycache__ -o -name "*.pyc" \) -delete

# Clear and display pwd
clear:
    clear
    pwd

# Install no-dev dependencies with poetry
install: clean clear
    poetry install --no-dev --remove-untracked

# install all dependencies with poetry and npm
install-all: clean clear
    poetry install --remove-untracked
    npm install
    sudo npm install markdownlint-cli2 --global

# Launch exodam pytest
test: clean clear
    python3 -m pytest

# Launch test and clean
pytest: test clean

# Update pre-commit hook and make a clean install of pre-commit dependencies
preupdate: clean clear
    pre-commit clean
    pre-commit autoupdate
    preinstall

# Do a clean install of pre-commit dependencies
preinstall: clean clear
    pre-commit install --hook-type pre-merge-commit
    pre-commit install --hook-type pre-push
    pre-commit install --hook-type post-rewrite
    pre-commit install-hooks
    pre-commit install

# Simulate a pre-commit check on added files
prepre: clean onmy31 clear
    #!/usr/bin/env sh
    set -eux pipefail
    git status
    pre-commit run --all-files

# Launch the mypy type linter on the module
mypy: clean clear
    mypy --show-error-codes --pretty -p py_linq_sql --config-file pyproject.toml

# launch psql shell
psql: clear
    sudo -u postgres psql exodam_objects

# psql sservice status
psql-status:
    sudo service postgresql status

# Run service of psql db
psql-start:
    sudo service postgresql start
    sudo service postgresql status

# Run service of psql db
psql-stop:
    sudo service postgresql stop
    sudo service postgresql status

# Run ruff
ruff path ="py_linq_sql": clear clear
    ruff {{path}}

# Run markdownlint
lintmd path='"**/*.md" "#node_modules"': clean clear
    markdownlint-cli2-config ".markdownlint-cli2.yaml" {{ path }}

# Run all linter
lint : clean clear mypy ruff lintmd

# Run black and isort
onmy31 path ="py_linq_sql tests": clean clear
    black {{path}}
    isort {{path}}

# auto interactive rebase
autorebase-main: clean clear
    git rebase -i $(git merge-base $(git branch --show-current) main)

autorebase-dev: clean clear
    git rebase -i $(git merge-base $(git branch --show-current) dev)

# rebase on main
rebaseM: clean clear
    git checkout main
    git pull
    git checkout -
    git rebase main

# Launch coverage on all
coverage: clean clear
    coverage run -m pytest
    clear
    coverage report -m --skip-covered --precision=3

# List of all just commands
list:
    just --list
