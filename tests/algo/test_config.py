# Pytest imports
import pytest
from pytest import param

# Standard imports
from contextlib import contextmanager
from typing import Any, Dict, List

# Third party imports
from assertpy import assert_that

# First party imports
import py_linq_sql
from py_linq_sql.config.config import (
    _get_config,
    _load_config_file,
    is_read_only,
    is_valid_table_name_with_white_and_black_list,
)

TEST_PATH = "tests/algo/test_config_file/"


@contextmanager
def faking_config(conf: Dict[str, Any]):
    def fake_config() -> Dict[str, List[str | None] | None | bool]:
        return conf

    old_get_config, py_linq_sql.config.config._get_config = (
        py_linq_sql.config.config._get_config,
        fake_config,
    )
    yield
    py_linq_sql.config.config._get_config = old_get_config


@pytest.mark.parametrize(
    "input_table, input_config",
    [
        param(
            "a",
            {"whitelist": None, "blacklist": None},
            id="white and black list: None, None",
        ),
        param(
            "a",
            {"whitelist": None, "blacklist": []},
            id="white and black list: None, []",
        ),
        param(
            "a",
            {"whitelist": ["a", "b", "c"], "blacklist": None},
            id="white and black list: [a, b, c], None",
        ),
        param(
            "d",
            {"whitelist": None, "blacklist": ["a", "b", "c"]},
            id="white and black list: None, [a, b, c]",
        ),
        param(
            "a",
            {"whitelist": ["a", "b", "c"], "blacklist": []},
            id="white and black list: [a, b, c], []",
        ),
        param(
            "a",
            {"whitelist": ["a", "b", "c"], "blacklist": ["b", "c"]},
            id="white and black list: [a, b, c], [b, c]",
        ),
    ],
)
def test_black_and_white_list_success(input_table, input_config):
    with faking_config(input_config):
        assert is_valid_table_name_with_white_and_black_list(input_table)


@pytest.mark.parametrize(
    "input_table, input_config",
    [
        param(
            "a", {"whitelist": [], "blacklist": []}, id="white and black list: [], []"
        ),
        param(
            "a",
            {"whitelist": [], "blacklist": None},
            id="white and black list: [], None",
        ),
        param(
            "d",
            {"whitelist": ["a", "b", "c"], "blacklist": None},
            id="white and black list: [a, b, c], None",
        ),
        param(
            "a",
            {"whitelist": None, "blacklist": ["a", "b", "c"]},
            id="white and black list: None, [a, b, c]",
        ),
        param(
            "a",
            {"whitelist": [], "blacklist": ["a", "b", "c"]},
            id="white and black list: [], [a, b, c] with a",
        ),
        param(
            "d",
            {"whitelist": [], "blacklist": ["a", "b", "c"]},
            id="white and black list: [], [a, b, c] with d",
        ),
        param(
            "d",
            {"whitelist": ["a", "b", "c"], "blacklist": []},
            id="white and black list: [a, b, c], [] with d",
        ),
        param(
            "a",
            {"whitelist": ["a", "b", "c"], "blacklist": ["a", "b", "c"]},
            id="white and black list: [a, b, c], [a, b, c] with a",
        ),
        param(
            "d",
            {"whitelist": ["a", "b", "c"], "blacklist": ["a", "b", "c"]},
            id="white and black list: [a, b, c], [a, b, c] with d",
        ),
        param(
            "b",
            {"whitelist": ["a", "b", "c"], "blacklist": ["b", "c"]},
            id="white and black list: [a, b, c], [b, c] with b",
        ),
        param(
            "a",
            {"whitelist": ["b", "c"], "blacklist": ["a", "b", "c"]},
            id="white and black list: [a, b], [a, b, c] with a",
        ),
        param(
            "b",
            {"whitelist": ["b", "c"], "blacklist": ["a", "b", "c"]},
            id="white and black list: [a, b], [a, b, c] with b",
        ),
    ],
)
def test_black_and_white_list_fails(input_table, input_config):
    with faking_config(input_config):
        assert not is_valid_table_name_with_white_and_black_list(input_table)


def test_readonly_true():
    with faking_config({"readonly": True}):
        assert is_read_only()


def test_readonly_false():
    with faking_config({"readonly": False}):
        assert not is_read_only()


@pytest.mark.parametrize(
    "input_toml, input_yaml, input_pyproject",
    [
        param(
            ".toml_test_config.toml",
            "no yaml file",
            "no pyproject file",
            id="load toml",
        ),
        param(
            "no toml file",
            ".yaml_test_config.yaml",
            "no pyproject file",
            id="load yaml",
        ),
        param(
            "no toml file",
            "no yaml file",
            ".pyproject_test_config.toml",
            id="load pyproject",
        ),
    ],
)
def test_load_config_file(input_toml, input_yaml, input_pyproject):
    config = _load_config_file(
        f"{TEST_PATH}{input_toml}",
        f"{TEST_PATH}{input_yaml}",
        f"{TEST_PATH}{input_pyproject}",
    )

    assert_that(config).is_equal_to(
        {"whitelist": ["toto"], "blacklist": ["titi"], "readonly": False},
    )


def test_load_no_config_file():
    config = _load_config_file("no toml", "no yaml", "no pyproject")

    assert_that(config).is_equal_to(
        {"whitelist": None, "blacklist": None, "readonly": False},
    )


@pytest.mark.parametrize(
    "input_conf, input_expected",
    [
        param(
            """
                whitelist: ['toto']
                blacklist: ['titi']
            """,
            {"whitelist": ["toto"], "blacklist": ["titi"], "readonly": False},
            id="black and white",
        ),
        param(
            """
                blacklist: ['titi']
            """,
            {"whitelist": None, "blacklist": ["titi"], "readonly": False},
            id="only black",
        ),
        param(
            """
                whitelist: ['toto']
            """,
            {"whitelist": ["toto"], "blacklist": None, "readonly": False},
            id="only white",
        ),
        param(
            """ """,
            {"whitelist": None, "blacklist": None, "readonly": False},
            id="nothings",
        ),
    ],
)
def test_default_config(input_conf, input_expected):
    config = _get_config(input_conf)
    assert_that(config).is_equal_to(input_expected)


@pytest.mark.parametrize(
    "input_toml, input_yaml, input_pyproject",
    [
        param(
            ".toml_test_empty_config.toml",
            "no yaml file",
            "no pyproject file",
            id="empty toml",
        ),
        param(
            "no toml file",
            ".yaml_test_empty_config.yaml",
            "no pyproject file",
            id="empty yaml",
        ),
        param(
            "no toml file",
            "no yaml file",
            ".pyproject_test_empty_config.toml",
            id="empty pyproject",
        ),
    ],
)
def test_empty_config_file(input_toml, input_yaml, input_pyproject):
    config = _load_config_file(
        f"{TEST_PATH}{input_toml}",
        f"{TEST_PATH}{input_yaml}",
        f"{TEST_PATH}{input_pyproject}",
    )

    assert_that(config).is_equal_to(
        {"whitelist": None, "blacklist": None, "readonly": False},
    )
