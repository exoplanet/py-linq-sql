# Pytest imports
import pytest

# Third party imports
from py_linq import Enumerable

# First party imports
from py_linq_sql import ReturnEmptyEnumerable
from py_linq_sql.build_request.consult_context import define_one_begin_end
from py_linq_sql.utils.classes.enum import CommandType

_PYE = Enumerable([1, 2, 3, 4, 5, 6, 7])


@pytest.mark.parametrize(
    "input_instruction ,input_expected",
    [
        # TEST TAKE FIRST
        ([("take", 4), ("take", 1)], _PYE.take(4).take(1)),
        ([("take", 1), ("take", 4)], _PYE.take(1).take(4)),
        ([("take", 4), ("skip", 1)], _PYE.take(4).skip(1)),
        ([("take", 18), ("skip", 1)], _PYE.take(18).skip(1)),
        ([("take", 4), ("take_last", 2)], _PYE.take(4).take_last(2)),
        ([("take", 2), ("take_last", 4)], _PYE.take(2).take_last(4)),
        ([("take", 4), ("skip_last", 2)], _PYE.take(4).skip_last(2)),
        # TEST SKIP FIRST
        ([("skip", 1), ("skip", 2)], _PYE.skip(1).skip(2)),
        ([("skip", 4), ("skip", 2)], _PYE.skip(4).skip(2)),
        ([("skip", 4), ("take", 2)], _PYE.skip(4).take(2)),
        ([("skip", 2), ("take", 4)], _PYE.skip(2).take(4)),
        ([("skip", 6), ("take", 4)], _PYE.skip(6).take(4)),
        ([("skip", 2), ("take", 18)], _PYE.skip(2).take(18)),
        ([("skip", 2), ("take_last", 4)], _PYE.skip(2).take_last(4)),
        ([("skip", 5), ("take_last", 3)], _PYE.skip(5).take_last(3)),
        ([("skip", 2), ("skip_last", 4)], _PYE.skip(2).skip_last(4)),
        # TEST TAKE_LAST FIRST
        ([("take_last", 4), ("take_last", 1)], _PYE.take_last(4).take_last(1)),
        ([("take_last", 2), ("take_last", 3)], _PYE.take_last(2).take_last(3)),
        ([("take_last", 5), ("take_last", 5)], _PYE.take_last(5).take_last(5)),
        ([("take_last", 5), ("take", 18)], _PYE.take_last(5).take(18)),
        ([("take_last", 4), ("take", 3)], _PYE.take_last(4).take(3)),
        ([("take_last", 2), ("take", 3)], _PYE.take_last(2).take(3)),
        ([("take_last", 4), ("skip", 3)], _PYE.take_last(4).skip(3)),
        ([("take_last", 4), ("skip_last", 2)], _PYE.take_last(4).skip_last(2)),
        # TEST SKIP_LAST FIRST
        ([("skip_last", 2), ("skip_last", 4)], _PYE.skip_last(2).skip_last(4)),
        ([("skip_last", 3), ("skip_last", 1)], _PYE.skip_last(3).skip_last(1)),
        ([("skip_last", 2), ("take", 4)], _PYE.skip_last(2).take(4)),
        ([("skip_last", 2), ("take", 6)], _PYE.skip_last(2).take(6)),
        ([("skip_last", 4), ("take", 3)], _PYE.skip_last(4).take(3)),
        ([("skip_last", 4), ("take", 4)], _PYE.skip_last(4).take(4)),
        ([("skip_last", 2), ("skip", 3)], _PYE.skip_last(2).skip(3)),
        ([("skip_last", 4), ("skip", 2)], _PYE.skip_last(4).skip(2)),
        ([("skip_last", 2), ("take_last", 3)], _PYE.skip_last(2).take_last(3)),
        ([("skip_last", 3), ("take_last", 1)], _PYE.skip_last(3).take_last(1)),
        ([("skip_last", 4), ("take_last", 4)], _PYE.skip_last(4).take_last(4)),
        # BONUS TESTS
        (
            [("take", 4), ("take_last", 2), ("take", 1)],
            _PYE.take(4).take_last(2).take(1),
        ),
        ([("take", 6), ("skip", 2), ("take", 3)], _PYE.take(6).skip(2).take(3)),
        (
            [("skip", 2), ("skip_last", 3), ("skip", 1)],
            _PYE.skip(2).skip_last(3).skip(1),
        ),
        (
            [("take", 4), ("skip", 2), ("take_last", 3)],
            _PYE.take(4).skip(2).take_last(3),
        ),
        (
            [("skip", 1), ("take", 4), ("take_last", 5)],
            _PYE.skip(1).take(4).take_last(5),
        ),
    ],
)
def test_define_begin_end_success(input_instruction, input_expected):
    e = [1, 2, 3, 4, 5, 6, 7]
    begin, end = 0, 7
    for element in input_instruction:
        value = element[1]
        match element[0]:
            case "take":
                begin, end = define_one_begin_end(begin, end, value, CommandType.TAKE)
            case "skip":
                begin, end = define_one_begin_end(begin, end, value, CommandType.SKIP)
            case "take_last":
                begin, end = define_one_begin_end(
                    begin, end, value, CommandType.TAKE_LAST
                )
            case "skip_last":
                begin, end = define_one_begin_end(
                    begin, end, value, CommandType.SKIP_LAST
                )

    assert input_expected.to_list() == e[begin:end]


@pytest.mark.parametrize(
    "input_instruction",
    [
        ([("skip", 5), ("skip_last", 3)]),
        ([("take", 2), ("skip", 4)]),
        ([("take", 2), ("skip", 3)]),
        ([("take", 4), ("skip_last", 5)]),
        ([("skip", 8)]),
        ([("take_last", 4), ("skip", 6)]),
        ([("take_last", 4), ("skip", 4)]),
        ([("take_last", 4), ("skip_last", 4)]),
        ([("take_last", 2), ("skip_last", 4)]),
        ([("skip_last", 4), ("skip", 4)]),
        ([("skip_last", 2), ("skip_last", 5)]),
    ],
)
def test_define_begin_end_empty_return(input_instruction):
    begin, end = 0, 7
    with pytest.raises(ReturnEmptyEnumerable):
        for element in input_instruction:
            value = element[1]
            match element[0]:
                case "take":
                    begin, end = define_one_begin_end(
                        begin, end, value, CommandType.TAKE
                    )
                case "skip":
                    begin, end = define_one_begin_end(
                        begin, end, value, CommandType.SKIP
                    )
                case "take_last":
                    begin, end = define_one_begin_end(
                        begin, end, value, CommandType.TAKE_LAST
                    )
                case "skip_last":
                    begin, end = define_one_begin_end(
                        begin, end, value, CommandType.SKIP_LAST
                    )
