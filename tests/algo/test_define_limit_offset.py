# Pytest imports
import pytest

# Third party imports
from dotmap import DotMap as DM

# First party imports
from py_linq_sql import ReturnEmptyEnumerable
from py_linq_sql.build_request.consult_context import define_limit_offset
from py_linq_sql.utils.classes.enum import CommandType as CT
from py_linq_sql.utils.classes.other_classes import Command as CMD
from py_linq_sql.utils.classes.other_classes import SQLEnumerableData


@pytest.mark.parametrize(
    "input_cmd ,input_expected",
    [
        # TEST TAKE FIRST
        ([CMD(CT.TAKE, DM(number=4)), CMD(CT.TAKE, DM(number=1))], "LIMIT 1 OFFSET 0"),
        ([CMD(CT.TAKE, DM(number=1)), CMD(CT.TAKE, DM(number=4))], "LIMIT 1 OFFSET 0"),
        ([CMD(CT.TAKE, DM(number=4)), CMD(CT.SKIP, DM(number=1))], "LIMIT 3 OFFSET 1"),
        ([CMD(CT.TAKE, DM(number=18)), CMD(CT.SKIP, DM(number=1))], "LIMIT 6 OFFSET 1"),
        (
            [CMD(CT.TAKE, DM(number=4)), CMD(CT.TAKE_LAST, DM(number=2))],
            "LIMIT 2 OFFSET 2",
        ),
        (
            [CMD(CT.TAKE, DM(number=2)), CMD(CT.TAKE_LAST, DM(number=4))],
            "LIMIT 2 OFFSET 0",
        ),
        (
            [CMD(CT.TAKE, DM(number=4)), CMD(CT.SKIP_LAST, DM(number=2))],
            "LIMIT 2 OFFSET 0",
        ),
        # TEST SKIP FIRST
        ([CMD(CT.SKIP, DM(number=1)), CMD(CT.SKIP, DM(number=2))], "LIMIT 4 OFFSET 3"),
        ([CMD(CT.SKIP, DM(number=4)), CMD(CT.SKIP, DM(number=2))], "LIMIT 1 OFFSET 6"),
        ([CMD(CT.SKIP, DM(number=4)), CMD(CT.TAKE, DM(number=2))], "LIMIT 2 OFFSET 4"),
        ([CMD(CT.SKIP, DM(number=2)), CMD(CT.TAKE, DM(number=4))], "LIMIT 4 OFFSET 2"),
        ([CMD(CT.SKIP, DM(number=6)), CMD(CT.TAKE, DM(number=4))], "LIMIT 1 OFFSET 6"),
        ([CMD(CT.SKIP, DM(number=2)), CMD(CT.TAKE, DM(number=18))], "LIMIT 5 OFFSET 2"),
        (
            [CMD(CT.SKIP, DM(number=2)), CMD(CT.TAKE_LAST, DM(number=4))],
            "LIMIT 4 OFFSET 3",
        ),
        (
            [CMD(CT.SKIP, DM(number=5)), CMD(CT.TAKE_LAST, DM(number=3))],
            "LIMIT 2 OFFSET 5",
        ),
        (
            [CMD(CT.SKIP, DM(number=2)), CMD(CT.SKIP_LAST, DM(number=4))],
            "LIMIT 1 OFFSET 2",
        ),
        # TEST TAKE_LAST FIRST
        (
            [CMD(CT.TAKE_LAST, DM(number=4)), CMD(CT.TAKE_LAST, DM(number=1))],
            "LIMIT 1 OFFSET 6",
        ),
        (
            [CMD(CT.TAKE_LAST, DM(number=2)), CMD(CT.TAKE_LAST, DM(number=3))],
            "LIMIT 2 OFFSET 5",
        ),
        (
            [CMD(CT.TAKE_LAST, DM(number=5)), CMD(CT.TAKE_LAST, DM(number=5))],
            "LIMIT 5 OFFSET 2",
        ),
        (
            [CMD(CT.TAKE_LAST, DM(number=5)), CMD(CT.TAKE, DM(number=18))],
            "LIMIT 5 OFFSET 2",
        ),
        (
            [CMD(CT.TAKE_LAST, DM(number=4)), CMD(CT.TAKE, DM(number=3))],
            "LIMIT 3 OFFSET 3",
        ),
        (
            [CMD(CT.TAKE_LAST, DM(number=2)), CMD(CT.TAKE, DM(number=3))],
            "LIMIT 2 OFFSET 5",
        ),
        (
            [CMD(CT.TAKE_LAST, DM(number=4)), CMD(CT.SKIP, DM(number=3))],
            "LIMIT 1 OFFSET 6",
        ),
        (
            [CMD(CT.TAKE_LAST, DM(number=4)), CMD(CT.SKIP_LAST, DM(number=2))],
            "LIMIT 2 OFFSET 3",
        ),
        # TEST SKIP_LAST FIRST
        (
            [CMD(CT.SKIP_LAST, DM(number=2)), CMD(CT.SKIP_LAST, DM(number=4))],
            "LIMIT 1 OFFSET 0",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=3)), CMD(CT.SKIP_LAST, DM(number=1))],
            "LIMIT 3 OFFSET 0",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=2)), CMD(CT.TAKE, DM(number=4))],
            "LIMIT 4 OFFSET 0",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=2)), CMD(CT.TAKE, DM(number=6))],
            "LIMIT 5 OFFSET 0",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=4)), CMD(CT.TAKE, DM(number=3))],
            "LIMIT 3 OFFSET 0",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=4)), CMD(CT.TAKE, DM(number=4))],
            "LIMIT 3 OFFSET 0",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=2)), CMD(CT.SKIP, DM(number=3))],
            "LIMIT 2 OFFSET 3",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=4)), CMD(CT.SKIP, DM(number=2))],
            "LIMIT 1 OFFSET 2",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=2)), CMD(CT.TAKE_LAST, DM(number=3))],
            "LIMIT 3 OFFSET 2",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=3)), CMD(CT.TAKE_LAST, DM(number=1))],
            "LIMIT 1 OFFSET 3",
        ),
        (
            [CMD(CT.SKIP_LAST, DM(number=4)), CMD(CT.TAKE_LAST, DM(number=4))],
            "LIMIT 3 OFFSET 0",
        ),
        # BONUS TESTS
        (
            [
                CMD(CT.TAKE, DM(number=4)),
                CMD(CT.TAKE_LAST, DM(number=2)),
                CMD(CT.TAKE, DM(number=1)),
            ],
            "LIMIT 1 OFFSET 2",
        ),
        (
            [
                CMD(CT.TAKE, DM(number=6)),
                CMD(CT.SKIP, DM(number=2)),
                CMD(CT.TAKE, DM(number=3)),
            ],
            "LIMIT 3 OFFSET 2",
        ),
        (
            [
                CMD(CT.SKIP, DM(number=2)),
                CMD(CT.SKIP_LAST, DM(number=3)),
                CMD(CT.SKIP, DM(number=1)),
            ],
            "LIMIT 1 OFFSET 3",
        ),
        (
            [
                CMD(CT.TAKE, DM(number=4)),
                CMD(CT.SKIP, DM(number=2)),
                CMD(CT.TAKE_LAST, DM(number=3)),
            ],
            "LIMIT 2 OFFSET 2",
        ),
        (
            [
                CMD(CT.SKIP, DM(number=1)),
                CMD(CT.TAKE, DM(number=4)),
                CMD(CT.TAKE_LAST, DM(number=5)),
            ],
            "LIMIT 4 OFFSET 1",
        ),
    ],
)
def test_define_begin_end(db_connection_empty, input_cmd, input_expected):

    sqle = SQLEnumerableData(db_connection_empty, DM(), input_cmd, "objects", 7)
    assert define_limit_offset(sqle, set()) == input_expected


@pytest.mark.parametrize(
    "input_cmd",
    [
        ([CMD(CT.SKIP, DM(number=5)), CMD(CT.SKIP_LAST, DM(number=3))]),
        ([CMD(CT.TAKE, DM(number=2)), CMD(CT.SKIP, DM(number=4))]),
        ([CMD(CT.TAKE, DM(number=2)), CMD(CT.SKIP, DM(number=3))]),
        ([CMD(CT.TAKE, DM(number=4)), CMD(CT.SKIP_LAST, DM(number=5))]),
        ([CMD(CT.SKIP, DM(number=8))]),
        ([CMD(CT.TAKE_LAST, DM(number=4)), CMD(CT.SKIP, DM(number=6))]),
        ([CMD(CT.TAKE_LAST, DM(number=4)), CMD(CT.SKIP, DM(number=4))]),
        ([CMD(CT.TAKE_LAST, DM(number=4)), CMD(CT.SKIP_LAST, DM(number=4))]),
        ([CMD(CT.TAKE_LAST, DM(number=2)), CMD(CT.SKIP_LAST, DM(number=4))]),
        ([CMD(CT.SKIP_LAST, DM(number=4)), CMD(CT.SKIP, DM(number=4))]),
        ([CMD(CT.SKIP_LAST, DM(number=2)), CMD(CT.SKIP_LAST, DM(number=5))]),
    ],
)
def test_define_limit_offset_empty_return(db_connection_empty, input_cmd):
    sqle = SQLEnumerableData(db_connection_empty, DM(), input_cmd, "objects", 7)
    with pytest.raises(ReturnEmptyEnumerable):
        define_limit_offset(sqle, set())
