# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql.utils.functions.other_functions import _fix_same_column_name


@pytest.mark.parametrize(
    """input_list, input_expected""",
    [
        param(
            ["toto", "titi", "tutu"],
            ["toto", "titi", "tutu"],
            id="fix column name without duplicate",
        ),
        param(
            ["toto", "titi", "toto", "tutu"],
            ["toto", "titi", "toto__1", "tutu"],
            id="fix column name with duplicate",
        ),
        param(
            ["toto", "toto", "toto", "toto"],
            ["toto", "toto__1", "toto__2", "toto__3"],
            id="fix column name with many same duplicate",
        ),
        param(
            ["toto", "titi", "toto", "titi"],
            ["toto", "titi", "toto__1", "titi__1"],
            id="fix column name with many not same duplicate",
        ),
    ],
)
def test_fix_column_name(input_list, input_expected):
    assert _fix_same_column_name(input_list) == input_expected
