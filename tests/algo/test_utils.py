# Pytest imports
import pytest
from pytest import param

# Standard imports
from datetime import date
from decimal import Decimal

# Third party imports
from py_linq import Enumerable

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql.utils.functions.other_functions import get_good_type, pretty_print


@pytest.mark.parametrize(
    "input_cast_type, input_expected",
    [
        param(int, "decimal))", id="int"),
        param(float, "decimal))", id="float"),
        param(Decimal, "decimal))", id="decimal"),
        param(date, "date))", id="date"),
    ],
)
def test_get_good_type(input_cast_type, input_expected):
    assert get_good_type(input_cast_type) == input_expected


def test_pretty_print_classic_execute(db_connection_with_data, table_objects):
    record = SQLEnumerable(db_connection_with_data, table_objects).select().execute()
    pretty_print(record)
    assert True


@pytest.mark.parametrize(
    "input_record",
    [
        param(None, id="None"),
        param(Enumerable(), id="empty enumerable"),
    ],
)
def test_pretty_print_empty_or_none(input_record):
    pretty_print(input_record)
    assert True
