# Pytest imports
import pytest

# Third party imports
from dotmap import DotMap

# First party imports
from py_linq_sql.utils.classes.enum import CommandType as CT
from py_linq_sql.utils.classes.other_classes import Command


@pytest.mark.parametrize(
    "type_1, type_2, args_1, args_2, expected",
    [
        (CT.SELECT, CT.SELECT, DotMap(a=1, b=2), DotMap(a=1, b=2), True),
        (CT.JOIN, CT.JOIN, DotMap(a=1, b=2), DotMap(a=1, b=2), True),
        (CT.JOIN, CT.JOIN, DotMap(a=1, b=2), DotMap(b=2, a=1), True),
        (CT.JOIN, CT.JOIN, DotMap(a=1, b=2), DotMap(a=1, c=3), False),
        (CT.SELECT, CT.JOIN, DotMap(a=1, b=2), DotMap(a=1, b=2), False),
        (CT.JOIN, CT.SELECT, DotMap(a=1, b=2), DotMap(a=1, b=2), False),
        (CT.JOIN, CT.JOIN, DotMap(a=1, b=2), DotMap(b=1, a=2), False),
    ],
)
def test_classes_command_eq(type_1, type_2, args_1, args_2, expected):
    c1 = Command(type_1, args_1)
    c2 = Command(type_2, args_2)
    assert (c1 == c2) == expected


@pytest.mark.parametrize(
    "type_1, type_2, args_1, args_2, expected",
    [
        (CT.SELECT, CT.SELECT, DotMap(a=1, b=2), DotMap(a=1, b=2), False),
        (CT.JOIN, CT.JOIN, DotMap(a=1, b=2), DotMap(a=1, b=2), False),
        (CT.JOIN, CT.JOIN, DotMap(a=1, b=2), DotMap(b=2, a=1), False),
        (CT.JOIN, CT.JOIN, DotMap(a=1, b=2), DotMap(a=1, c=3), True),
        (CT.SELECT, CT.JOIN, DotMap(a=1, b=2), DotMap(a=1, b=2), True),
        (CT.JOIN, CT.SELECT, DotMap(a=1, b=2), DotMap(a=1, b=2), True),
        (CT.JOIN, CT.JOIN, DotMap(a=1, b=2), DotMap(b=1, a=2), True),
    ],
)
def test_classes_command_ne(type_1, type_2, args_1, args_2, expected):
    c1 = Command(type_1, args_1)
    c2 = Command(type_2, args_2)
    assert (c1 != c2) == expected


def test_classes_command_eq_not_implemented():
    c1 = Command(CT.SELECT, DotMap(a=1, b=2))

    assert not (c1 == 12) == NotImplemented
