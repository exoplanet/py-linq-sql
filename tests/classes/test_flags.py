# Pytest imports
import pytest

# First party imports
from py_linq_sql.utils.classes.other_classes import Flags, equality


@pytest.mark.parametrize(
    "flags_1, flags_2, expected",
    [
        (Flags(), Flags(), True),
        (Flags(select=True), Flags(select=True), True),
        (
            Flags(
                select=True,
                alter=False,
                one=False,
                terminal=True,
                limit_offset=False,
                default_cmd=True,
            ),
            Flags(
                select=True,
                alter=False,
                one=False,
                terminal=True,
                limit_offset=False,
                default_cmd=True,
            ),
            True,
        ),
        (Flags(select=True), Flags(terminal=True), False),
        (
            Flags(
                select=True,
                alter=False,
                one=False,
                terminal=False,
                limit_offset=False,
                default_cmd=True,
            ),
            Flags(
                select=True,
                alter=False,
                one=False,
                terminal=True,
                limit_offset=False,
                default_cmd=True,
            ),
            False,
        ),
    ],
)
def test_classes_flags_eq(flags_1, flags_2, expected):
    assert (flags_1 == flags_2) == expected


@pytest.mark.parametrize(
    "flags_1, flags_2, expected",
    [
        (Flags(), Flags(), False),
        (Flags(select=True), Flags(select=True), False),
        (
            Flags(
                select=True,
                alter=False,
                one=False,
                terminal=True,
                limit_offset=False,
                default_cmd=True,
            ),
            Flags(
                select=True,
                alter=False,
                one=False,
                terminal=True,
                limit_offset=False,
                default_cmd=True,
            ),
            False,
        ),
        (Flags(select=True), Flags(terminal=True), True),
        (
            Flags(
                select=True,
                alter=False,
                one=False,
                terminal=False,
                limit_offset=False,
                default_cmd=True,
            ),
            Flags(
                select=True,
                alter=False,
                one=False,
                terminal=True,
                limit_offset=False,
                default_cmd=True,
            ),
            True,
        ),
    ],
)
def test_classes_flags_ne(flags_1, flags_2, expected):
    assert (flags_1 != flags_2) == expected


def test_equality_functions():
    res = equality(12, "toto")
    assert res == NotImplemented
