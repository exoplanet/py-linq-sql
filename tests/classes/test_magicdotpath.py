# Standard imports
from decimal import Decimal

# First party imports
from py_linq_sql.utils.classes.magicdotpath import MagicDotPath


def test_classes_MagicDotPath_int_for_operator_eq(db_connection_empty):
    fquery = lambda x: x.data.obj.name == 12  # noqa: E731
    mdp = fquery(MagicDotPath(db_connection_empty))

    assert mdp


def test_classes_MagicDotPath_decimal_for_operator_eq(db_connection_empty):
    fquery = lambda x: x.data.obj.name == Decimal(12)  # noqa: E731
    mdp = fquery(MagicDotPath(db_connection_empty))

    assert mdp


def test_classes_MagicDotPath_int_for_operator_lt(db_connection_empty):
    fquery = lambda x: x.data.obj.name < 12  # noqa: E731
    mdp = fquery(MagicDotPath(db_connection_empty))

    assert mdp


def test_classes_MagicDotPath_decimal_for_operator_lt(db_connection_empty):
    fquery = lambda x: x.data.obj.name < Decimal(12)  # noqa: E731
    mdp = fquery(MagicDotPath(db_connection_empty))

    assert mdp


def test_classes_MagicDotPath_getitem(db_connection_empty):
    fquery = lambda x: x["data"]["obj"]["name"] < Decimal(12)  # noqa: E731
    mdp = fquery(MagicDotPath(db_connection_empty))

    assert mdp
