# Pytest imports
import pytest

# Third party imports
from dotmap import DotMap

# First party imports
from py_linq_sql import SQLEnumerable as sqle
from py_linq_sql.utils.classes.enum import CommandType
from py_linq_sql.utils.classes.other_classes import Command, Flags


@pytest.mark.parametrize(
    "table_1, table_2, expected",
    [
        ("objects", "objects", True),
        ("objects", "toto", False),
    ],
)
def test_classes_sql_enumerable_eq_simple(
    db_connection_empty, table_1, table_2, expected
):
    sqle_1 = sqle(db_connection_empty, table_1)
    sqle_2 = sqle(db_connection_empty, table_2)
    assert (sqle_1 == sqle_2) == expected


@pytest.mark.parametrize(
    "len_1, len_2, expected",
    [
        (2, 2, True),
        (2, 3, False),
        (2, None, False),
    ],
)
def test_classes_sql_enumerable_eq_length(
    db_connection_empty, table_objects, len_1, len_2, expected
):
    sqle_1 = sqle(db_connection_empty, table_objects).length = len_1
    sqle_2 = sqle(db_connection_empty, table_objects).length = len_2
    assert (sqle_1 == sqle_2) == expected


@pytest.mark.parametrize(
    "cmd_1, cmd_2, expected",
    [
        ([], [], True),
        (
            [Command(CommandType.TAKE, DotMap())],
            [Command(CommandType.TAKE, DotMap())],
            True,
        ),
        ([Command(CommandType.TAKE, DotMap())], [], False),
        (
            [Command(CommandType.TAKE, DotMap())],
            [Command(CommandType.SKIP, DotMap())],
            False,
        ),
    ],
)
def test_classes_sql_enumerable_eq_cmd(
    db_connection_empty, table_objects, cmd_1, cmd_2, expected
):
    sqle_1 = sqle(db_connection_empty, table_objects).cmd = cmd_1
    sqle_2 = sqle(db_connection_empty, table_objects).cmd = cmd_2
    assert (sqle_1 == sqle_2) == expected


@pytest.mark.parametrize(
    "flags_1, flags_2, expected",
    [
        (Flags(), Flags(), True),
        (Flags(select=True), Flags(select=True), True),
        (Flags(select=True), Flags(), False),
        (Flags(select=True), Flags(terminal=True), False),
    ],
)
def test_classes_sql_enumerable_eq_flags(
    db_connection_empty, table_objects, flags_1, flags_2, expected
):
    sqle_1 = sqle(db_connection_empty, table_objects).flags = flags_1
    sqle_2 = sqle(db_connection_empty, table_objects).flags = flags_2
    assert (sqle_1 == sqle_2) == expected


@pytest.mark.parametrize(
    "table_1, table_2, expected",
    [
        ("objects", "objects", False),
        ("objects", "toto", True),
    ],
)
def test_classes_sql_enumerable_ne_simple(
    db_connection_empty, table_1, table_2, expected
):
    sqle_1 = sqle(db_connection_empty, table_1)
    sqle_2 = sqle(db_connection_empty, table_2)
    assert (sqle_1 != sqle_2) == expected


@pytest.mark.parametrize(
    "len_1, len_2, expected",
    [
        (2, 2, False),
        (2, 3, True),
        (2, None, True),
    ],
)
def test_classes_sql_enumerable_ne_length(
    db_connection_empty, table_objects, len_1, len_2, expected
):
    sqle_1 = sqle(db_connection_empty, table_objects).length = len_1
    sqle_2 = sqle(db_connection_empty, table_objects).length = len_2
    assert (sqle_1 != sqle_2) == expected


@pytest.mark.parametrize(
    "cmd_1, cmd_2, expected",
    [
        ([], [], False),
        (
            [Command(CommandType.TAKE, DotMap())],
            [Command(CommandType.TAKE, DotMap())],
            False,
        ),
        ([Command(CommandType.TAKE, DotMap())], [], True),
        (
            [Command(CommandType.TAKE, DotMap())],
            [Command(CommandType.SKIP, DotMap())],
            True,
        ),
    ],
)
def test_classes_sql_enumerable_ne_cmd(
    db_connection_empty, table_objects, cmd_1, cmd_2, expected
):
    sqle_1 = sqle(db_connection_empty, table_objects).cmd = cmd_1
    sqle_2 = sqle(db_connection_empty, table_objects).cmd = cmd_2
    assert (sqle_1 != sqle_2) == expected


@pytest.mark.parametrize(
    "flags_1, flags_2, expected",
    [
        (Flags(), Flags(), False),
        (Flags(select=True), Flags(select=True), False),
        (Flags(select=True), Flags(), True),
        (Flags(select=True), Flags(terminal=True), True),
    ],
)
def test_classes_sql_enumerable_ne_flags(
    db_connection_empty, table_objects, flags_1, flags_2, expected
):
    sqle_1 = sqle(db_connection_empty, table_objects).flags = flags_1
    sqle_2 = sqle(db_connection_empty, table_objects).flags = flags_2
    assert (sqle_1 != sqle_2) == expected


def test_classes_sqle_enumerable_copy_table_str(db_connection_empty, table_objects):
    original = sqle(db_connection_empty, table_objects)
    copy = original.copy()
    assert original == copy


def test_classes_sqle_enumerable_copy_table_request(db_connection_empty, table_objects):
    se = sqle(db_connection_empty, table_objects)
    original = sqle(db_connection_empty, se)
    copy = original.copy()
    assert original == copy
