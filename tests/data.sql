-- INSERT INTO objects table
INSERT INTO objects(data) VALUES
('{"obj": {"name": "earth", "mass": 1}, "toto": "toto", "refereed": false}'),
('{"obj": {"name": "saturn", "mass": 52}, "toto": "toto", "refereed": true}'),
('{"obj": {"name": "jupiter", "mass": 100}, "toto": "toto", "refereed": true}');


-- INSERT INTO satellite table
INSERT INTO satellite(data) VALUES
('{"obj": {"name": "moon", "owner_id": 1}}'),
('{"obj": {"name": "europe", "owner_id": 3}}'),
('{"obj": {"name": "callisto", "owner_id": 3}}'),
('{"obj": {"name": "alone"}}');

-- INSERT INTO test_sc.test table
INSERT INTO test_sc.test(data) VALUES
('{"obj": {"name": "earth", "mass": 1}, "toto": "toto"}');
