# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql.utils.classes.enum import CommandType
from py_linq_sql.utils.classes.other_classes import Command, Flags


def test_exception_Command_lt():
    c1 = Command(CommandType.SELECT, None)
    c2 = Command(CommandType.SELECT, None)
    with pytest.raises(TypeError):
        c1 < c2


def test_exception_Command_le():
    c1 = Command(CommandType.SELECT, None)
    c2 = Command(CommandType.SELECT, None)
    with pytest.raises(TypeError):
        c1 <= c2


def test_exception_Command_ge():
    c1 = Command(CommandType.SELECT, None)
    c2 = Command(CommandType.SELECT, None)
    with pytest.raises(TypeError):
        c1 > c2


def test_exception_Command_gt():
    c1 = Command(CommandType.SELECT, None)
    c2 = Command(CommandType.SELECT, None)
    with pytest.raises(TypeError):
        c1 >= c2


def test_exception_Flags_lt():
    f1 = Flags()
    f2 = Flags()
    with pytest.raises(TypeError):
        f1 < f2


def test_exception_Flags_le():
    f1 = Flags()
    f2 = Flags()
    with pytest.raises(TypeError):
        f1 <= f2


def test_exception_Flags_ge():
    f1 = Flags()
    f2 = Flags()
    with pytest.raises(TypeError):
        f1 >= f2


def test_exception_Flags_gt():
    f1 = Flags()
    f2 = Flags()
    with pytest.raises(TypeError):
        f1 > f2


def test_exception_SQLEnumerable_lt(db_connection_empty, table_objects):
    sqle1 = SQLEnumerable(db_connection_empty, table_objects)
    sqle2 = SQLEnumerable(db_connection_empty, table_objects)
    with pytest.raises(TypeError):
        sqle1 < sqle2


def test_exception_SQLEnumerable_le(db_connection_empty, table_objects):
    sqle1 = SQLEnumerable(db_connection_empty, table_objects)
    sqle2 = SQLEnumerable(db_connection_empty, table_objects)
    with pytest.raises(TypeError):
        sqle1 <= sqle2


def test_exception_SQLEnumerable_ge(db_connection_empty, table_objects):
    sqle1 = SQLEnumerable(db_connection_empty, table_objects)
    sqle2 = SQLEnumerable(db_connection_empty, table_objects)
    with pytest.raises(TypeError):
        sqle1 >= sqle2


def test_exception_SQLEnumerable_gt(db_connection_empty, table_objects):
    sqle1 = SQLEnumerable(db_connection_empty, table_objects)
    sqle2 = SQLEnumerable(db_connection_empty, table_objects)
    with pytest.raises(TypeError):
        sqle1 > sqle2
