# Pytest imports
import pytest

# Third party imports
from dotmap import DotMap

# First party imports
from py_linq_sql import ColumnNameError, TypeOperatorError
from py_linq_sql.utils.classes.op_and_func_of_mdp import (
    HyperBFuncType,
    MathFunctType,
    col_name_hyper,
    col_name_math,
    col_name_ope,
    col_name_str,
    col_name_trigo,
    json_path_hyper,
    json_path_math,
    json_path_ope,
    json_path_str,
    json_path_trigo,
)
from py_linq_sql.utils.functions.other_functions import _col_name_validator


def test_exception_col_name_hyper():
    with pytest.raises(TypeOperatorError):
        col_name_hyper(DotMap(op1="mass"), MathFunctType.CBRT)


def test_exception_col_name_math():
    with pytest.raises(TypeOperatorError):
        col_name_math(DotMap(op1="mass"), HyperBFuncType.ACOSH)


def test_exception_col_name_op():
    with pytest.raises(TypeOperatorError):
        col_name_ope(DotMap(op1="mass"), MathFunctType.CBRT)


def test_exception_col_name_trigo():
    with pytest.raises(TypeOperatorError):
        col_name_trigo(DotMap(op1="mass"), MathFunctType.CBRT)


def test_exception_col_name_str():
    with pytest.raises(TypeOperatorError):
        col_name_str(DotMap(op1="name"), MathFunctType.CBRT)


def test_exception_json_path_hyper():
    with pytest.raises(TypeOperatorError):
        json_path_hyper(DotMap(op1="mass"), MathFunctType.CBRT)


def test_exception_json_path_math():
    with pytest.raises(TypeOperatorError):
        json_path_math(DotMap(op1="mass"), HyperBFuncType.ACOSH)


def test_exception_json_path_ope():
    with pytest.raises(TypeOperatorError):
        json_path_ope(DotMap(op1="mass"), MathFunctType.CBRT)


def test_exception_json_path_trigo():
    with pytest.raises(TypeOperatorError):
        json_path_trigo(DotMap(op1="mass"), MathFunctType.CBRT)


def test_exception_json_path_str():
    with pytest.raises(TypeOperatorError):
        json_path_str(DotMap(op1="name"), MathFunctType.CBRT)


# Col name syntax


def test_exception_wrong_col_name_syntax():
    with pytest.raises(ColumnNameError):
        _col_name_validator(
            "48_degrees_51_minutes_45_81_seconds_N_"
            "2_degrees_17_minutes_15_331_seconds_E"
        )
