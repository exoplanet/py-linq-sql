# Pytest imports
import pytest

# Standard imports
from contextlib import contextmanager
from typing import Any, Dict, List

# First party imports
import py_linq_sql
from py_linq_sql import SQLEnumerable, TablePermissionDeniedError
from py_linq_sql.exception.exception import ReadOnlyPermissionDeniedError


@contextmanager
def faking_config(conf: Dict[str, Any]):
    def fake_config() -> Dict[str, List[str | None] | None | bool]:
        return conf

    old_get_config, py_linq_sql.config.config._get_config = (
        py_linq_sql.config.config._get_config,
        fake_config,
    )
    yield
    py_linq_sql.config.config._get_config = old_get_config


def test_exception_black_list(db_connection_with_data) -> SQLEnumerable:
    with pytest.raises(TablePermissionDeniedError):
        SQLEnumerable(db_connection_with_data, "not_good_table_name")


def test_exception_read_only(
    db_connection_with_data_for_modif, table_objects
) -> SQLEnumerable:
    with faking_config({"whitelist": None, "blacklist": None, "readonly": True}):
        with pytest.raises(ReadOnlyPermissionDeniedError):
            SQLEnumerable(db_connection_with_data_for_modif, table_objects).delete(
                armageddon=True
            )
