# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import (
    ActionError,
    AlterError,
    CursorCloseError,
    DatabError,
    EmptyQueryError,
    EmptyRecordError,
    FetchError,
    GroupByWithJoinError,
    LengthMismatchError,
    NeedSelectError,
    NegativeNumberError,
    OneError,
    SelectError,
    SQLEnumerable,
    TerminalError,
    concat,
)
from py_linq_sql.utils.classes.magicdotpath import (
    AggregateType,
    MagicDotPath,
    MagicDotPathAggregate,
)
from py_linq_sql.utils.classes.other_classes import Flags, SQLEnumerableData
from py_linq_sql.utils.functions.aggregate_functions import (
    get_aggregate,
    get_one_aggregate,
)


def test_exception_execute_empty_request(db_connection_with_data, table_objects):
    r = SQLEnumerable(db_connection_with_data, table_objects).any()
    r.cmd = None
    with pytest.raises(EmptyQueryError):
        r.execute()


def test_exception_execute_without_action(db_connection_with_data, table_objects):
    with pytest.raises(ActionError):
        SQLEnumerable(db_connection_with_data, table_objects).order_by(
            lambda x: x.data.obj.name
        ).execute()


def test_exception_execute_wrong_data_request(db_connection_with_data, table_objects):
    with pytest.raises(DatabError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select(lambda toto: toto.a.b)
            .execute()
        )


@pytest.mark.skip(
    reason="We have no way to reproduce this specific error, "
    "the cursor is now created in the execute function"
)
def test_exception_execute_cursor_close(db_connection_with_data, table_objects):
    with pytest.raises(CursorCloseError):  # pragma: no cover
        SQLEnumerable(db_connection_with_data, table_objects).select().execute()


def test_exception_execute_element_at_index_error(
    db_connection_with_data, table_objects
):
    with pytest.raises(IndexError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select()
            .element_at(8)
            .execute()
        )


@pytest.mark.skip(reason="We have no way to reproduce this specific error")
def test_exception_execute_nothing_to_fetch_all(db_connection_with_data, table_objects):
    with pytest.raises(FetchError):  # pragma: no cover
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select()
            .where(lambda data: data.a.b == "toto")
            .execute()
        )


@pytest.mark.skip(reason="We have no way to reproduce this specific error")
def test_exception_execute_nothing_to_fetch_one(db_connection_with_data, table_objects):
    with pytest.raises(FetchError):  # pragma: no cover
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select()
            .where(lambda data: data.a.b == "toto")
            .element_at(0)
            .execute()
        )


def test_exception_insert_wrong_data_type(
    db_connection_with_data_for_modif, table_objects
):
    record = SQLEnumerable(db_connection_with_data_for_modif, table_objects).insert(
        "data",
        [12, [12]],
    )
    with pytest.raises(TypeError):
        record.execute()


def test_exception_join_length_mismatch(
    db_connection_with_data, table_objects, table_satellite
):

    se = SQLEnumerable(db_connection_with_data, table_satellite)

    record = SQLEnumerable(db_connection_with_data, table_objects).join(
        se,
        lambda objects: (objects.id, objects.data.obj.name),
        lambda satellite: satellite.data.obj.owner_id,
        lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
    )

    with pytest.raises(LengthMismatchError):
        record.execute()


def test_exception_mdp_with_op_wrong_type(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(lambda x: "toto" == 12)
    )

    with pytest.raises(TypeError):
        record.execute()


@pytest.mark.parametrize(
    "input_wrong_type_lambda",
    [
        param(lambda satellite, objects: (satellite.data.obj.name, 12), id="in tuple"),
        param(
            lambda satellite, objects: {"name": satellite.data.obj.name, "unknown": 12},
            id="in dict",
        ),
        param(
            lambda satellite, objects: [satellite.data.obj.name, 12], id="other type"
        ),
    ],
)
def test_exception_group_join_wrong_type_in_result_func_for_tuple(
    db_connection_with_data,
    table_objects,
    table_satellite,
    input_wrong_type_lambda,
):
    se = SQLEnumerable(db_connection_with_data, table_satellite)

    record = SQLEnumerable(db_connection_with_data, table_objects).group_join(
        se,
        lambda objects: objects.id,
        lambda satellite: satellite.data.obj.owner_id,
        input_wrong_type_lambda,
    )

    with pytest.raises(TypeError):
        record.execute()


def test_exception_record_is_none(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select()
        .where(lambda x: x.data.obj.mass < 0)
        .single()
    )

    with pytest.raises(EmptyRecordError):
        record.execute()


def test_exception_database_error_in_alter(
    db_connection_with_data_for_modif,
    table_objects,
):
    with pytest.raises(DatabError):
        SQLEnumerable(db_connection_with_data_for_modif, table_objects).insert(
            "toto", {"titi": "a"}
        ).execute()


def test_exception_database_error_in_one(db_connection_with_data, table_objects):
    with pytest.raises(DatabError):
        SQLEnumerable(db_connection_with_data, table_objects).all(
            lambda x: x.a.b
        ).execute()


@pytest.mark.parametrize(
    "input_args",
    [
        param((1, 2, 3), id="wrong type in tuple"),
        param(12, id="wrong type"),
    ],
)
def test_exception_get_aggregate_wrong_type(
    db_connection_with_data, table_objects, input_args
):
    sqle_d = SQLEnumerableData(
        db_connection_with_data, Flags(), [], table_objects, None
    )

    with pytest.raises(TypeError):
        get_aggregate(input_args, sqle_d)


def test_exception_get_one_aggregate_wrong_type(db_connection_with_data, table_objects):
    sqle_d = SQLEnumerableData(
        db_connection_with_data, Flags(), [], table_objects, None
    )

    mdpa = MagicDotPathAggregate(
        MagicDotPath(db_connection_with_data, ["data", "obj", "name"]),
        AggregateType.MAX,
        dict,
    )

    with pytest.raises(TypeError):
        get_one_aggregate(mdpa, dict, sqle_d)


def test_exception_alter_error(db_connection_with_data_for_modif, table_objects):
    record = SQLEnumerable(db_connection_with_data_for_modif, table_objects).delete(
        armageddon=True
    )

    with pytest.raises(AlterError):
        record.select()


def test_exception_group_by_with_join_error(
    db_connection_with_data,
    table_objects,
    table_satellite,
):
    se = SQLEnumerable(db_connection_with_data, table_satellite)

    record = SQLEnumerable(db_connection_with_data, table_objects).join(
        se,
        lambda objects: objects.id,
        lambda satellite: satellite.data.obj.owner_id,
        lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
    )

    with pytest.raises(GroupByWithJoinError):
        record.group_by(
            lambda x: x.data.obj.name, lambda x: concat(x.data.obj.name, ";")
        )


def test_exception_one_error(db_connection_with_data, table_objects):
    record = SQLEnumerable(db_connection_with_data, table_objects).any()

    with pytest.raises(OneError):
        record.select()


def test_exception_select_error(db_connection_with_data, table_objects):
    record = SQLEnumerable(db_connection_with_data, table_objects).select()

    with pytest.raises(SelectError):
        record.select()


def test_exception_terminal_error_in_check_terminal_alter(
    db_connection_with_data,
    table_objects,
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select()
        .max(lambda x: x.data.obj.mass)
    )

    with pytest.raises(TerminalError):
        record.max(lambda x: x.data.obj.mass)


def test_exception_alter_error_in_check_terminal_alter(
    db_connection_with_data_for_modif,
    table_objects,
):
    record = SQLEnumerable(db_connection_with_data_for_modif, table_objects).delete(
        armageddon=True
    )

    with pytest.raises(AlterError):
        record.max(lambda x: x.data.obj.mass)


def test_exception_intersect_without_select(db_connection_with_data, table_objects):
    se = SQLEnumerable(db_connection_with_data, table_objects)

    with pytest.raises(NeedSelectError):
        SQLEnumerable(db_connection_with_data, table_objects).intersect(se)


def test_exception_union_without_select(db_connection_with_data, table_objects):
    se = SQLEnumerable(db_connection_with_data, table_objects)

    with pytest.raises(NeedSelectError):
        SQLEnumerable(db_connection_with_data, table_objects).union(se)


def test_exception_skip_last_w_negative_number(db_connection_with_data, table_objects):
    with pytest.raises(NegativeNumberError):
        SQLEnumerable(db_connection_with_data, table_objects).select().skip_last(-1)


def test_exception_take_last_w_negative_number(db_connection_with_data, table_objects):
    with pytest.raises(NegativeNumberError):
        SQLEnumerable(db_connection_with_data, table_objects).select().take_last(-1)
