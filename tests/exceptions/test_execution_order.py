# Pytest imports
import pytest

# First party imports
from py_linq_sql import (
    MoreThanZeroError,
    NoMaxOrMinAfterLimitOffsetError,
    OtherThanWhereError,
    SQLEnumerable,
    TerminalError,
)


# TESTS Terminal command before an other command
def test_exception_first_before_X(db_connection_with_data, table_objects):
    with pytest.raises(TerminalError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .first()
            .where(lambda data: data.obj.name == "toto")
            .execute()
        )


def test_exception_element_at_before_X(db_connection_with_data, table_objects):
    with pytest.raises(TerminalError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .element_at(0)
            .where(lambda data: data.obj.name == "toto")
            .execute()
        )


def test_exception_max_before_X(db_connection_with_data, table_objects):
    with pytest.raises(TerminalError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .max(lambda data: data.obj.name)
            .where(lambda data: data.obj.name == "toto")
            .execute()
        )


def test_exception_min_before_X(db_connection_with_data, table_objects):
    with pytest.raises(TerminalError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .min(lambda data: data.obj.name)
            .where(lambda data: data.obj.name == "toto")
            .execute()
        )


# TESTS Update error sub commands
def test_exception_not_only_where_before_update(db_connection_with_data, table_objects):
    with pytest.raises(OtherThanWhereError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .where(lambda data: data.obj.name == "toto")
            .order_by(lambda data: data.obj.name)
            .update(lambda data: data.obj.mass == 666)
            .execute()
        )


# TEST Insert make other command before insert
def test_exception_command_before_insert(db_connection_with_data, table_objects):
    with pytest.raises(MoreThanZeroError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .where(lambda data: data.obj.name == "toto")
            .insert("data", {"obj": {"name": "titi", "mass": 12}})
            .execute()
        )


# TEST all make other command before all
def test_exception_command_before_all(db_connection_with_data, table_objects):
    with pytest.raises(MoreThanZeroError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .where(lambda data: data.obj.name == "toto")
            .all(lambda data: data.obj.name == "toto")
            .execute()
        )


# TEST max or min after a limit offset command
def test_exception_max_after_limit_offset(db_connection_with_data, table_objects):
    with pytest.raises(NoMaxOrMinAfterLimitOffsetError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .take(3)
            .max(lambda x: x.data.obj.name)
        )


def test_exception_min_after_limit_offset(db_connection_with_data, table_objects):
    with pytest.raises(NoMaxOrMinAfterLimitOffsetError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .skip_last(3)
            .min(lambda x: x.data.obj.name)
        )
