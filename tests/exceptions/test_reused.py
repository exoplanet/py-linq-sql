# Pytest imports
import pytest

# First party imports
from py_linq_sql import AlreadyExecutedError, SQLEnumerable


def test_re_use_sqle(db_connection_with_data, table_objects) -> SQLEnumerable:
    # Create an enumerable and do some request...
    toto = SQLEnumerable(db_connection_with_data, table_objects).select()

    # ...execute the request
    toto.execute()

    # We can not add some more request to the enumerable
    with pytest.raises(AlreadyExecutedError):
        toto.select()


def test_re_execute(db_connection_with_data, table_objects) -> SQLEnumerable:
    # Create an enumerable and do some request...
    toto = SQLEnumerable(db_connection_with_data, table_objects).select()

    # ...execute the request
    toto.execute()

    # We can not execute the enumerable a second time
    with pytest.raises(AlreadyExecutedError):
        toto.execute()
