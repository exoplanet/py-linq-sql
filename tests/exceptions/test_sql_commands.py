# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import (
    DeleteError,
    EmptyInputError,
    NeedSelectError,
    NeedWhereError,
    NegativeNumberError,
    SQLEnumerable,
    TableError,
    TooManyReturnValueError,
    TypeOperatorError,
)
from py_linq_sql.exception.exception import LengthMismatchError


def test_exception_number_operator(db_connection_with_data, table_objects):
    with pytest.raises(TypeOperatorError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select()
            .where(lambda data: data.obj.mass > "toto")
            .execute()
        )


def test_exception_generic_operator(db_connection_with_data, table_objects):
    with pytest.raises(TypeOperatorError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select()
            .where(lambda data: data.obj.mass != tuple(["a", "b"]))
            .execute()
        )


def test_exception_skip(db_connection_with_data, table_objects):
    with pytest.raises(NegativeNumberError):
        SQLEnumerable(db_connection_with_data, table_objects).skip(-1)


def test_exception_take(db_connection_with_data, table_objects):
    with pytest.raises(NegativeNumberError):
        SQLEnumerable(db_connection_with_data, table_objects).take(-1)


def test_exception_take_element_at(db_connection_with_data, table_objects):
    with pytest.raises(IndexError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select()
            .take(1)
            .element_at(1)
            .execute()
        )


def test_exception_update(db_connection_with_data, table_objects):
    with pytest.raises(TooManyReturnValueError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .update(lambda data: (data.obj.name == "toto", data.obj.mass == 666))
            .execute()
        )


def test_exception_min(db_connection_with_data, table_objects):
    with pytest.raises(TypeError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select()
            .min(lambda data: data.obj.mass, tuple)
            .execute()
        )


def test_exception_max(db_connection_with_data, table_objects):
    with pytest.raises(TypeError):
        (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select()
            .max(lambda data: data.obj.mass, tuple)
            .execute()
        )


def test_exception_element_at(db_connection_with_data, table_objects):
    with pytest.raises(NegativeNumberError):
        SQLEnumerable(db_connection_with_data, table_objects).element_at(-2)


def test_exception_contains(db_connection_with_data, table_objects):
    with pytest.raises(EmptyInputError):
        SQLEnumerable(db_connection_with_data, table_objects).contains({})


def test_exception_union(db_connection_with_data, table_objects):
    sqle_1 = SQLEnumerable(db_connection_with_data, table_objects).select()
    sqle_2 = SQLEnumerable(db_connection_with_data, table_objects).insert("toto", [])
    with pytest.raises(NeedSelectError):
        sqle_1.union(sqle_2).execute()


@pytest.mark.parametrize(
    "input_lambda_self, input_lambda_sqle",
    [
        param(None, lambda x: x.data.obj.name, id="exception union all and len = 1"),
        param(lambda x: x.data.obj.name, None, id="exception union len = 1 and all"),
        param(
            lambda x: (x.data.obj.name, x.data.obj.mass),
            lambda x: x.data.obj.name,
            id="exception union len = 2 and len = 1",
        ),
        param(
            lambda x: x.data.obj.name,
            lambda x: (x.data.obj.name, x.data.obj.mass),
            id="exception union len = 1 and len = 2",
        ),
    ],
)
def test_exception_union_len_mismatch(
    db_connection_with_data, table_objects, input_lambda_self, input_lambda_sqle
):
    sqle_1 = SQLEnumerable(db_connection_with_data, table_objects).select(
        input_lambda_self,
    )
    sqle_2 = SQLEnumerable(db_connection_with_data, table_objects).select(
        input_lambda_sqle,
    )
    with pytest.raises(LengthMismatchError):
        sqle_1.union(sqle_2).execute()


def test_exception_intersect(db_connection_with_data, table_objects):
    sqle_1 = SQLEnumerable(db_connection_with_data, table_objects).select()
    sqle_2 = SQLEnumerable(db_connection_with_data, table_objects).insert("toto", [])
    with pytest.raises(NeedSelectError):
        sqle_1.intersect(sqle_2).execute()


@pytest.mark.parametrize(
    "input_lambda_self, input_lambda_sqle",
    [
        param(
            None, lambda x: x.data.obj.name, id="exception intersect all and len = 1"
        ),
        param(
            lambda x: x.data.obj.name, None, id="exception intersect len = 1 and all"
        ),
        param(
            lambda x: (x.data.obj.name, x.data.obj.mass),
            lambda x: x.data.obj.name,
            id="exception intersect len = 2 and len = 1",
        ),
        param(
            lambda x: x.data.obj.name,
            lambda x: (x.data.obj.name, x.data.obj.mass),
            id="exception intersect len = 1 and len = 2",
        ),
    ],
)
def test_exception_intersect_len_mismatch(
    db_connection_with_data, table_objects, input_lambda_self, input_lambda_sqle
):
    sqle_1 = SQLEnumerable(db_connection_with_data, table_objects).select(
        input_lambda_self,
    )
    sqle_2 = SQLEnumerable(db_connection_with_data, table_objects).select(
        input_lambda_sqle,
    )
    with pytest.raises(LengthMismatchError):
        sqle_1.intersect(sqle_2).execute()


@pytest.mark.parametrize(
    "input_lambda_self, input_lambda_sqle",
    [
        param(None, lambda x: x.data.obj.name, id="exception except_ all and len = 1"),
        param(lambda x: x.data.obj.name, None, id="exception except_ len = 1 and all"),
        param(
            lambda x: (x.data.obj.name, x.data.obj.mass),
            lambda x: x.data.obj.name,
            id="exception except_ len = 2 and len = 1",
        ),
        param(
            lambda x: x.data.obj.name,
            lambda x: (x.data.obj.name, x.data.obj.mass),
            id="exception except_ len = 1 and len = 2",
        ),
    ],
)
def test_exception_except__len_mismatch(
    db_connection_with_data, table_objects, input_lambda_self, input_lambda_sqle
):
    sqle_1 = SQLEnumerable(db_connection_with_data, table_objects).select(
        input_lambda_self,
    )
    sqle_2 = SQLEnumerable(db_connection_with_data, table_objects).select(
        input_lambda_sqle,
    )
    with pytest.raises(LengthMismatchError):
        sqle_1.except_(sqle_2).execute()


def test_exception_delete_predicate_and_armageddon(
    db_connection_with_data_for_modif, table_objects
):
    sqle = SQLEnumerable(db_connection_with_data_for_modif, table_objects)
    with pytest.raises(DeleteError):
        sqle.delete(lambda x: x.data.obj.name, armageddon=True)


def test_exception_delete_where_and_armageddon(
    db_connection_with_data_for_modif, table_objects
):
    sqle = SQLEnumerable(db_connection_with_data_for_modif, table_objects).where(
        lambda x: x.data.obj.name
    )
    with pytest.raises(DeleteError):
        sqle.delete(armageddon=True).execute()


def test_exception_delete_no_where_no_armageddon(
    db_connection_with_data_for_modif, table_objects
):
    sqle = SQLEnumerable(db_connection_with_data_for_modif, table_objects)
    with pytest.raises(NeedWhereError):
        sqle.delete().execute()


def test_exception_wrong_table_name_after_join(
    db_connection_with_data, table_objects, table_satellite
):
    sqle_satellite = SQLEnumerable(db_connection_with_data, table_satellite)

    sqle = SQLEnumerable(db_connection_with_data, table_objects).join(
        sqle_satellite,
        lambda objects: objects.id,
        lambda satellite: satellite.data.obj.owner_id,
        lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
    )

    with pytest.raises(TableError):
        sqle.where(lambda x: x.toto.data.obj.name == "moon").execute()


def test_execution_lambda_without_mdp(db_connection_with_data, table_objects):
    sqle = SQLEnumerable(db_connection_with_data, table_objects).select(lambda x: 1 + 1)

    with pytest.raises(TypeError):
        sqle.get_command()


def test_where_with_dict_lambda_fails(db_connection_with_data, table_objects):
    sqle = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: {"toto": x.data.obj.name})
        .where(lambda x: {"toto": x.data.obj.name != "earth"})
    )

    with pytest.raises(TypeError):
        sqle.get_command()
