# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable as sqle


@pytest.mark.parametrize(
    "input_function",
    [
        (sqle.select_many),
        (sqle.to_list),
        (sqle.median),
        (sqle.aggregate),
        (sqle.append),
        (sqle.prepend),
        (sqle.empty),
        (sqle.range),
        (sqle.repeat),
        (sqle.reverse),
        (sqle.skip_while),
        (sqle.take_while),
        (sqle.zip),
        (sqle.default_if_empty),
    ],
)
def test_exception_sqle_not_implemented_methods(
    db_connection_with_data,
    table_objects,
    input_function,
):
    with pytest.raises(NotImplementedError):
        # HACK: this is a method call
        input_function(sqle(db_connection_with_data, table_objects))
