# Pytest imports
import pytest

# First party imports
from py_linq_sql.utils.functions.path_functions import get_path


@pytest.mark.parametrize(
    "input",
    [
        {"a": "aaa"},
        ["a", "b"],
        "toto",
        2,
        -2,
        2.0,
        -2.0,
        tuple({"a": "aaa"}),
        tuple(["a", "b"]),
    ],
)
def test_exception_get_path(input):
    with pytest.raises(TypeError):
        get_path(input)
