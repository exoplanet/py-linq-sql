# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: x.data.obj.name == "toto",
            """SELECT CASE WHEN ("""
            """(SELECT COUNT(*) FROM "objects" """
            """WHERE "data"->'obj'->>'name' = 'toto' ) """
            """= (SELECT COUNT(*) FROM "objects")) """
            'THEN 1 ELSE 0 END FROM "objects"',
        ),
        (
            lambda x: x.data.obj.mass >= 8,
            """SELECT CASE WHEN ("""
            """(SELECT COUNT(*) FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) >= 8 ) """
            """= (SELECT COUNT(*) FROM "objects")) """
            'THEN 1 ELSE 0 END FROM "objects"',
        ),
        (
            lambda x: (x.data.obj.mass >= 8, x.data.obj.name == "toto"),
            """SELECT CASE WHEN ("""
            """(SELECT COUNT(*) FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) >= 8 """
            """AND "data"->'obj'->>'name' = 'toto' ) """
            """= (SELECT COUNT(*) FROM "objects")) """
            'THEN 1 ELSE 0 END FROM "objects"',
        ),
    ],
)
def test_all_succeeds(db_connection_empty, table_objects, input_lambda, input_expected):
    request = (
        SQLEnumerable(db_connection_empty, table_objects)
        .all(input_lambda)
        .get_command()
    )
    assert request == input_expected
