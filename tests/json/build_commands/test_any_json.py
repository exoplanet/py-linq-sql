# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: x.data.obj.name == "toto",
            """SELECT * FROM "objects" WHERE "data"->'obj'->>'name' = 'toto'""",
        ),
        (
            lambda x: (x.data.obj.name == "toto", x.data.obj.mass > 0),
            """SELECT * FROM "objects" """
            """WHERE "data"->'obj'->>'name' = 'toto' """
            """AND CAST("data"->'obj'->'mass' AS decimal) > 0""",
        ),
        (
            lambda x: x.data.obj.mass >= 8,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) >= 8""",
        ),
        (None, '''SELECT * FROM "objects"'''),
    ],
)
def test_any_succeeds(db_connection_empty, table_objects, input_lambda, input_expected):
    request = (
        SQLEnumerable(db_connection_empty, table_objects)
        .any(input_lambda)
        .get_command()
    )
    assert request == input_expected
