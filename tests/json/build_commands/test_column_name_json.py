# First party imports
from py_linq_sql import SQLEnumerable, sqrt


def test_select_same_column_name(db_connection_with_data, table_objects):
    se = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(
            lambda x: (
                x.data.obj.name,
                x.data.obj.name,
                sqrt(x.data.obj.mass + 2),
                sqrt(x.data.obj.mass + 2),
            )
        )
        .execute()
    )

    assert se
