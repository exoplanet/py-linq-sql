# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "contains_input ,expected",
    [
        (
            lambda x: x.data.obj.name == "toto",
            """SELECT * FROM "objects" WHERE "data"->'obj'->>'name' = 'toto'""",
        ),
        (
            {"id": 50, "data": {"name": "toto", "mass": 12}},
            """SELECT * FROM "objects" """
            """WHERE id = 50 """
            """AND data = '{"name": "toto", "mass": 12}'""",
        ),
        (None, '''SELECT * FROM "objects"'''),
    ],
)
def test_contains_succeeds(
    db_connection_empty, table_objects, contains_input, expected
):
    request = (
        SQLEnumerable(db_connection_empty, table_objects)
        .contains(contains_input)
        .get_command()
    )
    assert request == expected
