# First party imports
from py_linq_sql import SQLEnumerable


def test_count_succeeds(db_connection_empty, table_objects):
    request = (
        SQLEnumerable(db_connection_empty, table_objects).select().count().get_command()
    )
    assert request == 'SELECT COUNT(*) FROM "objects"'
