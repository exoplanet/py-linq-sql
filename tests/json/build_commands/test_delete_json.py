# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_armageddon, input_expected",
    [
        (
            lambda x: x.data.obj.name == "toto",
            False,
            """DELETE FROM "objects" WHERE "data"->'obj'->>'name' = 'toto'""",
        ),
        (
            None,
            True,
            'DELETE FROM "objects"',
        ),
    ],
)
def test_delete_succeeds(
    db_connection_empty, table_objects, input_lambda, input_armageddon, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .delete(input_lambda, armageddon=input_armageddon)
        .get_command()
    )
    assert record == input_expected
