# First party imports
from py_linq_sql import SQLEnumerable


def test_distinct_succeeds(db_connection_empty, table_objects):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .select(lambda x: x.data.obj.name)
        .distinct()
        .get_command()
    )
    assert (
        record
        == '''SELECT DISTINCT "data"->'obj'->'name' AS data_obj_name FROM "objects"'''
    )
