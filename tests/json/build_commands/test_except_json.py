# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: x.data.obj.name == "toto",
            """SELECT * FROM "objects" EXCEPT SELECT * FROM "objects" """
            """WHERE "data"->'obj'->>'name' = 'toto'""",
        ),
        (
            lambda x: x.data.obj.mass >= 8,
            """SELECT * FROM "objects" EXCEPT SELECT * FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) >= 8""",
        ),
    ],
)
def test_except__succeeds(
    db_connection_empty, table_objects, input_lambda, input_expected
):
    se = SQLEnumerable(db_connection_empty, table_objects).select().where(input_lambda)
    request = (
        SQLEnumerable(db_connection_empty, table_objects)
        .select()
        .except_(se)
        .get_command()
    )

    assert request == input_expected
