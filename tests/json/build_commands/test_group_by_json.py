# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import SQLEnumerable, avg, concat, count, max, min, sum


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        param(
            lambda x: min(x.data.obj.mass),
            """SELECT "data"->'obj'->'name' AS data_obj_name , """
            """MIN(CAST("data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" GROUP BY "data"->'obj'->'name'""",
            id="test aggregate min",
        ),
        param(
            lambda x: max(x.data.obj.mass),
            """SELECT "data"->'obj'->'name' AS data_obj_name , """
            """MAX(CAST("data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" GROUP BY "data"->'obj'->'name'""",
            id="test aggregate max",
        ),
        param(
            lambda x: count(x.data.obj.mass),
            """SELECT "data"->'obj'->'name' AS data_obj_name , """
            """COUNT(CAST("data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" GROUP BY "data"->'obj'->'name'""",
            id="test aggregate count",
        ),
        param(
            lambda x: concat(x.data.obj.name, ";"),
            """SELECT "data"->'obj'->'name' AS data_obj_name , """
            """STRING_AGG("data"->'obj'->>'name', ';') """
            """FROM "objects" GROUP BY "data"->'obj'->'name'""",
            id="test aggregate concat",
        ),
        param(
            lambda x: (sum(x.data.obj.mass), avg(x.data.obj.mass)),
            """SELECT "data"->'obj'->'name' AS data_obj_name , """
            """SUM(CAST("data"->'obj'->'mass' AS decimal)) , """
            """AVG(CAST("data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" GROUP BY "data"->'obj'->'name'""",
            id="test aggregate sum and avg",
        ),
    ],
)
def test_group_by_succeeds(
    db_connection_empty, table_objects, input_lambda, input_expected
):
    request = (
        SQLEnumerable(db_connection_empty, table_objects)
        .group_by(lambda x: x.data.obj.name, input_lambda)
        .get_command()
    )

    assert request == input_expected
