# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import JoinType, SQLEnumerable, sum


@pytest.mark.parametrize(
    "input_lambda_1, input_lambda_2, input_lambda_3, input_join_type, input_expected",
    [
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (
                satellite.data.name,
                objects.data.name,
                sum(objects.data.obj.mass),
            ),
            JoinType.INNER,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name, """
            """SUM(CAST("objects"."data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" """
            """INNER JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """GROUP BY "satellite"."data"->'name', "objects"."data"->'name'""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (
                satellite.data.name,
                objects.data.name,
                sum(objects.data.obj.mass),
            ),
            JoinType.LEFT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name, """
            """SUM(CAST("objects"."data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" """
            """LEFT JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """GROUP BY "satellite"."data"->'name', "objects"."data"->'name'""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (
                satellite.data.name,
                objects.data.name,
                sum(objects.data.obj.mass),
            ),
            JoinType.LEFT_MINUS_INTERSECT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name, """
            """SUM(CAST("objects"."data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" """
            """LEFT JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """WHERE "satellite"."data"->'owner_id' IS NULL """
            """GROUP BY "satellite"."data"->'name', "objects"."data"->'name'""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (
                satellite.data.name,
                objects.data.name,
                sum(objects.data.obj.mass),
            ),
            JoinType.RIGHT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name, """
            """SUM(CAST("objects"."data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" """
            """RIGHT JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """GROUP BY "satellite"."data"->'name', "objects"."data"->'name'""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (
                satellite.data.name,
                objects.data.name,
                sum(objects.data.obj.mass),
            ),
            JoinType.RIGHT_MINUS_INTERSECT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name, """
            """SUM(CAST("objects"."data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" """
            """RIGHT JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """WHERE "satellite"."data"->'owner_id' IS NULL """
            """GROUP BY "satellite"."data"->'name', "objects"."data"->'name'""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (
                satellite.data.name,
                objects.data.name,
                sum(objects.data.obj.mass),
            ),
            JoinType.FULL,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name, """
            """SUM(CAST("objects"."data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" """
            """FULL JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """GROUP BY "satellite"."data"->'name', "objects"."data"->'name'""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (
                satellite.data.name,
                objects.data.name,
                sum(objects.data.obj.mass),
            ),
            JoinType.FULL_MINUS_INTERSECT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name, """
            """SUM(CAST("objects"."data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" """
            """FULL JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """WHERE "objects"."id" IS NULL """
            """OR "satellite"."data"->'owner_id' IS NULL """
            """GROUP BY "satellite"."data"->'name', "objects"."data"->'name'""",
        ),
        (
            lambda objects: (objects.id, objects.data.name),
            lambda satellite: (satellite.data.owner_id, satellite.data.owner_name),
            lambda satellite, objects: (
                satellite.data.name,
                objects.data.name,
                sum(objects.data.obj.mass),
            ),
            JoinType.INNER,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name, """
            """SUM(CAST("objects"."data"->'obj'->'mass' AS decimal)) """
            """FROM "objects" """
            """INNER JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """AND ("objects"."data"->'name')::text = """
            """("satellite"."data"->'owner_name')::text """
            """GROUP BY "satellite"."data"->'name', "objects"."data"->'name'""",
        ),
        param(
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: {
                "sat_name": satellite.data.name,
                "obj_name": objects.data.name,
                "sum_mass": sum(objects.data.obj.mass),
            },
            JoinType.FULL,
            """SELECT "satellite"."data"->'name' AS sat_name, """
            """"objects"."data"->'name' AS obj_name, """
            """SUM(CAST("objects"."data"->'obj'->'mass' AS decimal)) AS sum_mass """
            """FROM "objects" """
            """FULL JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """GROUP BY "satellite"."data"->'name', "objects"."data"->'name'""",
            id="group join with dict lambda",
        ),
    ],
)
def test_group_join_succeeds(
    db_connection_empty,
    table_objects,
    table_satellite,
    input_lambda_1,
    input_lambda_2,
    input_lambda_3,
    input_join_type,
    input_expected,
):
    se = SQLEnumerable(db_connection_empty, table_satellite)

    request = (
        SQLEnumerable(db_connection_empty, table_objects)
        .group_join(se, input_lambda_1, input_lambda_2, input_lambda_3, input_join_type)
        .get_command()
    )

    assert request == input_expected
