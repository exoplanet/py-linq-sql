# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_data, input_expected",
    [
        param(
            {"obj": {"name": "earth", "mass": 1}},
            r"""INSERT INTO "objects"( data ) """
            r"""VALUES ('{"obj": {"name": "earth", "mass": 1}}')""",
            id="insert json 1 obj",
        ),
        param(
            [
                {"obj": {"name": "venus", "mass": 2}},
                {"obj": {"name": "saturn", "mass": 3}},
            ],
            r"""INSERT INTO "objects"( data ) """
            r"""VALUES ('{"obj": {"name": "venus", "mass": 2}}'), """
            r"""('{"obj": {"name": "saturn", "mass": 3}}')""",
            id="insert json 2 obj",
        ),
    ],
)
def test_insert_succeeds(
    db_connection_empty, table_objects, input_data, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .insert("data", input_data)
        .get_command()
    )
    assert record == input_expected


@pytest.mark.parametrize(
    "input_data",
    [
        param(
            {"obj": {"name": "earth", "mass": [0, 1]}},
            id="insert json list in tuple",
        ),
        param(
            {"obj": {"name": "earth", "mass": (0, 1)}},
            id="insert json tuple in tuple",
        ),
    ],
)
def test_insert_array_succeeds(db_connection_empty, table_objects, input_data):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .insert("data", input_data)
        .get_command()
    )

    expected = (
        r"""INSERT INTO "objects"( data ) """
        r"""VALUES ('{"obj": {"name": "earth", "mass": [0, 1]}}')"""
    )

    assert record == expected


@pytest.mark.parametrize(
    "input_data",
    [
        param(
            {"obj": {"name": "earth", "mass": (0, (1, 2))}},
            id="insert json array tuple in tuple",
        ),
        param(
            {"obj": {"name": "earth", "mass": [0, [1, 2]]}},
            id="insert json array list in list",
        ),
        param(
            {"obj": {"name": "earth", "mass": (0, [1, 2])}},
            id="insert json array list in tuple",
        ),
        param(
            {"obj": {"name": "earth", "mass": [0, (1, 2)]}},
            id="insert json array tuple in list",
        ),
    ],
)
def test_insert_multidimensional_array_succeeds(
    db_connection_empty,
    table_objects,
    input_data,
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .insert("data", input_data)
        .get_command()
    )

    expected = (
        r"""INSERT INTO "objects"( data ) """
        r"""VALUES ('{"obj": {"name": "earth", "mass": [0, [1, 2]]}}')"""
    )

    assert record == expected
