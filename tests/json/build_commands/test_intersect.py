# First party imports
from py_linq_sql import SQLEnumerable


def test_simple_intersect_succeeds(db_connection_empty):
    se = SQLEnumerable(db_connection_empty, "satellite").select()

    record = (
        SQLEnumerable(db_connection_empty, "objects")
        .select()
        .intersect(se)
        .get_command()
    )

    assert record == 'SELECT * FROM "objects" INTERSECT SELECT * FROM "satellite"'


def test_intersect_succeeds(db_connection_empty):
    se = (
        SQLEnumerable(db_connection_empty, "satellite")
        .select()
        .where(lambda x: x.data.obj.name == "titi")
    )

    record = (
        SQLEnumerable(db_connection_empty, "objects")
        .where(lambda x: x.data.obj.name == "toto")
        .select()
        .intersect(se)
        .get_command()
    )

    assert (
        record == """SELECT * FROM "objects" WHERE "data"->'obj'->>'name' = 'toto' """
        """INTERSECT SELECT * FROM "satellite" WHERE "data"->'obj'->>'name' = 'titi'"""
    )
