# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import JoinType, SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda_1, input_lambda_2, input_lambda_3, input_join_type, input_expected",
    [
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (satellite.data.name, objects.data.name),
            JoinType.INNER,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name """
            """FROM "objects" """
            """INNER JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (satellite.data.name, objects.data.name),
            JoinType.LEFT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name """
            """FROM "objects" """
            """LEFT JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (satellite.data.name, objects.data.name),
            JoinType.LEFT_MINUS_INTERSECT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name """
            """FROM "objects" """
            """LEFT JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """WHERE "satellite"."data"->'owner_id' IS NULL""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (satellite.data.name, objects.data.name),
            JoinType.RIGHT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name """
            """FROM "objects" """
            """RIGHT JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (satellite.data.name, objects.data.name),
            JoinType.RIGHT_MINUS_INTERSECT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name """
            """FROM "objects" """
            """RIGHT JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """WHERE "satellite"."data"->'owner_id' IS NULL""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (satellite.data.name, objects.data.name),
            JoinType.FULL,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name """
            """FROM "objects" """
            """FULL JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            lambda satellite, objects: (satellite.data.name, objects.data.name),
            JoinType.FULL_MINUS_INTERSECT,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name """
            """FROM "objects" """
            """FULL JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """WHERE "objects"."id" IS NULL """
            """OR "satellite"."data"->'owner_id' IS NULL""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.owner_id,
            None,
            JoinType.INNER,
            """SELECT "objects".*, "satellite".* """
            """FROM "objects" """
            """INNER JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text""",
        ),
        (
            lambda objects: (objects.id, objects.data.name),
            lambda satellite: (satellite.data.owner_id, satellite.data.owner_name),
            lambda satellite, objects: (satellite.data.name, objects.data.name),
            JoinType.INNER,
            """SELECT "satellite"."data"->'name' AS satellite_data_name, """
            """"objects"."data"->'name' AS objects_data_name """
            """FROM "objects" """
            """INNER JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """AND ("objects"."data"->'name')::text = """
            """("satellite"."data"->'owner_name')::text""",
        ),
        param(
            lambda objects: (objects.id, objects.data.name),
            lambda satellite: (satellite.data.owner_id, satellite.data.owner_name),
            lambda satellite, objects: {
                "sat_name": satellite.data.name,
                "obj_name": objects.data.name,
            },
            JoinType.INNER,
            """SELECT "satellite"."data"->'name' AS sat_name, """
            """"objects"."data"->'name' AS obj_name """
            """FROM "objects" """
            """INNER JOIN "satellite" ON """
            """("objects"."id")::text = ("satellite"."data"->'owner_id')::text """
            """AND ("objects"."data"->'name')::text = """
            """("satellite"."data"->'owner_name')::text""",
            id="join with dict lambda",
        ),
    ],
)
def test_join_succeeds(
    db_connection_empty,
    table_objects,
    table_satellite,
    input_lambda_1,
    input_lambda_2,
    input_lambda_3,
    input_join_type,
    input_expected,
):
    se = SQLEnumerable(db_connection_empty, table_satellite)

    request = (
        SQLEnumerable(db_connection_empty, table_objects)
        .join(se, input_lambda_1, input_lambda_2, input_lambda_3, input_join_type)
        .get_command()
    )

    assert request == input_expected
