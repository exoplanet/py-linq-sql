# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_type ,input_expected",
    [
        (
            lambda x: x.data.obj.name,
            None,
            '''SELECT MAX("data"->'obj'->>'name') FROM "objects"''',
        ),
        (
            lambda x: x.data.obj.name,
            str,
            '''SELECT MAX("data"->'obj'->>'name') FROM "objects"''',
        ),
        (
            lambda x: x.data.obj.mass,
            int,
            '''SELECT MAX(CAST("data"->'obj'->'mass' AS decimal)) FROM "objects"''',
        ),
    ],
)
def test_max_succeeds(
    db_connection_empty, table_objects, input_lambda, input_type, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .select()
        .max(input_lambda, input_type)
        .get_command()
    )
    assert record == input_expected
