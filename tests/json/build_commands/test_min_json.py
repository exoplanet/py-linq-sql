# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_type, input_expected",
    [
        (
            lambda x: x.data.obj.name,
            None,
            '''SELECT MIN("data"->'obj'->>'name') FROM "objects"''',
        ),
        (
            lambda x: x.data.obj.name,
            str,
            '''SELECT MIN("data"->'obj'->>'name') FROM "objects"''',
        ),
        (
            lambda x: x.data.obj.mass,
            int,
            '''SELECT MIN(CAST("data"->'obj'->'mass' AS decimal)) FROM "objects"''',
        ),
    ],
)
def test_min_succeeds(
    db_connection_empty, table_objects, input_lambda, input_type, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .select()
        .min(input_lambda, input_type)
        .get_command()
    )
    assert record == input_expected
