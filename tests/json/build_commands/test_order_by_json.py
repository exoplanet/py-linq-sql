# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (
            lambda x: x.data.obj.name,
            """SELECT * FROM "objects" ORDER BY "data"->'obj'->'name' ASC""",
        ),
        (
            lambda x: (x.data.obj.name, x.data.obj.mass),
            """SELECT * FROM "objects" """
            """ORDER BY "data"->'obj'->'name' ASC, "data"->'obj'->'mass' ASC""",
        ),
    ],
)
def test_order_by_asc_succeeds(
    db_connection_empty, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .select()
        .order_by(input_lambda)
        .get_command()
    )
    assert record == input_expected


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (
            lambda x: x.data.obj.name,
            """SELECT * FROM "objects" ORDER BY "data"->'obj'->'name' DESC""",
        ),
        (
            lambda x: (x.data.obj.name, x.data.obj.mass),
            """SELECT * FROM "objects" """
            """ORDER BY "data"->'obj'->'name' DESC, "data"->'obj'->'mass' DESC""",
        ),
    ],
)
def test_order_by_desc_succeeds(
    db_connection_empty, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .select()
        .order_by_descending(input_lambda)
        .get_command()
    )
    assert record == input_expected
