# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        param(
            lambda x: x.data.obj.name,
            '''SELECT "data"->'obj'->'name' AS data_obj_name FROM "objects"''',
            id="select with lambda",
        ),
        param(None, '''SELECT * FROM "objects"''', id="select without lambda"),
        param(
            lambda x: {"toto": x.data.obj.name},
            '''SELECT "data"->'obj'->'name' AS toto FROM "objects"''',
            id="select with dict lambda",
        ),
    ],
)
def test_select_succeeds(
    db_connection_empty,
    table_objects,
    input_lambda,
    input_expected,
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .select(input_lambda)
        .get_command()
    )
    assert record == input_expected


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        param(
            None,
            '''SELECT * FROM "test_sc"."test"''',
            id="select without lambda in other than public schema",
        ),
        param(
            lambda x: x.data.name,
            '''SELECT "data"->'name' AS data_name FROM "test_sc"."test"''',
            id="select with lambda in other than public schema",
        ),
    ],
)
def test_select_with_schema_in_table_succeeds(
    db_connection_empty,
    table_test_schema,
    input_lambda,
    input_expected,
):
    record = (
        SQLEnumerable(db_connection_empty, table_test_schema)
        .select(input_lambda)
        .get_command()
    )
    assert record == input_expected
