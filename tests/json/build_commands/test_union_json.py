# First party imports
from py_linq_sql import SQLEnumerable


def test_simple_union_succeeds(db_connection_empty):
    se = SQLEnumerable(db_connection_empty, "satellite").select()

    record = (
        SQLEnumerable(db_connection_empty, "objects").select().union(se).get_command()
    )

    assert record == 'SELECT * FROM "objects" UNION SELECT * FROM "satellite"'


def test_union_succeeds(db_connection_empty):
    se = (
        SQLEnumerable(db_connection_empty, "satellite")
        .select()
        .where(lambda x: x.data.obj.name == "titi")
    )

    record = (
        SQLEnumerable(db_connection_empty, "objects")
        .where(lambda x: x.data.obj.name == "toto")
        .select()
        .union(se)
        .get_command()
    )

    assert (
        record == """SELECT * FROM "objects" WHERE "data"->'obj'->>'name' = 'toto' """
        """UNION SELECT * FROM "satellite" WHERE "data"->'obj'->>'name' = 'titi'"""
    )
