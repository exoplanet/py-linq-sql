# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (
            lambda x: x.data.obj.name == "toto",
            r"""UPDATE "objects" SET "data" = jsonb_set("""
            r""""data", '{obj,name}'::text[], ' "toto" ', false)""",
        ),
        (
            lambda x: x.data.obj.mass == 666,
            r"""UPDATE "objects" SET "data" = jsonb_set("""
            r""""data", '{obj,mass}'::text[], ' 666 ', false)""",
        ),
    ],
)
def test_update_succeeds(
    db_connection_empty, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .update(input_lambda)
        .get_command()
    )
    assert record == input_expected
