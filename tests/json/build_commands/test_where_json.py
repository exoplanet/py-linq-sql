# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: x.data.obj.name == "beta pic ter",
            """SELECT * FROM "objects" WHERE "data"->'obj'->>'name' = 'beta pic ter'""",
        ),
        (
            lambda x: x.data.obj.mass == 50,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) = 50""",
        ),
        (
            lambda x: x.data.obj.mass > 50,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) > 50""",
        ),
        (
            lambda x: x.data.obj.mass < 50,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) < 50""",
        ),
        (
            lambda x: x.data.obj.mass >= 50,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) >= 50""",
        ),
        (
            lambda x: x.data.obj.mass <= 50,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) <= 50""",
        ),
        (
            lambda x: x.data.obj.mass > 50.2,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'obj'->'mass' AS decimal) > 50.2""",
        ),
        (
            lambda x: x.data.obj.name != "beta pic",
            """SELECT * FROM "objects" """
            """WHERE "data"->'obj'->>'name' <> 'beta pic'""",
        ),
        (
            lambda x: (x.data.obj.name != "beta pic", x.data.obj.mass > 0),
            """SELECT * FROM "objects" """
            """WHERE "data"->'obj'->>'name' <> 'beta pic' """
            """AND CAST("data"->'obj'->'mass' AS decimal) > 0""",
        ),
    ],
)
def test_where_succeeds(
    db_connection_empty, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .select()
        .where(input_lambda)
        .get_command()
    )
    assert record == input_expected


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        param(
            lambda x: x.data.refereed == True,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'refereed' AS boolean) = True""",
            id="equal True",
        ),
        param(
            lambda x: x.data.refereed == False,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'refereed' AS boolean) = False""",
            id="equal False",
        ),
        param(
            lambda x: x.data.refereed != True,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'refereed' AS boolean) <> True""",
            id="not equal True",
        ),
        param(
            lambda x: x.data.refereed != False,
            """SELECT * FROM "objects" """
            """WHERE CAST("data"->'refereed' AS boolean) <> False""",
            id="not equal False",
        ),
    ],
)
def test_where_succeeds_with_boolean(
    db_connection_empty, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty, table_objects)
        .select()
        .where(input_lambda)
        .get_command()
    )
    assert record == input_expected
