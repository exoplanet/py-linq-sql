# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.data.obj.mass > 0, True),
        (lambda x: x.data.obj.name == "toto", False),
        (lambda x: (x.data.obj.name == "toto", x.data.obj.mass > 0), False),
        (lambda x: (x.data.obj.mass != 0, x.data.obj.mass > 0), True),
    ],
)
def test_execute_all(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .all(input_lambda)
        .execute()
    )
    assert record == input_expected


def test_execute_all_on_empty_db(db_connection_with_only_schema, table_objects):
    record = (
        SQLEnumerable(db_connection_with_only_schema, table_objects)
        .all(lambda x: x.data.obj.name == "toto")
        .execute()
    )
    assert not record
