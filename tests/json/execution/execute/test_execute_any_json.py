# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.data.obj.mass < 0, False),
        (lambda x: x.data.obj.mass > 0, True),
        (lambda x: x.data.obj.name == "earth", True),
        (lambda x: x.data.obj.name == "toto", False),
        (lambda x: (x.data.obj.name == "toto", x.data.obj.mass > 0), False),
        (lambda x: (x.data.obj.name == "saturn", x.data.obj.mass > 0), True),
        (None, True),
    ],
)
def test_execute_any(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .any(input_lambda)
        .execute()
    )
    assert record == input_expected


def test_execute_any_on_empty_db(db_connection_with_only_schema, table_objects):
    record = (
        SQLEnumerable(db_connection_with_only_schema, table_objects).any().execute()
    )
    assert not record
