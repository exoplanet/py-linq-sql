# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input, input_expected",
    [
        (lambda x: x.data.obj.mass < 0, False),
        (lambda x: x.data.obj.mass > 0, True),
        (
            {
                "id": 1,
                "data": {
                    "obj": {"name": "earth", "mass": 1},
                    "toto": "toto",
                    "refereed": False,
                },
            },
            True,
        ),
        (
            {
                "id": 18,
                "data": {
                    "obj": {"name": "earth", "mass": 1},
                    "toto": "toto",
                    "refereed": True,
                },
            },
            False,
        ),
        (None, True),
    ],
)
def test_execute_contains(
    db_connection_with_data, table_objects, input, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects).contains(input).execute()
    )
    assert record == input_expected
