# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


def test_execute_count(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects).select().count().execute()
    )

    assert_that(record.to_list()).is_equal_to([(3,)])


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.data.obj.mass > 10, [(2,)]),
        (lambda x: x.data.obj.name == "earth", [(1,)]),
    ],
)
def test_execute_count_where(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select()
        .where(input_lambda)
        .count()
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(input_expected)
