# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


def test_execute_delete_w_predicate(db_connection_with_data_for_modif, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .delete(lambda x: x.data.obj.name == "earth")
        .execute()
    )

    check = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .select(lambda x: x.data.obj.name)
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check).contains_only(*[("saturn",), ("jupiter",)])


def test_execute_delete_w_where(db_connection_with_data_for_modif, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .delete()
        .where(lambda x: x.data.obj.name == "earth")
        .execute()
    )

    check = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .select(lambda x: x.data.obj.name)
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check).contains_only(*[("saturn",), ("jupiter",)])


def test_execute_delete_armageddon(db_connection_with_data_for_modif, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .delete(armageddon=True)
        .execute()
    )

    check = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .select(lambda x: x.data.obj.name)
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(3)
        assert_that(check).is_empty()
