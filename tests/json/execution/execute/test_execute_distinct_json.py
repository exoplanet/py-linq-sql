# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


def _db_add_duplicate(connection, table) -> None:
    SQLEnumerable(connection, table).insert(
        "data", {"obj": {"name": "earth", "mass": 1}}
    ).execute()


def test_execute_distinct(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .distinct()
        .execute()
    )

    assert_that(record.to_list()).contains_only(
        *[("earth",), ("saturn",), ("jupiter",)]
    )


def test_execute_distinct_with_duplicate(
    db_connection_with_data_for_modif, table_objects
):
    _db_add_duplicate(db_connection_with_data_for_modif, table_objects)

    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .select(lambda x: x.data.obj.name)
        .distinct()
        .execute()
    )

    assert_that(record).contains_only(*[("earth",), ("saturn",), ("jupiter",)])
