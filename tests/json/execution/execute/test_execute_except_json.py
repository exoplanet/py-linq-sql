# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_lambda_where, input_expected",
    [
        param(
            lambda x: x.data.obj.name,
            lambda x: x.data.obj.mass > 80,
            [("earth",), ("saturn",)],
            id="except select 1 where int",
        ),
        param(
            lambda x: x.data.obj.name,
            lambda x: x.data.obj.name == "earth",
            [("saturn",), ("jupiter",)],
            id="except select 1 where str",
        ),
        param(
            lambda x: (x.data.obj.name, x.data.obj.mass),
            lambda x: x.data.obj.mass > 80,
            [("earth", 1), ("saturn", 52)],
            id="except select 2",
        ),
        param(
            None,
            lambda x: x.data.obj.mass > 80,
            [
                (
                    1,
                    {
                        "obj": {"mass": 1, "name": "earth"},
                        "toto": "toto",
                        "refereed": False,
                    },
                ),
                (
                    2,
                    {
                        "obj": {"mass": 52, "name": "saturn"},
                        "toto": "toto",
                        "refereed": True,
                    },
                ),
            ],
            id="except select all",
        ),
    ],
)
def test_execute_except_(
    db_connection_with_data,
    table_objects,
    input_lambda,
    input_lambda_where,
    input_expected,
):
    se = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(input_lambda)
        .where(input_lambda_where)
    )

    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(input_lambda)
        .except_(se)
        .execute()
    )
    assert_that(record.to_list()).contains_only(*input_expected)


def test_execute_except__with_select_after(
    db_connection_with_data,
    table_objects,
):
    se = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(lambda x: x.data.obj.mass > 80)
    )

    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .except_(se)
        .select(lambda x: x.data.obj.name)
        .execute()
    )
    assert_that(record.to_list()).contains_only(*[("earth",), ("saturn",)])
