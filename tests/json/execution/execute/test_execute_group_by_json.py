# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable, avg, concat, count, max, min, sum


def _db_add_duplicate(connection, table) -> None:
    SQLEnumerable(connection, table).insert(
        "data", {"obj": {"name": "earth", "mass": 1}}
    ).execute()


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: min(x.data.obj.mass),
            [
                ("earth", 1.0),
                ("jupiter", 100.0),
                ("saturn", 52.0),
            ],
        ),
        (
            lambda x: max(x.data.obj.mass),
            [
                ("earth", 1.0),
                ("jupiter", 100.0),
                ("saturn", 52.0),
            ],
        ),
        (
            lambda x: count(x.data.obj.mass),
            [
                ("earth", 1),
                ("jupiter", 1),
                ("saturn", 1),
            ],
        ),
        (
            lambda x: (sum(x.data.obj.mass), avg(x.data.obj.mass)),
            [
                ("earth", 1, 1),
                ("jupiter", 100, 100),
                ("saturn", 52, 52),
            ],
        ),
    ],
)
def test_execute_group_by(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .group_by(lambda x: x.data.obj.name, input_lambda)
        .execute()
    )

    assert_that(record.to_list()).contains_only(*input_expected)


def test_execute_group_by_concat(db_connection_with_data_for_modif, table_objects):
    _db_add_duplicate(db_connection_with_data_for_modif, table_objects)

    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .group_by(lambda x: x.data.obj.name, lambda x: concat(x.data.obj.name, ";"))
        .execute()
    )

    assert_that(record).contains_only(
        *[
            ("earth", "earth;earth"),
            ("jupiter", "jupiter"),
            ("saturn", "saturn"),
        ]
    )
