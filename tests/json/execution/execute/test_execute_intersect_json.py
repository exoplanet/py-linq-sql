# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        param(
            lambda x: x.data.obj.name,
            [("earth",), ("jupiter",), ("saturn",)],
            id="intersect select 1",
        ),
        param(
            lambda x: (x.data.obj.name, x.data.obj.mass),
            [("earth", 1), ("jupiter", 100), ("saturn", 52)],
            id="intersect select 2",
        ),
        param(
            None,
            [
                (
                    1,
                    {
                        "obj": {"mass": 1, "name": "earth"},
                        "toto": "toto",
                        "refereed": False,
                    },
                ),
                (
                    3,
                    {
                        "obj": {"mass": 100, "name": "jupiter"},
                        "toto": "toto",
                        "refereed": True,
                    },
                ),
                (
                    2,
                    {
                        "obj": {"mass": 52, "name": "saturn"},
                        "toto": "toto",
                        "refereed": True,
                    },
                ),
            ],
            id="intersect select all",
        ),
    ],
)
def test_execute_simple_intersect(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    se = SQLEnumerable(db_connection_with_data, table_objects).select(input_lambda)

    record = (
        SQLEnumerable(db_connection_with_data, "objects")
        .select(input_lambda)
        .intersect(se)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda_1, input_lambda_2, input_expected",
    [
        (
            lambda x: x.data.obj.name != "jupiter",
            lambda x: x.data.obj.name != "saturn",
            [("earth",)],
        ),
        (
            lambda x: x.data.obj.name != "jupiter",
            lambda x: x.data.obj.name != "jupiter",
            [("earth",), ("saturn",)],
        ),
    ],
)
def test_execute_intersect(
    db_connection_with_data, input_lambda_1, input_lambda_2, input_expected
):
    se = (
        SQLEnumerable(db_connection_with_data, "objects")
        .select(lambda x: x.data.obj.name)
        .where(input_lambda_1)
    )

    record = (
        SQLEnumerable(db_connection_with_data, "objects")
        .select(lambda x: x.data.obj.name)
        .where(input_lambda_2)
        .intersect(se)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
