# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import JoinType, SQLEnumerable


@pytest.mark.parametrize(
    "input_inner_key, input_outer_key, input_res_function, input_type, input_expected",
    [
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
            JoinType.INNER,
            [("moon", "earth"), ("europe", "jupiter"), ("callisto", "jupiter")],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
            JoinType.LEFT,
            [
                ("moon", "earth"),
                ("europe", "jupiter"),
                ("callisto", "jupiter"),
                (None, "saturn"),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
            JoinType.LEFT_MINUS_INTERSECT,
            [(None, "saturn")],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
            JoinType.RIGHT,
            [
                ("moon", "earth"),
                ("europe", "jupiter"),
                ("callisto", "jupiter"),
                ("alone", None),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
            JoinType.RIGHT_MINUS_INTERSECT,
            [("alone", None)],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
            JoinType.FULL,
            [
                ("moon", "earth"),
                ("europe", "jupiter"),
                ("callisto", "jupiter"),
                (None, "saturn"),
                ("alone", None),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
            JoinType.FULL_MINUS_INTERSECT,
            [(None, "saturn"), ("alone", None)],
        ),
    ],
)
def test_execute_join(
    db_connection_with_data,
    table_objects,
    table_satellite,
    input_inner_key,
    input_outer_key,
    input_res_function,
    input_type,
    input_expected,
):
    se = SQLEnumerable(db_connection_with_data, table_satellite)

    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .join(se, input_inner_key, input_outer_key, input_res_function, input_type)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_where, input_expected",
    [
        (lambda x: x.satellite.data.obj.name == "moon", [("moon", "earth")]),
        (
            lambda x: x.objects.data.obj.mass == x.objects.data.obj.mass,
            [("moon", "earth"), ("europe", "jupiter"), ("callisto", "jupiter")],
        ),
    ],
)
def test_execute_join_w_where(
    db_connection_with_data,
    table_objects,
    table_satellite,
    input_where,
    input_expected,
):
    sqle_satellite = SQLEnumerable(db_connection_with_data, table_satellite)

    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .join(
            sqle_satellite,
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
        )
        .where(input_where)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


def test_execute_join__in_a_schema_w_where(
    db_connection_with_data,
    table_test_schema,
    table_satellite,
):
    sqle_satellite = SQLEnumerable(db_connection_with_data, table_satellite)

    record = (
        SQLEnumerable(db_connection_with_data, table_test_schema)
        .join(
            sqle_satellite,
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: {
                "sat_name": satellite.data.obj.name,
                "obj_name": objects.data.obj.name,
            },
        )
        .where(lambda x: x.test.data.obj.name == "earth")
        .execute()
    )

    assert_that(record).contains_only(*[("moon", "earth")])


def test_execute_join_w_order_by(
    db_connection_with_data, table_objects, table_satellite
):
    sqle_satellite = SQLEnumerable(db_connection_with_data, table_satellite)

    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .join(
            sqle_satellite,
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
        )
        .order_by(lambda x: x.objects.data.obj.mass)
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(
        [("moon", "earth"), ("europe", "jupiter"), ("callisto", "jupiter")]
    )


def test_execute_join_w_take(db_connection_with_data, table_objects, table_satellite):
    sqle_satellite = SQLEnumerable(db_connection_with_data, table_satellite)

    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .join(
            sqle_satellite,
            lambda objects: objects.id,
            lambda satellite: satellite.data.obj.owner_id,
            lambda satellite, objects: (satellite.data.obj.name, objects.data.obj.name),
        )
        .take(1)
        .execute()
    )

    assert_that(record).contains_only(*[("moon", "earth")])
