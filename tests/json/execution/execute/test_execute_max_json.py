# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_type ,input_expected",
    [
        (lambda x: x.data.obj.name, None, [("saturn",)]),
        (lambda x: x.data.obj.name, str, [("saturn",)]),
        (lambda x: x.data.obj.mass, int, [(100,)]),
    ],
)
def test_execute_max(
    db_connection_with_data, table_objects, input_lambda, input_type, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .max(input_lambda, input_type)
        .select(lambda x: x.data.obj.name)
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(input_expected)
