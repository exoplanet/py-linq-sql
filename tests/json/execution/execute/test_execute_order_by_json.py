# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.data.obj.name, [("earth", 1), ("jupiter", 100), ("saturn", 52)]),
        (lambda x: x.data.obj.mass, [("earth", 1), ("saturn", 52), ("jupiter", 100)]),
    ],
)
def test_execute_order_by(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .order_by(input_lambda)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.data.obj.name, [("saturn", 52), ("jupiter", 100), ("earth", 1)]),
        (lambda x: x.data.obj.mass, [("jupiter", 100), ("saturn", 52), ("earth", 1)]),
    ],
)
def test_execute_order_by_descending(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .order_by_descending(input_lambda)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
