# Third party imports
from py_linq import Enumerable

# First party imports
from py_linq_sql import SQLEnumerable


def test_return_type_select(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .execute()
    )

    first = record[0]

    assert first.data_obj_name


def test_return_type_max(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .max(lambda x: x.data.obj.name)
        .execute()
    )

    first = record[0]

    assert first.max


def test_return_type_count(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .count()
        .execute()
    )

    first = record[0]

    assert first.count


def test_return_type_execute_int(
    db_connection_with_only_schema_for_modif, table_objects
):
    record = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .insert("data", {"obj": {"name": "earth", "mass": 1}})
        .execute()
    )

    assert isinstance(record, int)


def test_return_type_execute_Enumerable(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(lambda x: x.data.obj.mass > 50)
        .execute()
    )

    assert isinstance(record, Enumerable)


def test_return_type_execute_Enuemrable_terminal(
    db_connection_with_data, table_objects
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .element_at(1)
        .execute()
    )

    assert isinstance(record, Enumerable)


def test_return_type_execute_bool(db_connection_with_data, table_objects):
    record = SQLEnumerable(db_connection_with_data, table_objects).any().execute()

    assert isinstance(record, bool)
