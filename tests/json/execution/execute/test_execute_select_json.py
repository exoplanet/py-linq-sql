# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        param(
            lambda x: x.data.obj.name,
            [("earth",), ("saturn",), ("jupiter",)],
            id="Select 1 param",
        ),
        param(
            lambda x: (x.data.obj.name, x.data.obj.mass),
            [("earth", 1), ("saturn", 52), ("jupiter", 100)],
            id="select more then 1 param",
        ),
        param(
            None,
            [
                (
                    1,
                    {
                        "obj": {"mass": 1, "name": "earth"},
                        "toto": "toto",
                        "refereed": False,
                    },
                ),
                (
                    2,
                    {
                        "obj": {"mass": 52, "name": "saturn"},
                        "toto": "toto",
                        "refereed": True,
                    },
                ),
                (
                    3,
                    {
                        "obj": {"mass": 100, "name": "jupiter"},
                        "toto": "toto",
                        "refereed": True,
                    },
                ),
            ],
            id="select all",
        ),
    ],
)
def test_execute_select(
    db_connection_with_data,
    table_objects,
    input_lambda,
    input_expected,
):
    if not input_lambda:
        record = (
            SQLEnumerable(db_connection_with_data, table_objects).select().execute()
        )
    else:
        record = (
            SQLEnumerable(db_connection_with_data, table_objects)
            .select(input_lambda)
            .execute()
        )

    assert_that(record).contains_only(*input_expected)


def test_execute_select_with_schema(db_connection_with_data, table_test_schema):
    record = (
        SQLEnumerable(db_connection_with_data, table_test_schema)
        .select(lambda x: x.data.obj.name)
        .execute()
    )
    assert_that(record).contains_only(*[("earth",)])


def test_execute_select_only_column(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data)
        .execute()
    )

    assert record
