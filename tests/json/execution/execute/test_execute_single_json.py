# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.data.obj.mass == 1, [({"mass": 1, "name": "earth"},)]),
        (
            lambda x: x.data.obj.name == "jupiter",
            [({"mass": 100, "name": "jupiter"},)],
        ),
    ],
)
def test_execute_single(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj)
        .single(input_lambda)
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(input_expected)


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.data.obj.mass == 1, [({"mass": 1, "name": "earth"},)]),
        (
            lambda x: x.data.obj.name == "jupiter",
            [({"mass": 100, "name": "jupiter"},)],
        ),
    ],
)
def test_execute_single_or_default(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj)
        .single_or_default(input_lambda)
        .execute()
    )
    assert_that(record.to_list()).is_equal_to(input_expected)


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.data.obj.name == "toto", None),
        (lambda x: x.data.obj.mass >= 1, None),
    ],
)
def test_execute_single_or_default_none(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj)
        .single_or_default(input_lambda)
        .execute()
    )

    assert not record
