# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_start,  input_expected",
    [
        (0, [("earth",), ("saturn",), ("jupiter",)]),
        (1, [("saturn",), ("jupiter",)]),
        (2, [("jupiter",)]),
    ],
)
def test_execute_skip(
    db_connection_with_data, table_objects, input_start, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .skip(input_start)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


def test_execute_skip_all(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects).select().skip(3).execute()
    )

    assert record.to_list() == []


def test_execute_skip_skip(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .skip(1)
        .skip(1)
        .execute()
    )

    assert_that(record).contains_only(*[("jupiter",)])
