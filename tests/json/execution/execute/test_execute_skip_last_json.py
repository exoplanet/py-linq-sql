# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


def test_execute_skip_last_all(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .skip_last(3)
        .execute()
    )

    assert_that(record).is_empty()


@pytest.mark.parametrize(
    "input_n, input_expected",
    [
        (1, [("earth",), ("saturn",)]),
        (2, [("earth",)]),
    ],
)
def test_execute_skip_last(
    db_connection_with_data, table_objects, input_n, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .skip_last(input_n)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


def test_execute_skip_skip_last(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .skip(1)
        .skip_last(1)
        .select(lambda x: x.data.obj.name)
        .execute()
    )
    assert_that(record).contains_only(*[("saturn",)])
