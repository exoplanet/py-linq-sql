# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_n, input_expected",
    [
        (1, [("earth",)]),
        (2, [("earth",), ("saturn",)]),
        (3, [("earth",), ("saturn",), ("jupiter",)]),
    ],
)
def test_execute_take(db_connection_with_data, table_objects, input_n, input_expected):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .take(input_n)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


def test_execute_take_0(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .take(0)
        .execute()
    )

    assert_that(record).is_empty()


@pytest.mark.parametrize(
    "input_1, input_2, input_expected",
    [
        (1, 1, [("earth",)]),
        (5, 2, [("earth",), ("saturn",)]),
    ],
)
def test_execute_take_take(
    db_connection_with_data, table_objects, input_1, input_2, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .take(input_1)
        .take(input_2)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
