# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda_self, input_lambda_sqle, input_expected",
    [
        param(
            lambda x: x.data.obj.name,
            lambda x: x.data.obj.name,
            [
                ("earth",),
                ("jupiter",),
                ("saturn",),
                ("moon",),
                ("europe",),
                ("callisto",),
                ("alone",),
            ],
            id="union select 1",
        ),
        param(
            lambda x: (x.data.obj.name, x.data.obj.mass),
            lambda x: (x.data.obj.name, x.data.obj.owner_id),
            [
                ("earth", 1),
                ("jupiter", 100),
                ("saturn", 52),
                ("moon", 1),
                ("alone", None),
                ("callisto", 3),
                ("europe", 3),
            ],
            id="union select 2",
        ),
        param(
            None,
            None,
            [
                (
                    1,
                    {
                        "obj": {"mass": 1, "name": "earth"},
                        "toto": "toto",
                        "refereed": False,
                    },
                ),
                (4, {"obj": {"name": "alone"}}),
                (2, {"obj": {"name": "europe", "owner_id": 3}}),
                (3, {"obj": {"name": "callisto", "owner_id": 3}}),
                (1, {"obj": {"name": "moon", "owner_id": 1}}),
                (
                    3,
                    {
                        "obj": {"mass": 100, "name": "jupiter"},
                        "toto": "toto",
                        "refereed": True,
                    },
                ),
                (
                    2,
                    {
                        "obj": {"mass": 52, "name": "saturn"},
                        "toto": "toto",
                        "refereed": True,
                    },
                ),
            ],
            id="union select all",
        ),
    ],
)
def test_execute_simple_union(
    db_connection_with_data,
    table_objects,
    table_satellite,
    input_lambda_self,
    input_lambda_sqle,
    input_expected,
):
    se = SQLEnumerable(db_connection_with_data, table_satellite).select(
        input_lambda_sqle
    )

    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(input_lambda_self)
        .union(se)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda_1, input_lambda_2, input_expected",
    [
        (
            lambda x: x.data.obj.name != "alone",
            lambda x: x.data.obj.name != "saturn",
            [("moon",), ("europe",), ("callisto",), ("earth",), ("jupiter",)],
        ),
        (
            lambda x: x.data.obj.name == "moon",
            lambda x: x.data.obj.name == "earth",
            [("moon",), ("earth",)],
        ),
    ],
)
def test_execute_union(
    db_connection_with_data, input_lambda_1, input_lambda_2, input_expected
):
    se = (
        SQLEnumerable(db_connection_with_data, "satellite")
        .select(lambda x: x.data.obj.name)
        .where(input_lambda_1)
    )

    record = (
        SQLEnumerable(db_connection_with_data, "objects")
        .select(lambda x: x.data.obj.name)
        .where(input_lambda_2)
        .union(se)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
