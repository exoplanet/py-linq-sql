# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "update_lambda, expected_update_result, expected_select_result",
    [
        pytest.param(
            lambda x: x.data.obj.name == "earth2", 1, [("earth2",)], id="name = earth2"
        ),
        pytest.param(
            lambda x: x.data.obj.mass == 666, 1, [("earth",)], id="mass = 666"
        ),
    ],
)
def test_execute_update(
    db_connection_with_data_for_modif,
    table_objects,
    update_lambda,
    expected_update_result,
    expected_select_result,
):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .update(update_lambda)
        .where(lambda x: x.data.obj.mass < 10)
        .execute()
    )

    check = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(update_lambda)
        .execute()
    )
    with soft_assertions():
        assert_that(record).is_equal_to(expected_update_result)
        assert_that(check).contains_only(*expected_select_result)
