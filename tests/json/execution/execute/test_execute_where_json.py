# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (lambda x: x.data.obj.name == "earth", [("earth",)]),
        (lambda x: x.data.obj.mass == 1, [("earth",)]),
        (lambda x: x.data.obj.mass > 50, [("saturn",), ("jupiter",)]),
        (lambda x: x.data.obj.mass < 50, [("earth",)]),
        (lambda x: x.data.obj.mass >= 52, [("saturn",), ("jupiter",)]),
        (lambda x: x.data.obj.mass <= 52, [("earth",), ("saturn",)]),
        (lambda x: x.data.obj.mass != 42.42, [("earth",), ("saturn",), ("jupiter",)]),
        param(
            lambda x: x.data.obj.mass != [1, 2, 3],
            [("earth",), ("saturn",), ("jupiter",)],
            id="Where with list",
        ),
        param(
            lambda x: x.data.obj.mass != {"titi": "a", "toto": "b"},
            [("earth",), ("saturn",), ("jupiter",)],
            id="Where with dict",
        ),
        (lambda x: x.data.obj.mass > 50.2, [("saturn",), ("jupiter",)]),
        (lambda x: x.data.obj.name != "saturn", [("earth",), ("jupiter",)]),
        (
            lambda x: (x.data.obj.name != "saturn", x.data.obj.mass > 0),
            [("earth",), ("jupiter",)],
        ),
        param(
            lambda x: x.data.toto == "toto",
            [("earth",), ("saturn",), ("jupiter",)],
            id="1 level of nesting",
        ),
    ],
)
def test_execute_where(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(input_lambda)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        param(
            lambda x: x.data.refereed == True,
            [("saturn",), ("jupiter",)],
            id="equal True",
        ),
        param(lambda x: x.data.refereed == False, [("earth",)], id="equal False"),
        param(lambda x: x.data.refereed != True, [("earth",)], id="not equal True"),
        param(
            lambda x: x.data.refereed != False,
            [("saturn",), ("jupiter",)],
            id="not equal False",
        ),
    ],
)
def test_execute_where_with_boolean(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(input_lambda)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda_1, input_lambda_2, input_expected",
    [
        (
            lambda x: x.data.obj.name == "earth",
            lambda x: x.data.obj.mass <= 50,
            [("earth",)],
        ),
        (
            lambda x: x.data.obj.name != "earth",
            lambda x: x.data.obj.mass > 32.2,
            [("saturn",), ("jupiter",)],
        ),
        (
            lambda x: x.data.obj.mass > 0,
            lambda x: x.data.obj.name != "beta pic",
            [("earth",), ("saturn",), ("jupiter",)],
        ),
    ],
)
def test_execute_multi_where(
    db_connection_with_data,
    table_objects,
    input_lambda_1,
    input_lambda_2,
    input_expected,
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(input_lambda_1)
        .where(input_lambda_2)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
