# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


def test_simple_req_in_req(db_connection_with_data, table_objects):
    r_one = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select()
        .where(lambda x: x.data.obj.mass > 32)
        .order_by(lambda x: x.data.obj.name)
    )

    r_two = (
        SQLEnumerable(db_connection_with_data, r_one)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .execute()
    )

    assert_that(r_two).contains_only(*[("saturn", 52), ("jupiter", 100)])
