# First party imports
from py_linq_sql import SQLEnumerable


def test_execution_order_select_first(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(lambda x: x.data.obj.name == "earth")
        .order_by(lambda x: x.data.obj.mass)
        .first()
        .execute()
    )

    assert record


def test_execution_order_select_second(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .where(lambda x: x.data.obj.name == "earth")
        .select(lambda x: x.data.obj.name)
        .order_by(lambda x: x.data.obj.mass)
        .first()
        .execute()
    )

    assert record


def test_execution_order_select_X(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .where(lambda x: x.data.obj.name == "earth")
        .order_by(lambda x: x.data.obj.mass)
        .select(lambda x: x.data.obj.name)
        .first()
        .execute()
    )

    assert record


def test_execution_order_select_last(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .where(lambda x: x.data.obj.name == "earth")
        .select(lambda x: x.data.obj.name)
        .order_by(lambda x: x.data.obj.mass)
        .first()
        .execute()
    )

    assert record


def test_execution_order_skip_skip(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select()
        .skip(1)
        .skip(1)
        .execute()
    )

    assert record


def test_execution_order_skip_element_at(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select()
        .skip(1)
        .element_at(1)
        .execute()
    )

    assert record


def test_execution_order_take_take(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select()
        .take(1)
        .take(1)
        .execute()
    )

    assert record


def test_execution_order_take_element_at(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select()
        .take(1)
        .element_at(0)
        .execute()
    )

    assert record
