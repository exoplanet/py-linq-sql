# First party imports
from py_linq_sql import SQLEnumerable


def test_execution_order_update_first(db_connection_with_data_for_modif, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .update(lambda x: x.data.obj.mass == 666)
        .where(lambda x: x.data.obj.mass >= 1)
        .execute()
    )

    assert record


def test_execution_order_update_X(db_connection_with_data_for_modif, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .where(lambda x: x.data.obj.mass >= 1)
        .update(lambda x: x.data.obj.mass == 666)
        .where(lambda x: x.data.obj.name == "earth")
        .execute()
    )

    assert record


def test_execution_order_update_last(db_connection_with_data_for_modif, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .where(lambda x: x.data.obj.mass >= 1)
        .update(lambda x: x.data.obj.mass == 666)
        .execute()
    )

    assert record
