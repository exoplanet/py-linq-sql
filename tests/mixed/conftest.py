"""All functions used in all tests."""

# Pytest imports
import pytest
import pytest_postgresql  # noqa: F401
from pytest_postgresql.executor import PostgreSQLExecutor
from pytest_postgresql.janitor import DatabaseJanitor

# Standard imports
from pathlib import Path

# Third party imports
import psycopg
from psycopg.conninfo import make_conninfo
from psycopg_pool import ConnectionPool

OBJ_VALUES = [
    {"json": {"obj": {"name": "earth", "mass": 1}}, "linked_id": 64},
    {"json": {"obj": {"name": "saturn", "mass": 52}}, "linked_id": 128},
    {"json": {"obj": {"name": "jupiter", "mass": 100}}, "linked_id": 256},
]


@pytest.fixture(scope="session")
def table_objects_mix() -> str:
    """Name of the table to store spatial objects."""
    return "mixed_objects"


def _load_mixed_schema(db: ConnectionPool) -> None:
    """
    Fill a database with a basic db schema.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    with open(Path.cwd() / Path("tests/mixed/schema.sql"), "rb") as sql_file:
        schema = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(schema)
                conn.commit()


@pytest.fixture(scope="session")
def db_mixed_with_only_schema(
    postgresql_proc: PostgreSQLExecutor, db_password: str
) -> ConnectionPool:
    """
    Return a temporary empty db with a db schema shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "py-linq_for_sql_test_db_mixed_with_schema"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            _load_mixed_schema(pool)
            yield pool


def _load_mixed_data(db: ConnectionPool) -> None:
    """
    Fill a database with some data for tests.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    with open(Path.cwd() / Path("tests/mixed/data.sql"), "rb") as sql_file:
        data = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(data)
                conn.commit()


@pytest.fixture(scope="session")
def db_mixed_with_data(
    postgresql_proc: PostgreSQLExecutor, db_password: str
) -> ConnectionPool:
    """
    Return a temporary db with schema and data shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "py-linq_for_sql_test_db_mixed_with_data"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            _load_mixed_schema(pool)
            _load_mixed_data(pool)
            yield pool


@pytest.fixture(scope="function")
def db_mixed_connection_with_only_schema_for_modif(
    db_mixed_with_only_schema: ConnectionPool,
) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary empty db with a db schema.

    Suitable for modification tests since all modification will be rolled back at the
    end of the test function.

    Args:
        - db_mixed_with_only_schema: a connection pool to the database

    Returns:
        A connection to the database
    """
    with db_mixed_with_only_schema.connection() as conn:
        yield conn
        conn.rollback()


@pytest.fixture
def db_mixed_connection_with_data(
    db_mixed_with_data: ConnectionPool,
) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary db with schema and data.

    Suitable for modification tests since the db will be destroyed after each test.

    Args:
        - db_mixed_with_only_schema: a connection pool to the database

    Returns:
        A connection to the database
    """
    with db_mixed_with_data.connection() as conn:
        yield conn
