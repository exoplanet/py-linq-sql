-- INSERT data in mixed_objects

INSERT INTO mixed_objects(data, linked_id) VALUES
('{"obj": {"name": "earth", "mass": 1}}', 64),
('{"obj": {"name": "saturn", "mass": 52}}', 128),
('{"obj": {"name": "jupiter", "mass": 100}}', 256);
