-- CREATE TABLE mixed_objects

CREATE TABLE mixed_objects
(id serial NOT NULL PRIMARY KEY, data jsonb NOT NULL,
    linked_id smallint NOT NULL);
CREATE INDEX mixed_datagin ON mixed_objects USING gin(data);
