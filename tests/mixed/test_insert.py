# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "obj_to_insert, expected_insert_result, expected_select_result",
    [
        param(
            ({"obj": {"name": "earth", "mass": 1}}, 64),
            1,
            [("earth", 64)],
            id="just earth",
        ),
        param(
            [({"obj": {"name": "saturn", "mass": 52}}, 128)],
            1,
            [("saturn", 128)],
            id="just saturn",
        ),
        param(
            [
                ({"obj": {"name": "jupiter", "mass": 100}}, 256),
                ({"obj": {"name": "uranus", "mass": 73}}, 512),
            ],
            2,
            [("jupiter", 256), ("uranus", 512)],
            id="jupiter and uranus",
        ),
    ],
)
def test_execute_insert(
    db_mixed_connection_with_only_schema_for_modif,
    table_objects_mix,
    obj_to_insert,
    expected_insert_result,
    expected_select_result,
):
    record = (
        SQLEnumerable(db_mixed_connection_with_only_schema_for_modif, table_objects_mix)
        .insert(["data", "linked_id"], obj_to_insert)
        .execute()
    )

    check = (
        SQLEnumerable(db_mixed_connection_with_only_schema_for_modif, table_objects_mix)
        .select(lambda x: (x.data.obj.name, x.linked_id))
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(expected_insert_result)
        assert_that(check).contains_only(*expected_select_result)


@pytest.mark.parametrize(
    "input_data",
    [
        param(
            ({"obj": {"name": "toto", "mass": [0, 1]}}, 64),
            id="with list in tuple",
        ),
        param(
            ({"obj": {"name": "toto", "mass": (0, 1)}}, 64),
            id="with tuple in tuple",
        ),
    ],
)
def test_execute_insert_rel_array(
    db_mixed_connection_with_only_schema_for_modif,
    table_objects_mix,
    input_data,
):
    record = (
        SQLEnumerable(
            db_mixed_connection_with_only_schema_for_modif,
            table_objects_mix,
        )
        .insert(["data", "linked_id"], input_data)
        .execute()
    )

    check = (
        SQLEnumerable(
            db_mixed_connection_with_only_schema_for_modif,
            table_objects_mix,
        )
        .select(lambda x: (x.data.obj.name, x.data.obj.mass, x.linked_id))
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check.to_list()[0][0]).is_equal_to("toto")
        assert_that(check.to_list()[0][1]).is_equal_to([0, 1])
        assert_that(check.to_list()[0][2]).is_equal_to(64)


@pytest.mark.parametrize(
    "input_data",
    [
        param(
            ({"obj": {"name": "toto", "mass": (0, (1, 2))}}, 64),
            id="insert json array tuple in tuple",
        ),
        param(
            ({"obj": {"name": "toto", "mass": [0, [1, 2]]}}, 64),
            id="insert json array list in list",
        ),
        param(
            ({"obj": {"name": "toto", "mass": (0, [1, 2])}}, 64),
            id="insert json array list in tuple",
        ),
        param(
            ({"obj": {"name": "toto", "mass": [0, (1, 2)]}}, 64),
            id="insert json array tuple in list",
        ),
    ],
)
def test_execute_insert_multidimensional_array_succeeds(
    db_mixed_connection_with_only_schema_for_modif,
    table_objects_mix,
    input_data,
):
    record = (
        SQLEnumerable(db_mixed_connection_with_only_schema_for_modif, table_objects_mix)
        .insert(["data", "linked_id"], input_data)
        .execute()
    )

    check = (
        SQLEnumerable(
            db_mixed_connection_with_only_schema_for_modif,
            table_objects_mix,
        )
        .select(lambda x: (x.data.obj.name, x.data.obj.mass, x.linked_id))
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check.to_list()[0][0]).is_equal_to("toto")
        assert_that(check.to_list()[0][1]).is_equal_to([0, [1, 2]])
        assert_that(check.to_list()[0][2]).is_equal_to(64)
