# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


def test_execute_select_mixed(db_mixed_connection_with_data, table_objects_mix):
    record = (
        SQLEnumerable(db_mixed_connection_with_data, table_objects_mix)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass, x.linked_id))
        .execute()
    )

    assert_that(record.to_list()).contains_only(
        *[
            ("earth", 1, 64),
            ("saturn", 52, 128),
            ("jupiter", 100, 256),
        ]
    )
