# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_mass",
    [
        12,
        1,
        0,
        100,
        -12,
        -1,
        -0,
        -100,
    ],
)
def test_operator_abs(
    db_connection_with_only_schema_for_modif, table_objects, input_mass
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(lambda x: (x.data.name, abs(x.data.mass)))
        .execute()
    ).to_list()[0][1]

    assert val == abs(input_mass)
