# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that
from dotmap import DotMap

# First party imports
from py_linq_sql import SQLEnumerable

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        (1, lambda x: x.data.mass + 0 - 0),
        (52, lambda x: x.data.mass + (1 - 1)),
        (100, lambda x: x.data.mass - 100 + 100),
        (12, lambda x: x.data.mass - (100 + 100)),
        (18, lambda x: x.data.mass - x.data.mass + 10),
    ],
)
def test_operator_add_sub(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(input_lambda)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_equal_to(input_lambda(x))
