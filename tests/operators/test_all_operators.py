# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable, cbrt, factorial, sqrt

# Local imports
from ..conftest import insert_value

# '(H)ALO' = Arithmetic and Logical Operators


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        # Test '(H)ALO' and '**'(pow)
        (
            lambda x: ((x.data.obj.mass**2 + 1 > 1000) & (x.data.obj.mass < 100)),
            [("saturn",)],
        ),
        (
            lambda x: ((x.data.obj.mass**2 - 1 > 1000) | (x.data.obj.mass < 100)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        (
            lambda x: (~(x.data.obj.mass**2 * 2 > 1000)),
            [("earth",)],
        ),
        # Test '(H)ALO' and '|/'(sqrt)
        (
            lambda x: ((sqrt(x.data.obj.mass) > 1 + 1) & (x.data.obj.mass < 100)),
            [("saturn",)],
        ),
        (
            lambda x: ((sqrt(x.data.obj.mass) - 1 > 1) | (x.data.obj.mass < 100)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        (
            lambda x: (~(sqrt(x.data.obj.mass) * 2 > 1)),
            [],
        ),
        # Test '(H)ALO' and '||/'(cbrt)
        (
            lambda x: ((cbrt(x.data.obj.mass) + 1 > 2) & (x.data.obj.mass < 100)),
            [("saturn",)],
        ),
        (
            lambda x: ((cbrt(x.data.obj.mass) / 2 > 1) | (x.data.obj.mass < 100)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        (
            lambda x: (~(cbrt(x.data.obj.mass) / 2 > 1)),
            [("earth",)],
        ),
        # Test '(H)ALO' and '!!'(factorial)
        (
            lambda x: ((factorial(x.data.obj.mass) * 2 > 2) & (x.data.obj.mass < 100)),
            [("saturn",)],
        ),
        (
            lambda x: ((factorial(x.data.obj.mass) / 2 > 1) | (x.data.obj.mass < 100)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        (
            lambda x: (~(factorial(x.data.obj.mass) - 1 > 1)),
            [("earth",)],
        ),
        # gloubi-boulga (divers)
        (
            lambda x: (
                (~(x.data.obj.mass + 4**2 >= 4) | (cbrt(x.data.obj.mass) > 3))
                & (sqrt(x.data.obj.mass) < 10)
            ),
            [("saturn",)],
        ),
    ],
)
def test_all_operator_without_abs(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(input_lambda)
        .execute()
    ).to_list()

    assert record == input_expected


# Test '(H)ALO' and '@'(abs)
@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (
            lambda x: ((abs(x.data.obj.mass) > 1) & (x.data.obj.mass < 100)),
            [("saturn",), ("toto",)],
        ),
        (
            lambda x: ((abs(x.data.obj.mass) > 1) | (x.data.obj.mass < 100)),
            [("earth",), ("saturn",), ("jupiter",), ("toto",)],
        ),
        (
            lambda x: (~(abs(x.data.obj.mass) > 1)),
            [("earth",)],
        ),
    ],
)
def test_all_operator_w_abs(
    db_connection_with_data_for_modif,
    table_objects,
    input_lambda,
    input_expected,
):
    insert_value(
        db_connection_with_data_for_modif, {"obj": {"name": "toto", "mass": -18}}
    )

    record = (
        SQLEnumerable(db_connection_with_data_for_modif, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(input_lambda)
        .execute()
    ).to_list()

    assert record == input_expected
