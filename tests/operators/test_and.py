# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


def test_operator_and_w_less_greater(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: ((x.data.obj.mass > 0) & (x.data.obj.mass < 25)))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("earth")
        assert_that(record[0].data_obj_mass).is_equal_to(1)


def test_operator_and_w_not_equal(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(
            lambda x: ((x.data.obj.name != "earth") & (x.data.obj.name != "jupiter"))
        )
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("saturn")
        assert_that(record[0].data_obj_mass).is_equal_to(52)


def test_operator_and_w_eq_ne(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: ((x.data.obj.mass == 52) & (x.data.obj.name != "earth")))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("saturn")
        assert_that(record[0].data_obj_mass).is_equal_to(52)


def test_operator_rand(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: (True & (x.data.obj.name != "jupiter")))
        .execute()
    ).to_list()

    assert_that(record).contains_only(*[("earth", 1), ("saturn", 52)])
