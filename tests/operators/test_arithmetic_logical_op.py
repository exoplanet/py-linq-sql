# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        # Test arithmetic operator with '&'(and)
        (
            lambda x: ((x.data.obj.mass + 4 >= 4) & (x.data.obj.mass != 0)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        (
            lambda x: ((x.data.obj.mass * 4 >= 4) & (x.data.obj.mass != 0)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        (
            lambda x: ((x.data.obj.mass / 4 >= 4) & (x.data.obj.mass != 0)),
            [("saturn",), ("jupiter",)],
        ),
        (
            lambda x: ((x.data.obj.mass - 4 >= 4) & (x.data.obj.mass != 0)),
            [("saturn",), ("jupiter",)],
        ),
        # Test arithmetic operator with '|'(or)
        (
            lambda x: ((x.data.obj.mass + 4 >= 4) | (x.data.obj.mass != 0)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        (
            lambda x: ((x.data.obj.mass * 4 >= 4) | (x.data.obj.mass != 0)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        (
            lambda x: ((x.data.obj.mass / 4 >= 4) | (x.data.obj.mass != 0)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        (
            lambda x: ((x.data.obj.mass - 4 >= 4) | (x.data.obj.mass != 0)),
            [("earth",), ("saturn",), ("jupiter",)],
        ),
        # Test arithmetic operator with '~'(not)
        (
            lambda x: (~(x.data.obj.mass + 4 >= 4)),
            [],
        ),
        (
            lambda x: (~(x.data.obj.mass * 4 >= 4)),
            [],
        ),
        (
            lambda x: (~(x.data.obj.mass / 4 >= 4)),
            [("earth",)],
        ),
        (
            lambda x: (~(x.data.obj.mass - 4 >= 4)),
            [("earth",)],
        ),
        # gloubi-boulga (divers)
        (
            lambda x: (
                (~(x.data.obj.mass + 4 >= 4) | (x.data.obj.mass > 25))
                & (x.data.obj.name != "jupiter")
            ),
            [("saturn",)],
        ),
    ],
)
def test_arithmetic_operator(
    db_connection_with_data, table_objects, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(input_lambda)
        .execute()
    ).to_list()

    assert record == input_expected
