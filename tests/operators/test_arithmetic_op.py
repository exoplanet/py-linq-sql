# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that
from dotmap import DotMap

# First party imports
from py_linq_sql import SQLEnumerable

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        # Test '+' and '*'
        (1, lambda x: x.data.mass + 4 * 2),
        (52, lambda x: x.data.mass * (7 + 3)),
        (100, lambda x: x.data.mass + (1 + 2) * 3),
        (578, lambda x: x.data.mass + 1 + 2 * 3),
        # Test '+' and '/'
        (1, lambda x: x.data.mass + 4 / 2),
        (52, lambda x: x.data.mass / (7 + 3)),
        (100, lambda x: x.data.mass + (1 + 2) / 3),
        (578, lambda x: x.data.mass + 1 + 2 / 3),
        # Test '+' and '%'
        (1, lambda x: x.data.mass + 4 % 2),
        (52, lambda x: x.data.mass % (7 + 3)),
        (100, lambda x: x.data.mass + (1 + 2) % 3),
        (578, lambda x: x.data.mass + 1 + 2 % 3),
        # Test '-' and '*'
        (1, lambda x: x.data.mass - 4 * 2),
        (52, lambda x: x.data.mass * (7 - 3)),
        (100, lambda x: x.data.mass - (1 - 2) * 3),
        (578, lambda x: x.data.mass - 1 - 2 * 3),
        # Test '-' and '/'
        (1, lambda x: x.data.mass - 4 / 2),
        (52, lambda x: x.data.mass / (7 - 3)),
        (100, lambda x: x.data.mass - (1 - 2) / 3),
        (578, lambda x: x.data.mass - 1 - 2 / 3),
        # Test '-' and '%'
        (1, lambda x: x.data.mass - 4 % 2),
        (52, lambda x: x.data.mass % (7 - 3)),
        (100, lambda x: x.data.mass - (1 - 2) % 3),
        (578, lambda x: x.data.mass - 1 - 2 % 3),
        # gloubi-boulga (divers)
        (938, lambda x: x.data.mass - 4 % 2 * 5 + 2 - 1 / 2),
        (52, lambda x: x.data.mass * (7 - 3) + (1 - 2) / 4 * 2.5),
        (
            1892,
            lambda x: x.data.mass * (x.data.mass - 3)
            + (x.data.mass - 1) / x.data.mass * 2.5,
        ),
    ],
)
def test_arithmetic_operator(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(input_lambda)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(x), 0.001)
