# Pytest imports
import pytest

# Standard imports
from functools import partial

# Third party imports
from assertpy import assert_that
from dotmap import DotMap
from numpy import cbrt as cbrt_py

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql import cbrt as cbrt_sql

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        (1, lambda cbrt, x: cbrt(x.data.mass)),
        (52, lambda cbrt, x: cbrt(x.data.mass)),
        (12, lambda cbrt, x: cbrt(x.data.mass)),
        (563, lambda cbrt, x: cbrt(x.data.mass)),
    ],
)
def test_operator_cbrt(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_cbrt = partial(input_lambda, cbrt_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_cbrt)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(cbrt_py, x), 0.001)
