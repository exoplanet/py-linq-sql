# Pytest imports
import pytest

# Standard imports
from functools import partial
from math import factorial as factorial_py

# Third party imports
from assertpy import assert_that
from dotmap import DotMap

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql import factorial as factorial_sql

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        (1, lambda factorial, x: factorial(x.data.mass)),
        (2, lambda factorial, x: factorial(x.data.mass)),
        (6, lambda factorial, x: factorial(x.data.mass)),
        (9, lambda factorial, x: factorial(x.data.mass)),
    ],
)
def test_operator_factorial(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_factorial = partial(input_lambda, factorial_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_factorial)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(factorial_py, x), 0.001)
