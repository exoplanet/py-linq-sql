# Pytest imports
import pytest
from pytest import param

# Standard imports
from functools import partial
from math import acosh as acosh_py
from math import asinh as asinh_py
from math import atanh as atanh_py
from math import cosh as cosh_py
from math import sinh as sinh_py
from math import tanh as tanh_py

# Third party imports
from assertpy import assert_that
from dotmap import DotMap

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql import acosh as acosh_sql
from py_linq_sql import asinh as asinh_sql
from py_linq_sql import atanh as atanh_sql
from py_linq_sql import cosh as cosh_sql
from py_linq_sql import sinh as sinh_sql
from py_linq_sql import tanh as tanh_sql

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(18.5, lambda acosh, x: acosh(x.data.mass), id="acosh_float_positive"),
        param(68, lambda acosh, x: acosh(x.data.mass), id="acosh_int_positive"),
        param(1, lambda acosh, x: acosh(x.data.mass), id="acosh_1"),
    ],
)
def test_trigo_function_acosh(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_acosh = partial(input_lambda, acosh_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_acosh)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(acosh_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(18.5, lambda asinh, x: asinh(x.data.mass), id="asinh_float_positive"),
        param(68, lambda asinh, x: asinh(x.data.mass), id="asinh_int_positive"),
        param(-168.4, lambda asinh, x: asinh(x.data.mass), id="asinh_float_negative"),
        param(-328, lambda asinh, x: asinh(x.data.mass), id="asinh_int_negative"),
        param(0, lambda asinh, x: asinh(x.data.mass), id="asinh_0"),
    ],
)
def test_trigo_function_asinh(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_asinh = partial(input_lambda, asinh_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_asinh)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(asinh_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.9, lambda atanh, x: atanh(x.data.mass), id="atanh_float_positive"),
        param(-0.9, lambda atanh, x: atanh(x.data.mass), id="atanh_float_negative"),
        param(0, lambda atanh, x: atanh(x.data.mass), id="atanh_0"),
    ],
)
def test_trigo_function_atanh(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_atanh = partial(input_lambda, atanh_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_atanh)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(atanh_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(18.5, lambda cosh, x: cosh(x.data.mass), id="cosh_float_positive"),
        param(68, lambda cosh, x: cosh(x.data.mass), id="cosh_int_positive"),
        param(-168.4, lambda cosh, x: cosh(x.data.mass), id="cosh_float_negative"),
        param(-328, lambda cosh, x: cosh(x.data.mass), id="cosh_int_negative"),
        param(0, lambda cosh, x: cosh(x.data.mass), id="cosh_0"),
    ],
)
def test_trigo_function_cosh(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_cosh = partial(input_lambda, cosh_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_cosh)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(cosh_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(18.5, lambda sinh, x: sinh(x.data.mass), id="sinh_float_positive"),
        param(68, lambda sinh, x: sinh(x.data.mass), id="sinh_int_positive"),
        param(-168.4, lambda sinh, x: sinh(x.data.mass), id="sinh_float_negative"),
        param(-328, lambda sinh, x: sinh(x.data.mass), id="sinh_int_negative"),
        param(0, lambda sinh, x: sinh(x.data.mass), id="sinh_0"),
    ],
)
def test_trigo_function_sinh(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_sinh = partial(input_lambda, sinh_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_sinh)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(sinh_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(18.5, lambda tanh, x: tanh(x.data.mass), id="tanh_float_positive"),
        param(68, lambda tanh, x: tanh(x.data.mass), id="tanh_int_positive"),
        param(-168.4, lambda tanh, x: tanh(x.data.mass), id="tanh_float_negative"),
        param(-328, lambda tanh, x: tanh(x.data.mass), id="tanh_int_negative"),
        param(0, lambda tanh, x: tanh(x.data.mass), id="tanh_0"),
    ],
)
def test_trigo_function_tanh(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_tanh = partial(input_lambda, tanh_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_tanh)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(tanh_py, x), 0)
