# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable, is_in


def test_operator_in_lst_no_str(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(lambda x: is_in(x.data.obj.mass, [100, 110]))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("jupiter")


def test_operator_in_lst_str(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(lambda x: is_in(x.data.obj.name, ["earth", "saturn"]))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(*record[0]).is_equal_to("earth")
        assert_that(*record[1]).is_equal_to("saturn")
