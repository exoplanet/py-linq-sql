# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


# Test '&'(and) and '~'(not)
@pytest.mark.parametrize(
    "input_lambda",
    [
        (lambda x: ((x.data.obj.mass > 25) & ~(x.data.obj.mass == 52))),
        (lambda x: (~(x.data.obj.mass == 52) & (x.data.obj.mass > 25))),
    ],
)
def test_logical_operator_and_not(db_connection_with_data, table_objects, input_lambda):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(input_lambda)
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("jupiter")
        assert_that(record[0].data_obj_mass).is_equal_to(100)


@pytest.mark.parametrize(
    "input_lambda",
    [
        (lambda x: ((x.data.obj.mass > 25) & ~(x.data.obj.mass > 25))),
        (lambda x: (~(x.data.obj.mass > 25) & (x.data.obj.mass > 25))),
    ],
)
def test_logical_operator_and_not_empty(
    db_connection_with_data, table_objects, input_lambda
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(input_lambda)
        .execute()
    ).to_list()

    assert not record


# Test '&'(and) and '|'(or)
@pytest.mark.parametrize(
    "input_lambda",
    [
        (
            lambda x: (
                ((x.data.obj.mass > 50) & (x.data.obj.name == "earth"))
                | (x.data.obj.mass >= 95)
            )
        ),
        (
            lambda x: (
                (x.data.obj.mass > 50)
                & ((x.data.obj.name == "earth") | (x.data.obj.mass >= 95))
            )
        ),
    ],
)
def test_logical_operator_and_or_1(
    db_connection_with_data, table_objects, input_lambda
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(input_lambda)
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("jupiter")
        assert_that(record[0].data_obj_mass).is_equal_to(100)


def test_logical_operator_and_or_2(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(
            lambda x: (
                (x.data.obj.name == "earth")
                & ((x.data.obj.mass < 50) | (x.data.obj.mass > 95))
            )
        )
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("earth")
        assert_that(record[0].data_obj_mass).is_equal_to(1)
        assert_that(record[1].data_obj_name).is_equal_to("jupiter")
        assert_that(record[1].data_obj_mass).is_equal_to(100)


# Test '|'(or) and '~'(not)
@pytest.mark.parametrize(
    "input_lambda",
    [
        (lambda x: ((x.data.obj.mass > 25) | ~(x.data.obj.mass == 52))),
        (lambda x: (~(x.data.obj.mass == 52) | (x.data.obj.mass > 25))),
        (lambda x: ((x.data.obj.mass > 25) | ~(x.data.obj.mass > 25))),
        (lambda x: (~(x.data.obj.mass > 25) | (x.data.obj.mass > 25))),
    ],
)
def test_logical_operator_or_not_all(
    db_connection_with_data, table_objects, input_lambda
):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(input_lambda)
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("earth")
        assert_that(record[0].data_obj_mass).is_equal_to(1)
        assert_that(record[1].data_obj_name).is_equal_to("saturn")
        assert_that(record[1].data_obj_mass).is_equal_to(52)
        assert_that(record[2].data_obj_name).is_equal_to("jupiter")
        assert_that(record[2].data_obj_mass).is_equal_to(100)


def test_logical_operator_or_not_not_all(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: (~(x.data.obj.name == "earth") | (x.data.obj.mass > 3)))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("saturn")
        assert_that(record[0].data_obj_mass).is_equal_to(52)
        assert_that(record[1].data_obj_name).is_equal_to("jupiter")
        assert_that(record[1].data_obj_mass).is_equal_to(100)


# Test '&'(and) and '|'(or) and '~'(not)
def test_logical_operator_gloubi_boulga(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(
            lambda x: (
                ((x.data.obj.mass > 25) | ~(x.data.obj.mass != 1))
                & ((x.data.obj.name == "saturn") | (x.data.obj.mass == 100))
            )
        )
        .execute()
    ).to_list()

    print(record)

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("saturn")
        assert_that(record[0].data_obj_mass).is_equal_to(52)
        assert_that(record[1].data_obj_name).is_equal_to("jupiter")
        assert_that(record[1].data_obj_mass).is_equal_to(100)
