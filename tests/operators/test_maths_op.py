# Pytest imports
import pytest

# Standard imports
from functools import partial
from math import factorial as raw_factorial_py
from math import sqrt as sqrt_py

# Third party imports
from assertpy import assert_that
from dotmap import DotMap
from numpy import cbrt as cbrt_py

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql import cbrt as cbrt_sql
from py_linq_sql import factorial as factorial_sql
from py_linq_sql import sqrt as sqrt_sql

# Local imports
from ..conftest import insert_value

factorial_py = lambda v: raw_factorial_py(int(v))  # noqa: E731


@pytest.mark.parametrize(
    "input_mass, input_lambda, input_func",
    [
        (
            1,
            lambda func_1, func_2, func_3, x: func_1(func_2(func_3(x.data.mass))),
            DotMap(
                func_1=DotMap(sql=sqrt_sql, py=sqrt_py),
                func_2=DotMap(sql=cbrt_sql, py=cbrt_py),
                func_3=DotMap(sql=factorial_sql, py=factorial_py),
            ),
        ),
        (
            27,
            lambda func_1, func_2, func_3, x: func_1(func_2(func_3(x.data.mass))),
            DotMap(
                func_1=DotMap(sql=sqrt_sql, py=sqrt_py),
                func_2=DotMap(sql=factorial_sql, py=factorial_py),
                func_3=DotMap(sql=cbrt_sql, py=cbrt_py),
            ),
        ),
    ],
)
def test_math_operators_3_math_op(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda,
    input_func,
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery = partial(
        input_lambda,
        input_func.func_1.sql,
        input_func.func_2.sql,
        input_func.func_3.sql,
    )

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(
        input_lambda(
            input_func.func_1.py, input_func.func_2.py, input_func.func_3.py, x
        ),
        0.001,
    )


@pytest.mark.parametrize(
    "input_mass, input_lambda, input_func",
    [
        # Test '|/'(pow) and '||/'(cbrt)
        (
            357,
            lambda func_1, func_2, x: func_1(func_2(x.data.mass)),
            DotMap(
                func_1=DotMap(sql=sqrt_sql, py=sqrt_py),
                func_2=DotMap(sql=cbrt_sql, py=cbrt_py),
            ),
        ),
        (
            656,
            lambda func_1, func_2, x: func_1(func_2(x.data.mass)),
            DotMap(
                func_1=DotMap(sql=cbrt_sql, py=cbrt_py),
                func_2=DotMap(sql=sqrt_sql, py=sqrt_py),
            ),
        ),
        # Test '|/'(sqrt) and '!!'(factorial)
        (
            5,
            lambda func_1, func_2, x: func_1(func_2(x.data.mass)),
            DotMap(
                func_1=DotMap(sql=sqrt_sql, py=sqrt_py),
                func_2=DotMap(sql=factorial_sql, py=factorial_py),
            ),
        ),
        (
            25,
            lambda func_1, func_2, x: func_1(func_2(x.data.mass)),
            DotMap(
                func_1=DotMap(sql=factorial_sql, py=factorial_py),
                func_2=DotMap(sql=sqrt_sql, py=sqrt_py),
            ),
        ),
        # Test '||/'(cbrt) and '!!'(factorial)
        (
            2,
            lambda func_1, func_2, x: func_1(func_2(x.data.mass)),
            DotMap(
                func_1=DotMap(sql=cbrt_sql, py=cbrt_py),
                func_2=DotMap(sql=factorial_sql, py=factorial_py),
            ),
        ),
        (
            27,
            lambda func_1, func_2, x: func_1(func_2(x.data.mass)),
            DotMap(
                func_1=DotMap(sql=factorial_sql, py=factorial_py),
                func_2=DotMap(sql=cbrt_sql, py=cbrt_py),
            ),
        ),
        # gloubi-boulga (divers)
        (
            52,
            lambda func_1, func_2, x: func_1(func_2(x.data.mass**3)),
            DotMap(
                func_1=DotMap(sql=cbrt_sql, py=cbrt_py),
                func_2=DotMap(sql=sqrt_sql, py=sqrt_py),
            ),
        ),
        (
            100,
            lambda func_1, func_2, x: func_1(x.data.mass ** func_2(x.data.mass)),
            DotMap(
                func_1=DotMap(sql=sqrt_sql, py=sqrt_py),
                func_2=DotMap(sql=cbrt_sql, py=cbrt_py),
            ),
        ),
        (
            -68,
            lambda func_1, func_2, x: func_1(func_2(abs(x.data.mass))),
            DotMap(
                func_1=DotMap(sql=sqrt_sql, py=sqrt_py),
                func_2=DotMap(sql=cbrt_sql, py=cbrt_py),
            ),
        ),
    ],
)
def test_math_operators_2_math_op(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda,
    input_func,
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery = partial(input_lambda, input_func.func_1.sql, input_func.func_2.sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(
        input_lambda(input_func.func_1.py, input_func.func_2.py, x), 0.001
    )


@pytest.mark.parametrize(
    "input_mass, input_lambda, input_func, input_delta",
    [
        # Test '**'(pow) and '!!'(factorial)
        (
            4,
            lambda func, x: func(x.data.mass) ** 2,
            DotMap(func=DotMap(sql=factorial_sql, py=factorial_py)),
            0,
        ),
        (
            8,
            lambda func, x: func(x.data.mass**2),
            DotMap(func=DotMap(sql=factorial_sql, py=factorial_py)),
            0,
        ),
        # Test 'abs'(@) and '!!'(factorial)
        (
            -16,
            lambda func, x: func(abs(x.data.mass)),
            DotMap(func=DotMap(sql=factorial_sql, py=factorial_py)),
            0,
        ),
        (
            6,
            lambda func, x: func(abs(x.data.mass)),
            DotMap(func=DotMap(sql=factorial_sql, py=factorial_py)),
            0,
        ),
        # Test '**'(pow) and '||/'(cbrt)
        (
            67,
            lambda func, x: func(x.data.mass) ** 3,
            DotMap(func=DotMap(sql=cbrt_sql, py=cbrt_py)),
            0.001,
        ),
        (
            216,
            lambda func, x: func(x.data.mass**3),
            DotMap(func=DotMap(sql=cbrt_sql, py=cbrt_py)),
            0.001,
        ),
        # Test 'abs'(@) and '||/'(cbrt)
        (
            67,
            lambda func, x: func(abs(x.data.mass)),
            DotMap(func=DotMap(sql=cbrt_sql, py=cbrt_py)),
            0.001,
        ),
        (
            -216,
            lambda func, x: func(abs(x.data.mass)),
            DotMap(func=DotMap(sql=cbrt_sql, py=cbrt_py)),
            0.001,
        ),
        # Test 'sqrt'(|/) and '**'(pow)
        (
            9,
            lambda func, x: func(x.data.mass) ** 2,
            DotMap(func=DotMap(sql=sqrt_sql, py=sqrt_py)),
            0.001,
        ),
        (
            52,
            lambda func, x: func(x.data.mass**2),
            DotMap(func=DotMap(sql=sqrt_sql, py=sqrt_py)),
            0.001,
        ),
        # Test 'sqrt'(|/) and '@'(abs)
        (
            -93,
            lambda func, x: func(abs(x.data.mass)),
            DotMap(func=DotMap(sql=sqrt_sql, py=sqrt_py)),
            0.001,
        ),
        (
            93,
            lambda func, x: func(abs(x.data.mass)),
            DotMap(func=DotMap(sql=sqrt_sql, py=sqrt_py)),
            0.001,
        ),
    ],
)
def test_math_operators_1_maths_op(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda,
    input_func,
    input_delta,
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery = partial(input_lambda, input_func.func.sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(input_func.func.py, x), input_delta)


# Test '**'(pow) and '@'(abs)
@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        (12, lambda x: abs(x.data.mass) ** 3),
        (18, lambda x: abs(x.data.mass) ** -8),
        (-12, lambda x: abs(x.data.mass) ** 3),
        (-18, lambda x: abs(x.data.mass) ** -8),
    ],
)
def test_math_operators_pow_abs(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(input_lambda)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(x), 0.001)
