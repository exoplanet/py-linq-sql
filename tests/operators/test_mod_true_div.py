# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that
from dotmap import DotMap

# First party imports
from py_linq_sql import SQLEnumerable

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        (12, lambda x: x.data.mass % (4 / 2)),
        (12, lambda x: x.data.mass % 4 / 2),
        (382, lambda x: x.data.mass % (18 / 3)),
        (382, lambda x: x.data.mass % 18 / 3),
        (9, lambda x: x.data.mass / (3 % 2)),
        (9, lambda x: x.data.mass / 3 % 2),
        (589, lambda x: x.data.mass / (6 % 4)),
        (589, lambda x: x.data.mass / 6 % 4),
        (52, lambda x: x.data.mass / x.data.mass % x.data.mass),
    ],
)
def test_operator_mod_true_div(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(input_lambda)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(x), 0.01)
