# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal
from functools import partial
from math import ceil as ceil_py
from math import degrees as degrees_py
from math import exp as exp_py
from math import floor as floor_py
from math import gcd as gcd_py
from math import lcm as lcm_py
from math import log as ln_py
from math import log10 as log10_py
from math import radians as radians_py
from math import trunc as trunc_py

# Third party imports
from assertpy import assert_that
from dotmap import DotMap
from numpy import sign as sign_py

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql import ceil as ceil_sql
from py_linq_sql import degrees as degrees_sql
from py_linq_sql import exp as exp_sql
from py_linq_sql import floor as floor_sql
from py_linq_sql import gcd as gcd_sql
from py_linq_sql import greatest as greatest_sql
from py_linq_sql import lcm as lcm_sql
from py_linq_sql import least as least_sql
from py_linq_sql import ln as ln_sql
from py_linq_sql import log as log_sql
from py_linq_sql import log10 as log10_sql
from py_linq_sql import min_scale as min_scale_sql
from py_linq_sql import radians as radians_sql
from py_linq_sql import round as round_sql
from py_linq_sql import scale as scale_sql
from py_linq_sql import sign as sign_sql
from py_linq_sql import trim_scale as trim_scale_sql
from py_linq_sql import trunc as trunc_sql

# Local imports
from ..conftest import insert_value


def scale_py(number: int | float):
    tmp = str(number).split(".")
    if len(tmp) == 2:
        return len(tmp[1])
    return 0


def trim_scale_py(number: int | float):
    tmp = str(number).split(".")
    if len(tmp) == 2:
        res = tmp[1].rstrip("0")
        return Decimal(f"{tmp[0]}.{res}")
    return number


def min_scale_py(number: int | float):
    tmp = str(number).split(".")

    if len(tmp) == 2:
        res = tmp[1].rstrip("0")
        return len(res)
    return 0


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(2.1, lambda log, x: log(x.data.mass, 2), id="log_float_positive_base_2"),
        param(3, lambda log, x: log(x.data.mass, 2), id="log_int_positive_base_2"),
        param(
            2.1, lambda log, x: log(x.data.mass, 10), id="log_float_positive_base_10"
        ),
        param(3, lambda log, x: log(x.data.mass, 10), id="log_int_positive_base_10"),
    ],
)
def test_math_function_log(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    # In psql:
    # - ln(number) is equal to log(number) in python
    # - log(number, base) is equal to log(number, base) in python
    log_py = ln_py

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_log = partial(input_lambda, log_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_log)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(log_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(2.1, lambda log10, x: log10(x.data.mass), id="log10_float_positive"),
        param(3, lambda log10, x: log10(x.data.mass), id="log10_int_positive"),
    ],
)
def test_math_function_log10(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_log10 = partial(input_lambda, log10_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_log10)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(log10_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(2.1, lambda ln, x: ln(x.data.mass), id="ln_float_positive"),
        param(3, lambda ln, x: ln(x.data.mass), id="ln_int_positive"),
    ],
)
def test_math_function_ln(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_ln = partial(input_lambda, ln_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_ln)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(ln_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(2.1, lambda exp, x: exp(x.data.mass), id="exp_float_positive"),
        param(-1.8, lambda exp, x: exp(x.data.mass), id="exp_float_negative"),
        param(3, lambda exp, x: exp(x.data.mass), id="exp_int_positive"),
        param(-4, lambda exp, x: exp(x.data.mass), id="exp_int_negative"),
    ],
)
def test_math_function_exp(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_exp = partial(input_lambda, exp_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_exp)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(exp_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda, input_expected",
    [
        param(
            10.71,
            lambda min_scale, x: min_scale(x.data.mass),
            2,
            id="min_scale_float_positive",
        ),
        param(
            -56.8,
            lambda min_scale, x: min_scale(x.data.mass),
            1,
            id="min_scale_float_negative",
        ),
        param(
            515,
            lambda min_scale, x: min_scale(x.data.mass),
            0,
            id="min_scale_int_positive",
        ),
        param(
            -150,
            lambda min_scale, x: min_scale(x.data.mass),
            0,
            id="min_scale_int_negative",
        ),
        param(
            10.7100,
            lambda min_scale, x: min_scale(x.data.mass),
            2,
            id="min_scale_float_positive_end_by_0",
        ),
        param(
            -10.7100,
            lambda min_scale, x: min_scale(x.data.mass),
            2,
            id="min_scale_float_negative_end_by_0",
        ),
    ],
)
def test_math_function_min_scale(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda,
    input_expected,
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_min_scale = partial(input_lambda, min_scale_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_min_scale)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_equal_to(input_lambda(min_scale_py, x))


@pytest.mark.parametrize(
    "input_mass, input_lambda, input_expected",
    [
        param(
            10.7100,
            lambda trim_scale, x: trim_scale(x.data.mass),
            Decimal("10.71"),
            id="trim_scale_float_positive",
        ),
        param(
            -56.80,
            lambda trim_scale, x: trim_scale(x.data.mass),
            Decimal("-56.8"),
            id="trim_scale_float_negative",
        ),
        param(
            515,
            lambda trim_scale, x: trim_scale(x.data.mass),
            Decimal("515"),
            id="trim_scale_int_positive",
        ),
        param(
            -15,
            lambda trim_scale, x: trim_scale(x.data.mass),
            Decimal("-15"),
            id="trim_scale_int_negative",
        ),
        param(
            150,
            lambda trim_scale, x: trim_scale(x.data.mass),
            Decimal("150"),
            id="trim_scale_int_positive_end_by_0",
        ),
        param(
            -150.0,
            lambda trim_scale, x: trim_scale(x.data.mass),
            Decimal("-150"),
            id="trim_scale_int_negative_end_by_0",
        ),
    ],
)
def test_math_function_trim_scale(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda,
    input_expected,
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_trim_scale = partial(input_lambda, trim_scale_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_trim_scale)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_equal_to(input_lambda(trim_scale_py, x))


@pytest.mark.parametrize(
    "input_mass, input_lambda, input_expected",
    [
        param(
            10.71,
            lambda scale, x: scale(x.data.mass),
            2,
            id="scale_float_positive",
        ),
        param(
            -56.8,
            lambda scale, x: scale(x.data.mass),
            1,
            id="scale_float_negative",
        ),
        param(
            515,
            lambda scale, x: scale(x.data.mass),
            0,
            id="scale_int_positive",
        ),
        param(
            -15,
            lambda scale, x: scale(x.data.mass),
            0,
            id="scale_int_negative",
        ),
    ],
)
def test_math_function_scale(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda,
    input_expected,
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_scale = partial(input_lambda, scale_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_scale)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_equal_to(input_lambda(scale_py, x))


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(10.71, lambda round, x: round(x.data.mass), id="round_float_positive"),
        param(-56.8, lambda round, x: round(x.data.mass), id="round_float_negative"),
        param(515, lambda round, x: round(x.data.mass), id="round_int_positive"),
        param(-15, lambda round, x: round(x.data.mass), id="round_int_negative"),
    ],
)
def test_math_function_round(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_round = partial(input_lambda, round_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_round)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(round, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(
            10.71, lambda degrees, x: degrees(x.data.mass), id="degrees_float_positive"
        ),
        param(
            -56.8, lambda degrees, x: degrees(x.data.mass), id="degrees_float_negative"
        ),
        param(515, lambda degrees, x: degrees(x.data.mass), id="degrees_int_positive"),
        param(-15, lambda degrees, x: degrees(x.data.mass), id="degrees_int_negative"),
    ],
)
def test_math_function_degrees(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_degrees = partial(input_lambda, degrees_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_degrees)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(degrees_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(
            10.71, lambda radians, x: radians(x.data.mass), id="radians_float_positive"
        ),
        param(
            -56.8, lambda radians, x: radians(x.data.mass), id="radians_float_negative"
        ),
        param(515, lambda radians, x: radians(x.data.mass), id="radians_int_positive"),
        param(-15, lambda radians, x: radians(x.data.mass), id="radians_int_negative"),
    ],
)
def test_math_function_radians(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_radians = partial(input_lambda, radians_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_radians)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(radians_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(10.71, lambda floor, x: floor(x.data.mass), id="floor_float_positive"),
        param(-56.8, lambda floor, x: floor(x.data.mass), id="floor_float_negative"),
        param(515, lambda floor, x: floor(x.data.mass), id="floor_int_positive"),
        param(-15, lambda floor, x: floor(x.data.mass), id="floor_int_negative"),
    ],
)
def test_math_function_floor(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_floor = partial(input_lambda, floor_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_floor)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(floor_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(10.71, lambda ceil, x: ceil(x.data.mass), id="ceil_float_positive"),
        param(-56.8, lambda ceil, x: ceil(x.data.mass), id="ceil_float_negative"),
        param(515, lambda ceil, x: ceil(x.data.mass), id="ceil_int_positive"),
        param(-15, lambda ceil, x: ceil(x.data.mass), id="ceil_int_negative"),
    ],
)
def test_math_function_ceil(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_ceil = partial(input_lambda, ceil_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_ceil)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(ceil_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(1071, lambda lcm, x: lcm(x.data.mass, 462), id="lcm_positive"),
        param(-56, lambda lcm, x: lcm(x.data.mass, 1658), id="lcm_negative"),
        param(15, lambda lcm, x: lcm(x.data.mass, x.data.mass), id="lcm_mass_mass"),
    ],
)
def test_math_function_lcm(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_lcm = partial(input_lambda, lcm_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_lcm)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(lcm_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(15, lambda gcd, x: gcd(x.data.mass, 68), id="gcd_positive"),
        param(-56, lambda gcd, x: gcd(x.data.mass, 1658), id="gcd_negative"),
        param(15, lambda gcd, x: gcd(x.data.mass, x.data.mass), id="gcd_mass_mass"),
    ],
)
def test_math_function_gcd(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_gcd = partial(input_lambda, gcd_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_gcd)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(gcd_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(15.168, lambda trunc, x: trunc(x.data.mass), id="trunc_0_positive"),
        param(-56.138, lambda trunc, x: trunc(x.data.mass), id="trunc_0_negative"),
    ],
)
def test_math_function_trunc_0(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_trunc = partial(input_lambda, trunc_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_trunc)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(trunc_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda, input_expected",
    [
        param(
            1.2345e-6,
            lambda trunc, x: trunc(x.data.mass, 2),
            Decimal("0.00"),
            id="trunc_2_positive",
        ),
        param(
            (15.168),
            lambda trunc, x: trunc(x.data.mass, 2),
            Decimal("15.16"),
            id="trunc_2_positive",
        ),
        param(
            -56.138,
            lambda trunc, x: trunc(x.data.mass, 2),
            Decimal("-56.13"),
            id="trunc_2_negative",
        ),
        param(
            15.168,
            lambda trunc, x: trunc(x.data.mass, 9),
            Decimal("15.168"),
            id="trunc_9_positive",
        ),
        param(
            -56.138,
            lambda trunc, x: trunc(x.data.mass, 9),
            Decimal("-56.138"),
            id="trunc_9_negative",
        ),
    ],
)
def test_math_function_trunc_X(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda,
    input_expected,
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_trunc = partial(input_lambda, trunc_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_trunc)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_equal_to(input_expected)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(15, lambda sign, x: sign(x.data.mass), id="positive"),
        param(-56, lambda sign, x: sign(x.data.mass), id="negative"),
    ],
)
def test_math_function_sign(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_sign = partial(input_lambda, sign_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_sign)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(sign_py, x), 0.0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(
            23,
            lambda greatest, x: greatest(x.data.mass, 51),
            id="greatest_right_positive",
        ),
        param(
            51,
            lambda greatest, x: greatest(x.data.mass, 23),
            id="greatest_left_positive",
        ),
        param(
            -123,
            lambda greatest, x: greatest(x.data.mass, -18),
            id="greatest_right_negative",
        ),
        param(
            -18,
            lambda greatest, x: greatest(x.data.mass, -123),
            id="greatest_left_negative",
        ),
        param(
            -16,
            lambda greatest, x: greatest(x.data.mass, 18),
            id="greatest_right_negpos",
        ),
        param(
            16,
            lambda greatest, x: greatest(x.data.mass, -18),
            id="greatest_left_negpos",
        ),
        param(
            0, lambda greatest, x: greatest(x.data.mass, 43), id="greatest_right_zero"
        ),
        param(
            43, lambda greatest, x: greatest(x.data.mass, 0), id="greatest_left_zero"
        ),
        param(
            0,
            lambda greatest, x: greatest(x.data.mass, -43),
            id="greatest_left_zero_right_neg",
        ),
        param(
            -43,
            lambda greatest, x: greatest(x.data.mass, 0),
            id="greatest_right_zero_left_neg",
        ),
        param(
            17, lambda greatest, x: greatest(x.data.mass, 17), id="greatest_right_equal"
        ),
        param(
            15,
            lambda greatest, x: greatest(x.data.mass, x.data.mass),
            id="greatest_mass_mass",
        ),
    ],
)
def test_math_function_greatest(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda,
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_greatest = partial(input_lambda, greatest_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_greatest)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_equal_to(input_lambda(max, x))


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(
            23,
            lambda least, x: least(x.data.mass, 51),
            id="least_left_positive",
        ),
        param(
            51,
            lambda least, x: least(x.data.mass, 23),
            id="least_right_positive",
        ),
        param(
            -123,
            lambda least, x: least(x.data.mass, -18),
            id="least_left_negative",
        ),
        param(
            -18,
            lambda least, x: least(x.data.mass, -123),
            id="least_right_negative",
        ),
        param(
            -16,
            lambda least, x: least(x.data.mass, 18),
            id="least_left_negpos",
        ),
        param(
            16,
            lambda least, x: least(x.data.mass, -18),
            id="least_right_negpos",
        ),
        param(0, lambda least, x: least(x.data.mass, 43), id="least_left_zero"),
        param(43, lambda least, x: least(x.data.mass, 0), id="least_right_zero"),
        param(17, lambda least, x: least(x.data.mass, 17), id="least_left_equal"),
        param(
            0, lambda least, x: least(x.data.mass, -43), id="least_left_zero_right_neg"
        ),
        param(
            -43, lambda least, x: least(x.data.mass, 0), id="least_right_zero_left_neg"
        ),
        param(
            15,
            lambda least, x: least(x.data.mass, x.data.mass),
            id="least_mass_mass",
        ),
    ],
)
def test_math_function_least(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda,
):
    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_least = partial(input_lambda, least_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_least)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_equal_to(input_lambda(min, x))
