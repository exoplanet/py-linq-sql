# Pytest imports
import pytest
from pytest import param

# Third party imports
from dotmap import DotMap

# First party imports
from py_linq_sql.utils.classes.op_and_func_of_mdp import (
    HyperBFuncType,
    MathFunctType,
    OperatorType,
    TrigoFunctType,
    col_name_hyper,
    col_name_math,
    col_name_ope,
    col_name_trigo,
)


@pytest.mark.parametrize(
    "input_operator, input_expected",
    [
        param(HyperBFuncType.ACOSH, "acosh_mass", id="acosh_col_name"),
        param(HyperBFuncType.ASINH, "asinh_mass", id="asinh_col_name"),
        param(HyperBFuncType.ATANH, "atanh_mass", id="atanh_col_name"),
        param(HyperBFuncType.COSH, "cosh_mass", id="cosh_col_name"),
        param(HyperBFuncType.SINH, "sinh_mass", id="sinh_col_name"),
        param(HyperBFuncType.TANH, "tanh_mass", id="tanh_col_name"),
    ],
)
def test_col_name_hyperB(input_operator, input_expected):
    name_op = DotMap(op1="mass")
    record = col_name_hyper(name_op, input_operator)

    assert record == input_expected


@pytest.mark.parametrize(
    "input_operator, input_names, input_expected",
    [
        param(MathFunctType.CBRT, DotMap(op1="mass"), "cbrt_mass", id="cbrt_col_name"),
        param(MathFunctType.CEIL, DotMap(op1="mass"), "ceil_mass", id="ceil_col_name"),
        param(
            MathFunctType.DEGREES,
            DotMap(op1="mass"),
            "degrees_mass",
            id="degrees_col_name",
        ),
        param(MathFunctType.EXP, DotMap(op1="mass"), "exp_mass", id="exp_col_name"),
        param(
            MathFunctType.FACTORIAL,
            DotMap(op1="mass"),
            "factorial_mass",
            id="factorial_col_name",
        ),
        param(
            MathFunctType.FLOOR, DotMap(op1="mass"), "floor_mass", id="floor_col_name"
        ),
        param(
            MathFunctType.GCD,
            DotMap(op1="mass", op2="number"),
            "gcd_mass_number",
            id="gcd_col_name",
        ),
        param(
            MathFunctType.GREATEST,
            DotMap(op1="mass", op2="number"),
            "greatest_mass_number",
            id="greatest_col_name",
        ),
        param(
            MathFunctType.LEAST,
            DotMap(op1="mass", op2="number"),
            "least_mass_number",
            id="least_col_name",
        ),
        param(
            MathFunctType.LCM,
            DotMap(op1="mass", op2="number"),
            "lcm_mass_number",
            id="lcm_col_name",
        ),
        param(MathFunctType.LN, DotMap(op1="mass"), "ln_mass", id="ln_col_name"),
        param(
            MathFunctType.LOG,
            DotMap(op1="mass", op2="base"),
            "log_mass_base",
            id="log_col_name",
        ),
        param(MathFunctType.LOG10, DotMap(op1="mass"), "log10_mass", id="log_col_name"),
        param(
            MathFunctType.MIN_SCALE,
            DotMap(op1="mass"),
            "minScale_mass",
            id="minScale_col_name",
        ),
        param(
            MathFunctType.RADIANS,
            DotMap(op1="mass"),
            "radians_mass",
            id="radians_col_name",
        ),
        param(
            MathFunctType.ROUND,
            DotMap(op1="mass", op2="digits"),
            "round_mass_digits",
            id="round_col_name",
        ),
        param(
            MathFunctType.SCALE, DotMap(op1="mass"), "scale_mass", id="scale_col_name"
        ),
        param(MathFunctType.SIGN, DotMap(op1="mass"), "sign_mass", id="sign_col_name"),
        param(MathFunctType.SQRT, DotMap(op1="mass"), "sqrt_mass", id="sqrt_col_name"),
        param(
            MathFunctType.TRIM_SCALE,
            DotMap(op1="mass"),
            "trimScale_mass",
            id="trimScale_col_name",
        ),
        param(
            MathFunctType.TRUNC,
            DotMap(op1="mass", op2="digits"),
            "trunc_mass_digits",
            id="trunc_col_name",
        ),
    ],
)
def test_col_name_math(input_operator, input_names, input_expected):
    record = col_name_math(input_names, input_operator)

    assert record == input_expected


@pytest.mark.parametrize(
    "input_operator, input_names, input_expected",
    [
        param(OperatorType.ABS, DotMap(op1="mass"), "abs_mass", id="abs_col_name"),
        param(
            OperatorType.ADD,
            DotMap(op1="mass", op2="other"),
            "mass_add_other",
            id="add_col_name",
        ),
        param(
            OperatorType.AND,
            DotMap(op1="mass", op2="other"),
            "mass_and_other",
            id="and_col_name",
        ),
        param(
            OperatorType.EQ,
            DotMap(op1="mass", op2="other"),
            "mass_equal_other",
            id="eq_col_name",
        ),
        param(
            OperatorType.GE,
            DotMap(op1="mass", op2="other"),
            "mass_greater_equal_other",
            id="ge_col_name",
        ),
        param(
            OperatorType.GT,
            DotMap(op1="mass", op2="other"),
            "mass_greater_other",
            id="gt_col_name",
        ),
        param(
            OperatorType.INVERT,
            DotMap(op1="mass"),
            "not_mass",
            id="invert_col_name",
        ),
        param(
            OperatorType.LE,
            DotMap(op1="mass", op2="other"),
            "mass_lesser_equal_other",
            id="le_col_name",
        ),
        param(
            OperatorType.LT,
            DotMap(op1="mass", op2="other"),
            "mass_lesser_other",
            id="lt_col_name",
        ),
        param(
            OperatorType.MOD,
            DotMap(op1="mass", op2="other"),
            "mass_mod_other",
            id="mod_col_name",
        ),
        param(
            OperatorType.MUL,
            DotMap(op1="mass", op2="other"),
            "mass_mul_other",
            id="mul_col_name",
        ),
        param(
            OperatorType.NE,
            DotMap(op1="mass", op2="other"),
            "mass_not_equal_other",
            id="ne_col_name",
        ),
        param(OperatorType.NEG, DotMap(op1="mass"), "neg_mass", id="neg_col_name"),
        param(
            OperatorType.OR,
            DotMap(op1="mass", op2="other"),
            "mass_or_other",
            id="or_col_name",
        ),
        param(OperatorType.POS, DotMap(op1="mass"), "pos_mass", id="pos_col_name"),
        param(
            OperatorType.POW,
            DotMap(op1="mass", op2="other"),
            "mass_pow_other",
            id="pow_col_name",
        ),
        param(
            OperatorType.SUB,
            DotMap(op1="mass", op2="other"),
            "mass_sub_other",
            id="sub_col_name",
        ),
        param(
            OperatorType.TRUEDIV,
            DotMap(op1="mass", op2="other"),
            "mass_div_other",
            id="div_col_name",
        ),
    ],
)
def test_col_name_operator(input_operator, input_names, input_expected):
    record = col_name_ope(input_names, input_operator)

    assert record == input_expected


@pytest.mark.parametrize(
    "input_operator, input_names, input_expected",
    [
        param(TrigoFunctType.ACOS, DotMap(op1="mass"), "acos_mass", id="acos_col_name"),
        param(
            TrigoFunctType.ACOSD, DotMap(op1="mass"), "acosd_mass", id="acos_col_name"
        ),
        param(TrigoFunctType.ASIN, DotMap(op1="mass"), "asin_mass", id="ASIN_col_name"),
        param(
            TrigoFunctType.ASIND, DotMap(op1="mass"), "asind_mass", id="asind_col_name"
        ),
        param(TrigoFunctType.ATAN, DotMap(op1="mass"), "atan_mass", id="atan_col_name"),
        param(
            TrigoFunctType.ATAND, DotMap(op1="mass"), "atand_mass", id="atand_col_name"
        ),
        param(
            TrigoFunctType.ATAN2,
            DotMap(op1="mass", op2="base"),
            "atan2_mass_base",
            id="atan2_col_name",
        ),
        param(
            TrigoFunctType.ATAN2D,
            DotMap(op1="mass", op2="base"),
            "atan2d_mass_base",
            id="atan2d_col_name",
        ),
        param(TrigoFunctType.COS, DotMap(op1="mass"), "cos_mass", id="cos_col_name"),
        param(TrigoFunctType.COSD, DotMap(op1="mass"), "cosd_mass", id="cosd_col_name"),
        param(TrigoFunctType.COT, DotMap(op1="mass"), "cot_mass", id="cot_col_name"),
        param(TrigoFunctType.COTD, DotMap(op1="mass"), "cotd_mass", id="cotd_col_name"),
        param(TrigoFunctType.SIN, DotMap(op1="mass"), "sin_mass", id="sin_col_name"),
        param(TrigoFunctType.SIND, DotMap(op1="mass"), "sind_mass", id="sind_col_name"),
        param(TrigoFunctType.TAN, DotMap(op1="mass"), "tan_mass", id="tan_col_name"),
        param(TrigoFunctType.TAND, DotMap(op1="mass"), "tand_mass", id="tand_col_name"),
    ],
)
def test_col_name_trigo(input_operator, input_names, input_expected):
    record = col_name_trigo(input_names, input_operator)

    assert record == input_expected
