# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


def test_operator_not_w_less(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: ~(x.data.obj.mass < 80))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("jupiter")
        assert_that(record[0].data_obj_mass).is_equal_to(100)


def test_operator_not_w_greater(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: ~(x.data.obj.mass > 25))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("earth")
        assert_that(record[0].data_obj_mass).is_equal_to(1)


def test_operator_not_w_not_equal(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: (~(x.data.obj.name != "earth")))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("earth")
        assert_that(record[0].data_obj_mass).is_equal_to(1)


def test_operator_not_w_equal(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: (~(x.data.obj.name == "earth")))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("saturn")
        assert_that(record[0].data_obj_mass).is_equal_to(52)
        assert_that(record[1].data_obj_name).is_equal_to("jupiter")
        assert_that(record[1].data_obj_mass).is_equal_to(100)
