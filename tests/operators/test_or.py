# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda",
    [
        # test operator 'or'(|) with less(<) and greater(>)
        (lambda x: ((x.data.obj.mass > 0) | (x.data.obj.mass < 25))),
        # test operator 'or'(|) with none equal (!=)
        (lambda x: ((x.data.obj.name != "earth") | (x.data.obj.name != "jupiter"))),
    ],
)
def test_operator_or_simple(db_connection_with_data, table_objects, input_lambda):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(input_lambda)
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("earth")
        assert_that(record[0].data_obj_mass).is_equal_to(1)
        assert_that(record[1].data_obj_name).is_equal_to("saturn")
        assert_that(record[1].data_obj_mass).is_equal_to(52)
        assert_that(record[2].data_obj_name).is_equal_to("jupiter")
        assert_that(record[2].data_obj_mass).is_equal_to(100)


def test_operator_or_w_eq_ne(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: ((x.data.obj.mass == 52) | (x.data.obj.name != "earth")))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("saturn")
        assert_that(record[0].data_obj_mass).is_equal_to(52)
        assert_that(record[1].data_obj_name).is_equal_to("jupiter")
        assert_that(record[1].data_obj_mass).is_equal_to(100)


def test_operator_ror(db_connection_with_data, table_objects):
    record = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: (x.data.obj.name, x.data.obj.mass))
        .where(lambda x: (True | (x.data.obj.mass < 25)))
        .execute()
    ).to_list()

    with soft_assertions():
        assert_that(record[0].data_obj_name).is_equal_to("earth")
        assert_that(record[0].data_obj_mass).is_equal_to(1)
        assert_that(record[1].data_obj_name).is_equal_to("saturn")
        assert_that(record[1].data_obj_mass).is_equal_to(52)
        assert_that(record[2].data_obj_name).is_equal_to("jupiter")
        assert_that(record[2].data_obj_mass).is_equal_to(100)
