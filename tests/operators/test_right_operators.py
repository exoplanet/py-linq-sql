# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions
from dotmap import DotMap

# First party imports
from py_linq_sql import SQLEnumerable

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_mass, input_lambda_r, input_lambda_l",
    [
        param(
            14,
            lambda x: 8 + x.data.mass,
            lambda x: x.data.mass + 8,
            id="test_right_op_add",
        ),
        param(
            14,
            lambda x: 8 * x.data.mass,
            lambda x: x.data.mass * 8,
            id="test_right_op_mul",
        ),
    ],
)
def test_right_operator_switchable(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda_r,
    input_lambda_l,
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    val_right = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(input_lambda_r)
        .execute()
    ).to_list()[0][0]

    val_left = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(input_lambda_l)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    with soft_assertions():
        assert_that(val_right).is_equal_to(val_left)
        assert_that(val_right).is_equal_to(input_lambda_r(x))


@pytest.mark.parametrize(
    "input_mass, input_lambda_r, input_lambda_l",
    [
        param(
            14,
            lambda x: 8 - x.data.mass,
            lambda x: x.data.mass - 8,
            id="test_right_op_sub",
        ),
        param(
            14,
            lambda x: 8 / x.data.mass,
            lambda x: x.data.mass / 8,
            id="test_right_op_div",
        ),
        param(
            14,
            lambda x: 8 % x.data.mass,
            lambda x: x.data.mass % 8,
            id="test_right_op_mod",
        ),
        param(
            14,
            lambda x: 8**x.data.mass,
            lambda x: x.data.mass**8,
            id="test_right_op_pow",
        ),
    ],
)
def test_right_operator_no_switchable(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_mass,
    input_lambda_r,
    input_lambda_l,
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    val_right = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(input_lambda_r)
        .execute()
    ).to_list()[0][0]

    val_left = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(input_lambda_l)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    with soft_assertions():
        assert_that(val_right).is_not_equal_to(val_left)
        assert_that(val_right).is_close_to(input_lambda_r(x), 0.001)


@pytest.mark.parametrize(
    "input_lambda_r, input_lambda_l",
    [
        param(
            lambda x: (x.data.obj.name != "jupiter") & (x.data.obj.mass > 0),
            lambda x: (x.data.obj.mass > 0) & (x.data.obj.name != "jupiter"),
            id="test_right_op_and",
        ),
        param(
            lambda x: (x.data.obj.mass == 1) | (x.data.obj.mass > 2),
            lambda x: (x.data.obj.mass > 2) | (x.data.obj.mass == 1),
            id="test_right_op_or",
        ),
    ],
)
def test_right_operator_and_or(
    db_connection_with_data,
    table_objects,
    input_lambda_r,
    input_lambda_l,
):

    val_right = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(input_lambda_r)
        .execute()
    ).to_list()[0][0]

    val_left = (
        SQLEnumerable(db_connection_with_data, table_objects)
        .select(lambda x: x.data.obj.name)
        .where(input_lambda_l)
        .execute()
    ).to_list()[0][0]

    assert val_left == val_right
