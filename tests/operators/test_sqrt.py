# Pytest imports
import pytest

# Standard imports
from functools import partial
from math import sqrt as sqrt_py

# Third party imports
from assertpy import assert_that
from dotmap import DotMap

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql import sqrt as sqrt_sql

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        (1, lambda sqrt, x: sqrt(x.data.mass)),
        (52, lambda sqrt, x: sqrt(x.data.mass)),
        (12, lambda sqrt, x: sqrt(x.data.mass)),
        (563, lambda sqrt, x: sqrt(x.data.mass)),
    ],
)
def test_operator_sqrt(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_sqrt = partial(input_lambda, sqrt_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_sqrt)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(sqrt_py, x), 0.001)
