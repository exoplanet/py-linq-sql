# Pytest imports
import pytest
from pytest import param

# Standard imports
from functools import partial

# Third party imports
from assertpy import assert_that
from dotmap import DotMap

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql import lower as lower_sql
from py_linq_sql import title as title_sql
from py_linq_sql import upper as upper_sql

# Local imports
from ..conftest import insert_value


@pytest.mark.parametrize(
    "input_name",
    [
        param("test_toto", id="all lower"),
        param("teST_TOTO", id="mix"),
        param("TEST_TOTO", id="all upper"),
    ],
)
def test_str_lower(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_name,
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": input_name, "mass": 1}
    )

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(lambda x: lower_sql(x.data.name))
        .execute()
    ).to_list()[0][0]

    assert_that(val).is_equal_to(f'"{input_name.lower()}"')


@pytest.mark.parametrize(
    "input_name",
    [
        param("test_toto", id="all lower"),
        param("teST_TOTO", id="mix"),
        param("TEST_TOTO", id="all upper"),
    ],
)
def test_str_upper(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_name,
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": input_name, "mass": 1}
    )

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(lambda x: upper_sql(x.data.name))
        .execute()
    ).to_list()[0][0]

    assert_that(val).is_equal_to(f'"{input_name.upper()}"')


@pytest.mark.parametrize(
    "input_name",
    [
        param("test_toto", id="all lower"),
        param("teST_TOTO", id="mix"),
        param("TEST_TOTO", id="all upper"),
    ],
)
def test_str_title(
    db_connection_with_only_schema_for_modif,
    table_objects,
    input_name,
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": input_name, "mass": 1}
    )

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(lambda x: title_sql(x.data.name))
        .execute()
    ).to_list()[0][0]

    assert_that(val).is_equal_to(f'"{input_name.title()}"')
