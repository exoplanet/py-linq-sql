# Pytest imports
import pytest
from pytest import param

# Standard imports
from functools import partial
from math import acos as acos_py
from math import asin as asin_py
from math import atan as atan_py
from math import atan2 as atan2_py
from math import cos as cos_py
from math import degrees, radians
from math import sin as sin_py
from math import tan as tan_py

# Third party imports
from assertpy import assert_that
from dotmap import DotMap
from mpmath import cot as cot_py

# First party imports
from py_linq_sql import SQLEnumerable
from py_linq_sql import acos as acos_sql
from py_linq_sql import acosd as acosd_sql
from py_linq_sql import asin as asin_sql
from py_linq_sql import asind as asind_sql
from py_linq_sql import atan as atan_sql
from py_linq_sql import atan2 as atan2_sql
from py_linq_sql import atan2d as atan2d_sql
from py_linq_sql import atand as atand_sql
from py_linq_sql import cos as cos_sql
from py_linq_sql import cosd as cosd_sql
from py_linq_sql import cot as cot_sql
from py_linq_sql import cotd as cotd_sql
from py_linq_sql import sin as sin_sql
from py_linq_sql import sind as sind_sql
from py_linq_sql import tan as tan_sql
from py_linq_sql import tand as tand_sql

# Local imports
from ..conftest import insert_value

# For functions in degrees we used degrees(asin(number)) because:
# https://mail.python.org/pipermail/python-ideas/2018-June/051246.html
# https://mail.python.org/pipermail/python-ideas/2018-June/051247.html

# We convert the result in degrees


def acosd_py(number: int | float):
    return degrees(acos_py(number))


def asind_py(number: int | float):
    return degrees(asin_py(number))


def atand_py(number: int | float):
    return degrees(atan_py(number))


def atan2d_py(number: int | float, other: int):
    return degrees(atan2_py(number, other))


# We convert the number in radians
def cosd_py(number: int | float):
    return cos_py(radians(number))


def cotd_py(number: int | float):
    return cot_py(radians(number))


def sind_py(number: int | float):
    return sin_py(radians(number))


def tand_py(number: int | float):
    return tan_py(radians(number))


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda acos, x: acos(x.data.mass), id="acos_float_positive"),
        param(1, lambda acos, x: acos(x.data.mass), id="acos_int_positive"),
        param(-0.3, lambda acos, x: acos(x.data.mass), id="acos_float_negative"),
        param(-1, lambda acos, x: acos(x.data.mass), id="acos_int_negative"),
        param(0, lambda acos, x: acos(x.data.mass), id="acos_0"),
    ],
)
def test_trigo_function_acos(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_acos = partial(input_lambda, acos_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_acos)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(acos_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda acosd, x: acosd(x.data.mass), id="acosd_float_positive"),
        param(1, lambda acosd, x: acosd(x.data.mass), id="acosd_int_positive"),
        param(-0.3, lambda acosd, x: acosd(x.data.mass), id="acosd_float_negative"),
        param(-1, lambda acosd, x: acosd(x.data.mass), id="acosd_int_negative"),
        param(0, lambda acosd, x: acosd(x.data.mass), id="acosd_0"),
    ],
)
def test_trigo_function_acosd(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_acosd = partial(input_lambda, acosd_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_acosd)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(acosd_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda asin, x: asin(x.data.mass), id="asin_float_positive"),
        param(1, lambda asin, x: asin(x.data.mass), id="asin_int_positive"),
        param(-0.3, lambda asin, x: asin(x.data.mass), id="asin_float_negative"),
        param(-1, lambda asin, x: asin(x.data.mass), id="asin_int_negative"),
        param(0, lambda asin, x: asin(x.data.mass), id="asin_0"),
    ],
)
def test_trigo_function_asin(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_asin = partial(input_lambda, asin_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_asin)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(asin_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda asind, x: asind(x.data.mass), id="asind_float_positive"),
        param(1, lambda asind, x: asind(x.data.mass), id="asind_int_positive"),
        param(-0.3, lambda asind, x: asind(x.data.mass), id="asind_float_negative"),
        param(-1, lambda asind, x: asind(x.data.mass), id="asind_int_negative"),
        param(0, lambda asind, x: asind(x.data.mass), id="asind_0"),
    ],
)
def test_trigo_function_asind(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_asind = partial(input_lambda, asind_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_asind)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(asind_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda atan, x: atan(x.data.mass), id="atan_float_positive"),
        param(1, lambda atan, x: atan(x.data.mass), id="atan_int_positive"),
        param(-0.3, lambda atan, x: atan(x.data.mass), id="atan_float_negative"),
        param(-1, lambda atan, x: atan(x.data.mass), id="atan_int_negative"),
        param(0, lambda atan, x: atan(x.data.mass), id="atan_0"),
    ],
)
def test_trigo_function_atan(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_atan = partial(input_lambda, atan_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_atan)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(atan_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda atand, x: atand(x.data.mass), id="atand_float_positive"),
        param(1, lambda atand, x: atand(x.data.mass), id="atand_int_positive"),
        param(-0.3, lambda atand, x: atand(x.data.mass), id="atand_float_negative"),
        param(-1, lambda atand, x: atand(x.data.mass), id="atand_int_negative"),
        param(0, lambda atand, x: atand(x.data.mass), id="atand_0"),
    ],
)
def test_trigo_function_atand(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_atand = partial(input_lambda, atand_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_atand)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(atand_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(
            0.5, lambda atan2, x: atan2(x.data.mass, 0), id="atan2_float_positive_w_0"
        ),
        param(1, lambda atan2, x: atan2(x.data.mass, 0), id="atan2_int_positive_w_0"),
        param(
            -0.3, lambda atan2, x: atan2(x.data.mass, 0), id="atan2_float_negative_w_0"
        ),
        param(-1, lambda atan2, x: atan2(x.data.mass, 0), id="atan2_int_negative_w_0"),
        param(0, lambda atan2, x: atan2(x.data.mass, 0), id="atan2_0_w_0"),
        param(
            0.5,
            lambda atan2, x: atan2(
                x.data.mass,
                1,
            ),
            id="atan2_float_positive_w_1",
        ),
        param(1, lambda atan2, x: atan2(x.data.mass, 1), id="atan2_int_positive_w_1"),
        param(
            -0.3, lambda atan2, x: atan2(x.data.mass, 1), id="atan2_float_negative_w_1"
        ),
        param(-1, lambda atan2, x: atan2(x.data.mass, 1), id="atan2_int_negative_w_1"),
        param(0, lambda atan2, x: atan2(x.data.mass, 1), id="atan2_0_w_1"),
        param(
            0.5,
            lambda atan2, x: atan2(x.data.mass, 0.3),
            id="atan2_float_positive_w_0.3",
        ),
        param(
            1, lambda atan2, x: atan2(x.data.mass, 0.3), id="atan2_int_positive_w_0.3"
        ),
        param(
            -0.3,
            lambda atan2, x: atan2(x.data.mass, 0.3),
            id="atan2_float_negative_w_0.3",
        ),
        param(
            -1, lambda atan2, x: atan2(x.data.mass, 0.3), id="atan2_int_negative_w_0.3"
        ),
        param(0, lambda atan2, x: atan2(x.data.mass, 0.3), id="atan2_0_w_0.3"),
    ],
)
def test_trigo_function_atan2(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_atan2 = partial(input_lambda, atan2_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_atan2)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(atan2_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(
            0.5,
            lambda atan2d, x: atan2d(x.data.mass, 0),
            id="atan2d_float_positive_w_0",
        ),
        param(
            1, lambda atan2d, x: atan2d(x.data.mass, 0), id="atan2d_int_positive_w_0"
        ),
        param(
            -0.3,
            lambda atan2d, x: atan2d(x.data.mass, 0),
            id="atan2d_float_negative_w_0",
        ),
        param(
            -1, lambda atan2d, x: atan2d(x.data.mass, 0), id="atan2d_int_negative_w_0"
        ),
        param(0, lambda atan2d, x: atan2d(x.data.mass, 0), id="atan2d_0_w_0"),
        param(
            0.5,
            lambda atan2d, x: atan2d(
                x.data.mass,
                1,
            ),
            id="atan2d_float_positive_w_1",
        ),
        param(
            1, lambda atan2d, x: atan2d(x.data.mass, 1), id="atan2d_int_positive_w_1"
        ),
        param(
            -0.3,
            lambda atan2d, x: atan2d(x.data.mass, 1),
            id="atan2d_float_negative_w_1",
        ),
        param(
            -1, lambda atan2d, x: atan2d(x.data.mass, 1), id="atan2d_int_negative_w_1"
        ),
        param(0, lambda atan2d, x: atan2d(x.data.mass, 1), id="atan2d_0_w_1"),
        param(
            0.5,
            lambda atan2d, x: atan2d(x.data.mass, 0.3),
            id="atan2d_float_positive_w_0.3",
        ),
        param(
            1,
            lambda atan2d, x: atan2d(x.data.mass, 0.3),
            id="atan2d_int_positive_w_0.3",
        ),
        param(
            -0.3,
            lambda atan2d, x: atan2d(x.data.mass, 0.3),
            id="atan2d_float_negative_w_0.3",
        ),
        param(
            -1,
            lambda atan2d, x: atan2d(x.data.mass, 0.3),
            id="atan2d_int_negative_w_0.3",
        ),
        param(0, lambda atan2d, x: atan2d(x.data.mass, 0.3), id="atan2d_0_w_0.3"),
    ],
)
def test_trigo_function_atan2d(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_atan2d = partial(input_lambda, atan2d_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_atan2d)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(atan2d_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda cos, x: cos(x.data.mass), id="cos_float_positive"),
        param(1, lambda cos, x: cos(x.data.mass), id="cos_int_positive"),
        param(-0.3, lambda cos, x: cos(x.data.mass), id="cos_float_negative"),
        param(-1, lambda cos, x: cos(x.data.mass), id="cos_int_negative"),
        param(0, lambda cos, x: cos(x.data.mass), id="cos_0"),
    ],
)
def test_trigo_function_cos(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_cos = partial(input_lambda, cos_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_cos)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(cos_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda cosd, x: cosd(x.data.mass), id="cosd_float_positive"),
        param(1, lambda cosd, x: cosd(x.data.mass), id="cosd_int_positive"),
        param(-0.3, lambda cosd, x: cosd(x.data.mass), id="cosd_float_negative"),
        param(-1, lambda cosd, x: cosd(x.data.mass), id="cosd_int_negative"),
        param(0, lambda cosd, x: cosd(x.data.mass), id="cosd_0"),
    ],
)
def test_trigo_function_cosd(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_cosd = partial(input_lambda, cosd_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_cosd)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(cosd_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda cot, x: cot(x.data.mass), id="cot_float_positive"),
        param(1, lambda cot, x: cot(x.data.mass), id="cot_int_positive"),
        param(-0.3, lambda cot, x: cot(x.data.mass), id="cot_float_negative"),
        param(-1, lambda cot, x: cot(x.data.mass), id="cot_int_negative"),
    ],
)
def test_trigo_function_cot(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_cot = partial(input_lambda, cot_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_cot)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(cot_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda cotd, x: cotd(x.data.mass), id="cotd_float_positive"),
        param(1, lambda cotd, x: cotd(x.data.mass), id="cotd_int_positive"),
        param(-0.3, lambda cotd, x: cotd(x.data.mass), id="cotd_float_negative"),
        param(-1, lambda cotd, x: cotd(x.data.mass), id="cotd_int_negative"),
    ],
)
def test_trigo_function_cotd(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_cotd = partial(input_lambda, cotd_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_cotd)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(cotd_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda sin, x: sin(x.data.mass), id="sin_float_positive"),
        param(1, lambda sin, x: sin(x.data.mass), id="sin_int_positive"),
        param(-0.3, lambda sin, x: sin(x.data.mass), id="sin_float_negative"),
        param(-1, lambda sin, x: sin(x.data.mass), id="sin_int_negative"),
        param(0, lambda sin, x: sin(x.data.mass), id="sin_0"),
    ],
)
def test_trigo_function_sin(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_sin = partial(input_lambda, sin_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_sin)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(sin_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda sind, x: sind(x.data.mass), id="sind_float_positive"),
        param(1, lambda sind, x: sind(x.data.mass), id="sind_int_positive"),
        param(-0.3, lambda sind, x: sind(x.data.mass), id="sind_float_negative"),
        param(-1, lambda sind, x: sind(x.data.mass), id="sind_int_negative"),
        param(0, lambda sind, x: sind(x.data.mass), id="sind_0"),
    ],
)
def test_trigo_function_sind(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_sind = partial(input_lambda, sind_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_sind)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(sind_py, x), 0.001)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda tan, x: tan(x.data.mass), id="tan_float_positive"),
        param(1, lambda tan, x: tan(x.data.mass), id="tan_int_positive"),
        param(-0.3, lambda tan, x: tan(x.data.mass), id="tan_float_negative"),
        param(-1, lambda tan, x: tan(x.data.mass), id="tan_int_negative"),
        param(0, lambda tan, x: tan(x.data.mass), id="tan_0"),
    ],
)
def test_trigo_function_tan(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_tan = partial(input_lambda, tan_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_tan)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(tan_py, x), 0)


@pytest.mark.parametrize(
    "input_mass, input_lambda",
    [
        param(0.5, lambda tand, x: tand(x.data.mass), id="tand_float_positive"),
        param(1, lambda tand, x: tand(x.data.mass), id="tand_int_positive"),
        param(-0.3, lambda tand, x: tand(x.data.mass), id="tand_float_negative"),
        param(-1, lambda tand, x: tand(x.data.mass), id="tand_int_negative"),
        param(0, lambda tand, x: tand(x.data.mass), id="tand_0"),
    ],
)
def test_trigo_function_tand(
    db_connection_with_only_schema_for_modif, table_objects, input_mass, input_lambda
):

    insert_value(
        db_connection_with_only_schema_for_modif, {"name": "test", "mass": input_mass}
    )

    fquery_tand = partial(input_lambda, tand_sql)

    val = (
        SQLEnumerable(db_connection_with_only_schema_for_modif, table_objects)
        .select(fquery_tand)
        .execute()
    ).to_list()[0][0]

    x = DotMap()
    x.data.mass = input_mass

    assert_that(val).is_close_to(input_lambda(tand_py, x), 0.001)
