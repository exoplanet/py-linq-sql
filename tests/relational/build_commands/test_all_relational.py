# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: x.name == "toto",
            """SELECT CASE WHEN ("""
            """(SELECT COUNT(*) FROM "relational_objects" """
            """WHERE "name" = 'toto' ) """
            """= (SELECT COUNT(*) FROM "relational_objects")) """
            'THEN 1 ELSE 0 END FROM "relational_objects"',
        ),
        (
            lambda x: x.mass >= 8,
            """SELECT CASE WHEN ("""
            """(SELECT COUNT(*) FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) >= 8 ) """
            """= (SELECT COUNT(*) FROM "relational_objects")) """
            'THEN 1 ELSE 0 END FROM "relational_objects"',
        ),
        (
            lambda x: (x.mass >= 8, x.name == "toto"),
            """SELECT CASE WHEN ("""
            """(SELECT COUNT(*) FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) >= 8 """
            """AND "name" = 'toto' ) """
            """= (SELECT COUNT(*) FROM "relational_objects")) """
            'THEN 1 ELSE 0 END FROM "relational_objects"',
        ),
    ],
)
def test_all_succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    request = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .all(input_lambda)
        .get_command()
    )
    assert request == input_expected
