# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: x.name == "toto",
            """SELECT * FROM "relational_objects" WHERE "name" = 'toto'""",
        ),
        (
            lambda x: (x.name == "toto", x.mass > 0),
            """SELECT * FROM "relational_objects" """
            """WHERE "name" = 'toto' """
            """AND CAST("mass" AS decimal) > 0""",
        ),
        (
            lambda x: x.mass >= 8,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) >= 8""",
        ),
        (None, '''SELECT * FROM "relational_objects"'''),
    ],
)
def test_any_succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    request = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .any(input_lambda)
        .get_command()
    )
    assert request == input_expected
