# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "contains_input ,expected",
    [
        (
            lambda x: x.name == "toto",
            """SELECT * FROM "relational_objects" WHERE "name" = 'toto'""",
        ),
        (
            {"id": 50, "name": "toto", "mass": 12},
            """SELECT * FROM "relational_objects" """
            """WHERE id = 50 """
            "AND name = toto "
            "AND mass = 12",
        ),
        (None, '''SELECT * FROM "relational_objects"'''),
    ],
)
def test_contains_succeeds(
    db_connection_empty_rel, table_objects_rel, contains_input, expected
):
    request = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .contains(contains_input)
        .get_command()
    )
    assert request == expected
