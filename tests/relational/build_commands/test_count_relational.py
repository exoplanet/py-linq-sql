# First party imports
from py_linq_sql import SQLEnumerable


def test_count_succeeds(db_connection_empty_rel, table_objects_rel):
    request = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select()
        .count()
        .get_command()
    )
    assert request == 'SELECT COUNT(*) FROM "relational_objects"'
