# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_armageddon, input_expected",
    [
        (
            lambda x: x.name == "toto",
            False,
            """DELETE FROM "relational_objects" WHERE "name" = 'toto'""",
        ),
        (
            None,
            True,
            'DELETE FROM "relational_objects"',
        ),
    ],
)
def test_delete_succeeds(
    db_connection_empty_rel,
    table_objects_rel,
    input_lambda,
    input_armageddon,
    input_expected,
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .delete(input_lambda, armageddon=input_armageddon)
        .get_command()
    )
    assert record == input_expected
