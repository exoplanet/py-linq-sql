# First party imports
from py_linq_sql import SQLEnumerable


def test_distinct_succeeds(db_connection_empty_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select(lambda x: x.name)
        .distinct()
        .get_command()
    )
    assert record == '''SELECT DISTINCT "name" AS name FROM "relational_objects"'''
