# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: x.name == "toto",
            """SELECT * FROM "relational_objects" EXCEPT SELECT * """
            """FROM "relational_objects" """
            """WHERE "name" = 'toto'""",
        ),
        (
            lambda x: x.mass >= 8,
            """SELECT * FROM "relational_objects" EXCEPT SELECT * """
            """FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) >= 8""",
        ),
    ],
)
def test_except__succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    se = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select()
        .where(input_lambda)
    )
    request = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select()
        .except_(se)
        .get_command()
    )

    assert request == input_expected
