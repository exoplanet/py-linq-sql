# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable, avg, concat, count, max, min, sum


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: min(x.mass),
            """SELECT "name" AS name , """
            """MIN(CAST("mass" AS decimal)) """
            'FROM "relational_objects" GROUP BY "name"',
        ),
        (
            lambda x: max(x.mass),
            """SELECT "name" AS name , """
            """MAX(CAST("mass" AS decimal)) """
            'FROM "relational_objects" GROUP BY "name"',
        ),
        (
            lambda x: count(x.mass),
            """SELECT "name" AS name , """
            """COUNT(CAST("mass" AS decimal)) """
            'FROM "relational_objects" GROUP BY "name"',
        ),
        (
            lambda x: concat(x.name, ";"),
            """SELECT "name" AS name , """
            """STRING_AGG("name", ';') """
            'FROM "relational_objects" GROUP BY "name"',
        ),
        (
            lambda x: (sum(x.mass), avg(x.mass)),
            """SELECT "name" AS name , """
            """SUM(CAST("mass" AS decimal)) , """
            """AVG(CAST("mass" AS decimal)) """
            'FROM "relational_objects" GROUP BY "name"',
        ),
    ],
)
def test_group_by_succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    request = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .group_by(lambda x: x.name, input_lambda)
        .get_command()
    )

    assert request == input_expected
