# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_data, input_expected",
    [
        param(
            ("earth", 1),
            r"""INSERT INTO "relational_objects"( name, mass ) """
            r"""VALUES ( 'earth', 1 )""",
            id="insert relational 1 obj",
        ),
        param(
            [("venus", 2), ("saturn", 3)],
            r"""INSERT INTO "relational_objects"( name, mass ) """
            r"""VALUES ( 'venus', 2 ), """
            r"""( 'saturn', 3 )""",
            id="insert relational 2 obj",
        ),
    ],
)
def test_insert_succeeds(
    db_connection_empty_rel,
    table_objects_rel,
    input_data,
    input_expected,
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .insert(["name", "mass"], input_data)
        .get_command()
    )
    assert record == input_expected


@pytest.mark.parametrize(
    "input_data",
    [
        param(
            ("earth", [0, 1]),
            id="insert relational list in tuple",
        ),
        param(
            ("earth", (0, 1)),
            id="insert relational tuple in tuple",
        ),
    ],
)
def test_insert_array_succeeds(
    db_connection_empty_rel,
    table_objects_rel,
    input_data,
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .insert(["name", "mass"], input_data)
        .get_command()
    )

    expected = (
        r"""INSERT INTO "relational_objects"( name, mass ) """
        r"""VALUES ( 'earth', ARRAY [0, 1] )"""
    )

    assert record == expected
