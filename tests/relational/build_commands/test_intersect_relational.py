# First party imports
from py_linq_sql import SQLEnumerable


def test_simple_intersect_succeeds(db_connection_empty_rel):
    se = SQLEnumerable(db_connection_empty_rel, "relational_satellite").select()

    record = (
        SQLEnumerable(db_connection_empty_rel, "relational_objects")
        .select()
        .intersect(se)
        .get_command()
    )

    assert record == (
        'SELECT * FROM "relational_objects" INTERSECT '
        'SELECT * FROM "relational_satellite"'
    )


def test_intersect_succeeds(db_connection_empty_rel):
    se = (
        SQLEnumerable(db_connection_empty_rel, "relational_satellite")
        .select()
        .where(lambda x: x.name == "titi")
    )

    record = (
        SQLEnumerable(db_connection_empty_rel, "relational_objects")
        .where(lambda x: x.name == "toto")
        .select()
        .intersect(se)
        .get_command()
    )

    assert (
        record == """SELECT * FROM "relational_objects" WHERE "name" = 'toto' """
        """INTERSECT SELECT * FROM "relational_satellite" WHERE "name" = 'titi'"""
    )
