# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import JoinType, SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda_1, input_lambda_2, input_lambda_3, input_join_type, input_expected",
    [
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.INNER,
            """SELECT "relational_satellite"."name" AS relational_satellite_name, """
            """"relational_objects"."name" AS relational_objects_name """
            """FROM "relational_objects" """
            """INNER JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.LEFT,
            """SELECT "relational_satellite"."name" AS relational_satellite_name, """
            """"relational_objects"."name" AS relational_objects_name """
            """FROM "relational_objects" """
            """LEFT JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.LEFT_MINUS_INTERSECT,
            """SELECT "relational_satellite"."name" AS relational_satellite_name, """
            """"relational_objects"."name" AS relational_objects_name """
            """FROM "relational_objects" """
            """LEFT JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text """
            """WHERE "relational_satellite"."owner_id" IS NULL""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.RIGHT,
            """SELECT "relational_satellite"."name" AS relational_satellite_name, """
            """"relational_objects"."name" AS relational_objects_name """
            """FROM "relational_objects" """
            """RIGHT JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.RIGHT_MINUS_INTERSECT,
            """SELECT "relational_satellite"."name" AS relational_satellite_name, """
            """"relational_objects"."name" AS relational_objects_name """
            """FROM "relational_objects" """
            """RIGHT JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text """
            """WHERE "relational_satellite"."owner_id" IS NULL""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.FULL,
            """SELECT "relational_satellite"."name" AS relational_satellite_name, """
            """"relational_objects"."name" AS relational_objects_name """
            """FROM "relational_objects" """
            """FULL JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.FULL_MINUS_INTERSECT,
            """SELECT "relational_satellite"."name" AS relational_satellite_name, """
            """"relational_objects"."name" AS relational_objects_name """
            """FROM "relational_objects" """
            """FULL JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text """
            """WHERE "relational_objects"."id" IS NULL """
            """OR "relational_satellite"."owner_id" IS NULL""",
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            None,
            JoinType.INNER,
            """SELECT "relational_objects".*, "relational_satellite".* """
            """FROM "relational_objects" """
            """INNER JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text""",
        ),
        (
            lambda objects: (objects.id, objects.name),
            lambda satellite: (satellite.owner_id, satellite.owner_name),
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.INNER,
            """SELECT "relational_satellite"."name" AS relational_satellite_name, """
            """"relational_objects"."name" AS relational_objects_name """
            """FROM "relational_objects" """
            """INNER JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text """
            """AND ("relational_objects"."name")::text = """
            """("relational_satellite"."owner_name")::text""",
        ),
        param(
            lambda objects: (objects.id, objects.name),
            lambda satellite: (satellite.owner_id, satellite.owner_name),
            lambda satellite, objects: {
                "sat_name": satellite.name,
                "obj_name": objects.name,
            },
            JoinType.INNER,
            """SELECT "relational_satellite"."name" AS sat_name, """
            """"relational_objects"."name" AS obj_name """
            """FROM "relational_objects" """
            """INNER JOIN "relational_satellite" ON """
            """("relational_objects"."id")::text = """
            """("relational_satellite"."owner_id")::text """
            """AND ("relational_objects"."name")::text = """
            """("relational_satellite"."owner_name")::text""",
            id="join with dict lambda",
        ),
    ],
)
def test_join_succeeds(
    db_connection_empty_rel,
    table_objects_rel,
    table_satellite_rel,
    input_lambda_1,
    input_lambda_2,
    input_lambda_3,
    input_join_type,
    input_expected,
):
    se = SQLEnumerable(db_connection_empty_rel, table_satellite_rel)

    request = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .join(se, input_lambda_1, input_lambda_2, input_lambda_3, input_join_type)
        .get_command()
    )

    assert request == input_expected
