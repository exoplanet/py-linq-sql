# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_type, input_expected",
    [
        (
            lambda x: x.name,
            None,
            '''SELECT MIN("name") FROM "relational_objects"''',
        ),
        (
            lambda x: x.name,
            str,
            '''SELECT MIN("name") FROM "relational_objects"''',
        ),
        (
            lambda x: x.mass,
            int,
            '''SELECT MIN(CAST("mass" AS decimal)) FROM "relational_objects"''',
        ),
    ],
)
def test_min_succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_type, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select()
        .min(input_lambda, input_type)
        .get_command()
    )

    assert record == input_expected
