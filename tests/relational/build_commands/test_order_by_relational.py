# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (
            lambda x: x.name,
            """SELECT * FROM "relational_objects" ORDER BY "name" ASC""",
        ),
        (
            lambda x: (x.name, x.mass),
            """SELECT * FROM "relational_objects" """
            """ORDER BY "name" ASC, "mass" ASC""",
        ),
    ],
)
def test_order_by_asc_succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select()
        .order_by(input_lambda)
        .get_command()
    )
    assert record == input_expected


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (
            lambda x: x.name,
            """SELECT * FROM "relational_objects" ORDER BY "name" DESC""",
        ),
        (
            lambda x: (x.name, x.mass),
            """SELECT * FROM "relational_objects" """
            """ORDER BY "name" DESC, "mass" DESC""",
        ),
    ],
)
def test_order_by_desc_succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select()
        .order_by_descending(input_lambda)
        .get_command()
    )
    assert record == input_expected
