# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        param(
            lambda x: x.name,
            '''SELECT "name" AS name FROM "relational_objects"''',
            id="select with lambda",
        ),
        param(
            None, '''SELECT * FROM "relational_objects"''', id="select without lambda"
        ),
        param(
            lambda x: {"toto": x.name},
            '''SELECT "name" AS toto FROM "relational_objects"''',
            id="select with dict lambda",
        ),
    ],
)
def test_select_succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select(input_lambda)
        .get_command()
    )
    assert record == input_expected


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        param(
            None,
            '''SELECT * FROM "test_sc"."test_rel"''',
            id="select without lambda in other than public schema",
        ),
        param(
            lambda x: x.id,
            '''SELECT "id" AS id FROM "test_sc"."test_rel"''',
            id="select with lambda in other than public schema",
        ),
    ],
)
def test_select_with_schema_succeeds(
    db_connection_empty_rel, table_test_schema_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_test_schema_rel)
        .select(input_lambda)
        .get_command()
    )
    assert record == input_expected
