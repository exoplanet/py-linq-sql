# First party imports
from py_linq_sql import SQLEnumerable


def test_simple_insert_succeeds(db_connection_empty_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .simple_insert(name="earth", mass=1)
        .get_command()
    )

    expected = (
        """INSERT INTO "relational_objects"( name, mass ) VALUES ( 'earth', 1 )"""
    )

    assert record == expected


def test_simple_insert_array_succeeds(db_connection_empty_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .simple_insert(name="earth", mass=[0, 1])
        .get_command()
    )

    expected = (
        """INSERT INTO "relational_objects"( name, mass ) """
        """VALUES ( 'earth', ARRAY [0, 1] )"""
    )

    assert record == expected
