# First party imports
from py_linq_sql import SQLEnumerable


def test_simple_union_succeeds(db_connection_empty_rel):
    se = SQLEnumerable(db_connection_empty_rel, "relational_satellite").select()

    record = (
        SQLEnumerable(db_connection_empty_rel, "relational_objects")
        .select()
        .union(se)
        .get_command()
    )

    assert record == (
        'SELECT * FROM "relational_objects" '
        'UNION SELECT * FROM "relational_satellite"'
    )


def test_union_succeeds(db_connection_empty_rel):
    se = (
        SQLEnumerable(db_connection_empty_rel, "relational_satellite")
        .select()
        .where(lambda x: x.name == "titi")
    )

    record = (
        SQLEnumerable(db_connection_empty_rel, "relational_objects")
        .where(lambda x: x.name == "toto")
        .select()
        .union(se)
        .get_command()
    )

    assert (
        record == """SELECT * FROM "relational_objects" WHERE "name" = 'toto' """
        """UNION SELECT * FROM "relational_satellite" WHERE "name" = 'titi'"""
    )
