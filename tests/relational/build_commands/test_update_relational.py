# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (
            lambda x: x.name == "toto",
            r"""UPDATE "relational_objects" SET "name" = 'toto'""",
        ),
        (
            lambda x: x.mass == 666,
            r"""UPDATE "relational_objects" SET "mass" = 666""",
        ),
    ],
)
def test_update_succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .update(input_lambda)
        .get_command()
    )
    assert record == input_expected
