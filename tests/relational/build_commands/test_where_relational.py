# Pytest imports
import pytest
from pytest import param

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: x.name == "beta pic ter",
            """SELECT * FROM "relational_objects" WHERE "name" = 'beta pic ter'""",
        ),
        (
            lambda x: x.mass == 50,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) = 50""",
        ),
        (
            lambda x: x.mass > 50,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) > 50""",
        ),
        (
            lambda x: x.mass < 50,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) < 50""",
        ),
        (
            lambda x: x.mass >= 50,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) >= 50""",
        ),
        (
            lambda x: x.mass <= 50,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) <= 50""",
        ),
        (
            lambda x: x.mass > 50.2,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("mass" AS decimal) > 50.2""",
        ),
        (
            lambda x: x.name != "beta pic",
            """SELECT * FROM "relational_objects" """ """WHERE "name" <> 'beta pic'""",
        ),
        (
            lambda x: (x.name != "beta pic", x.mass > 0),
            """SELECT * FROM "relational_objects" """
            """WHERE "name" <> 'beta pic' """
            """AND CAST("mass" AS decimal) > 0""",
        ),
    ],
)
def test_where_succeeds(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select()
        .where(input_lambda)
        .get_command()
    )
    assert record == input_expected


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        param(
            lambda x: x.refereed == True,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("refereed" AS boolean) = True""",
            id="equal True",
        ),
        param(
            lambda x: x.refereed == False,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("refereed" AS boolean) = False""",
            id="equal False",
        ),
        param(
            lambda x: x.refereed != True,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("refereed" AS boolean) <> True""",
            id="not equal True",
        ),
        param(
            lambda x: x.refereed != False,
            """SELECT * FROM "relational_objects" """
            """WHERE CAST("refereed" AS boolean) <> False""",
            id="not equal False",
        ),
    ],
)
def test_where_succeeds_with_boolean(
    db_connection_empty_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_empty_rel, table_objects_rel)
        .select()
        .where(input_lambda)
        .get_command()
    )

    assert record == input_expected
