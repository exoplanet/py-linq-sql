"""All functions used in all tests."""

# Pytest imports
import pytest
import pytest_postgresql  # noqa: F401
from pytest_postgresql.executor import PostgreSQLExecutor
from pytest_postgresql.janitor import DatabaseJanitor

# Standard imports
from pathlib import Path

# Third party imports
import psycopg
from psycopg.conninfo import make_conninfo
from psycopg_pool import ConnectionPool

OBJ_VALUES = [
    ["earth", 1],
    ["saturn", 52],
    ["jupiter", 100],
]

SATELLITE_VALUES = [
    ["moon", 1],
    ["europe", 3],
    ["callisto", 3],
    ["alone", None],
]


@pytest.fixture(scope="session")
def table_objects_rel() -> str:
    """Name of the table to store spatial objects."""
    return "relational_objects"


@pytest.fixture(scope="session")
def table_one_column_objects_rel() -> str:
    """Name of the table to store spatial objects name."""
    return "relational_one_column_objects"


@pytest.fixture(scope="session")
def table_array_column_objects_rel() -> str:
    """Name of the table to store spatial objects with type of mass is array."""
    return "relational_array_column_objects"


@pytest.fixture(scope="session")
def table_satellite_rel() -> str:
    """Name of the table to store satellites."""
    return "relational_satellite"


@pytest.fixture(scope="session")
def table_test_schema_rel() -> str:
    """Name of the table to store satellites."""
    return "test_sc.test_rel"


@pytest.fixture(scope="session")
def db_empty_rel(
    postgresql_proc: PostgreSQLExecutor, db_password: str
) -> ConnectionPool:
    """
    Return a temporary empty database name shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "py-linq_for_sql_test_db_empty_rel"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            yield pool


@pytest.fixture
def db_connection_empty_rel(db_empty_rel: ConnectionPool) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary empty database.

    Args:
        - db_empty_rel: connection pool to get a connection to an empty db

    Returns:
        A connection to the database
    """
    with db_empty_rel.connection() as conn:
        yield conn


def _load_relational_schema(db: ConnectionPool) -> None:
    """
    Fill a database with a basic db schema.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    with open(Path.cwd() / Path("tests/relational/schema.sql"), "rb") as sql_file:
        schema = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(schema)
                conn.commit()


@pytest.fixture(scope="session")
def db_with_only_schema_rel(
    postgresql_proc: PostgreSQLExecutor, db_password: str
) -> ConnectionPool:
    """
    Return a temporary empty db with a db schema shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "py-linq_for_sql_test_db_with_schema_rel"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            _load_relational_schema(pool)
            yield pool


def _load_relational_data(db: ConnectionPool) -> None:
    """
    Fill a database with some data for tests.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    with open(Path.cwd() / Path("tests/relational/data.sql"), "rb") as sql_file:
        data = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(data)
                conn.commit()


@pytest.fixture(scope="session")
def db_with_data_rel(
    postgresql_proc: PostgreSQLExecutor, db_password: str
) -> ConnectionPool:
    """
    Return a temporary db with schema and data shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "py-linq_for_sql_test_db_with_data_rel"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            _load_relational_schema(pool)
            _load_relational_data(pool)
            yield pool


@pytest.fixture
def db_connection_with_only_schema_rel(
    db_with_only_schema_rel: ConnectionPool,
) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary empty db with a db schema.

    Args:
        - db_with_only_schema_rel: a connection pool to the database

    Returns:
        A connection to the database
    """
    with db_with_only_schema_rel.connection() as conn:
        yield conn


@pytest.fixture
def db_connection_with_only_schema_for_modif_rel(
    db_with_only_schema_rel: ConnectionPool,
) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary empty db with a db schema.

    Suitable for modification tests since the db will be destroyed after each test.

    Args:
        - db_with_only_schema_rel: a connection pool to the database

    Returns:
        A connection to the database
    """
    with db_with_only_schema_rel.connection() as conn:
        yield conn
        conn.rollback()


@pytest.fixture
def db_connection_with_data_rel(db_with_data_rel: ConnectionPool) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary db with schema and data.

    Suitable for modification tests since the db will be destroyed after each test.

    Args:
        - db_with_data_rel: a connection pool to the database

    Returns:
        A connection to the database
    """
    with db_with_data_rel.connection() as conn:
        yield conn


@pytest.fixture
def db_connection_with_data_for_modif_rel(
    db_with_data_rel: ConnectionPool,
) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary db with schema and data.

    Args:
        - db_with_data_rel: a connection pool to the database

    Returns:
        A connection to the database
    """
    with db_with_data_rel.connection() as conn:
        yield conn
        conn.rollback()
