-- INSERT INTO relational_objects
INSERT INTO relational_objects(name, mass, refereed) VALUES
('earth', 1, FALSE),
('saturn', 52, TRUE),
('jupiter', 100, TRUE);

-- INSERT INTO relational_satellite
INSERT INTO relational_satellite(name, owner_id) VALUES
('moon', 1),
('europe', 3),
('callisto', 3),
('alone', NULL);

-- INSERT INTO test_sc.test_rel
INSERT INTO test_sc.test_rel(name, mass) VALUES
('earth', 1);
