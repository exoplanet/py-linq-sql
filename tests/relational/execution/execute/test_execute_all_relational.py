# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.mass > 0, True),
        (lambda x: x.name == "toto", False),
        (lambda x: (x.name == "toto", x.mass > 0), False),
        (lambda x: (x.mass != 0, x.mass > 0), True),
    ],
)
def test_execute_all(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .all(input_lambda)
        .execute()
    )
    assert record == input_expected


def test_execute_all_on_empty_db(db_connection_with_only_schema_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_only_schema_rel, table_objects_rel)
        .all(lambda x: x.name == "toto")
        .execute()
    )
    assert not record
