# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.mass < 0, False),
        (lambda x: x.mass > 0, True),
        (lambda x: x.name == "earth", True),
        (lambda x: x.name == "toto", False),
        (lambda x: (x.name == "toto", x.mass > 0), False),
        (lambda x: (x.name == "saturn", x.mass > 0), True),
        (None, True),
    ],
)
def test_execute_any(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .any(input_lambda)
        .execute()
    )
    assert record == input_expected


def test_execute_any_on_empty_db(db_connection_with_only_schema_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_only_schema_rel, table_objects_rel)
        .any()
        .execute()
    )
    assert not record
