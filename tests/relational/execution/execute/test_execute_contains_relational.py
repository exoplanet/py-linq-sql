# Pytest imports
import pytest

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input, input_expected",
    [
        (lambda x: x.mass < 0, False),
        (lambda x: x.mass > 0, True),
        ({"id": 1, "name": "'earth'", "mass": 1}, True),
        ({"id": 18, "name": "'earth'", "mass": 1}, False),
        (None, True),
    ],
)
def test_execute_contains(
    db_connection_with_data_rel, table_objects_rel, input, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .contains(input)
        .execute()
    )
    assert record == input_expected
