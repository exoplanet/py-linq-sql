# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


def test_execute_count(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select()
        .count()
        .execute()
    )

    assert_that(record.to_list()).is_equal_to([(3,)])


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.mass > 10, [(2,)]),
        (lambda x: x.name == "earth", [(1,)]),
    ],
)
def test_execute_count_where(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select()
        .where(input_lambda)
        .count()
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(input_expected)
