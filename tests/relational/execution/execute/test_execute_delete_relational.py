# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


def test_execute_delete_w_predicate(
    db_connection_with_data_for_modif_rel, table_objects_rel
):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif_rel, table_objects_rel)
        .delete(lambda x: x.name == "earth")
        .execute()
    )

    check = (
        SQLEnumerable(db_connection_with_data_for_modif_rel, table_objects_rel)
        .select(lambda x: x.name)
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check).contains_only(*[("saturn",), ("jupiter",)])


def test_execute_delete_w_where(
    db_connection_with_data_for_modif_rel, table_objects_rel
):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif_rel, table_objects_rel)
        .delete()
        .where(lambda x: x.name == "earth")
        .execute()
    )

    check = (
        SQLEnumerable(db_connection_with_data_for_modif_rel, table_objects_rel)
        .select(lambda x: x.name)
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check).contains_only(*[("saturn",), ("jupiter",)])


def test_execute_delete_armageddon(
    db_connection_with_data_for_modif_rel, table_objects_rel
):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif_rel, table_objects_rel)
        .delete(armageddon=True)
        .execute()
    )

    check = (
        SQLEnumerable(db_connection_with_data_for_modif_rel, table_objects_rel)
        .select(lambda x: x.name)
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(3)
        assert_that(check).is_empty()
