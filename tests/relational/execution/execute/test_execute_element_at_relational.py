# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_index, input_expected",
    [
        (0, [("earth",)]),
        (1, [("saturn",)]),
        (2, [("jupiter",)]),
    ],
)
def test_execute_element_at(
    db_connection_with_data_rel, table_objects_rel, input_index, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .element_at(input_index)
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(input_expected)


def test_execute_skip_element_at(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .skip(1)
        .element_at(1)
        .execute()
    )

    assert_that(record.to_list()).is_equal_to([("jupiter",)])


@pytest.mark.parametrize(
    "input_take, input_element_at, input_expected",
    [
        (2, 1, [("saturn",)]),
        (1, 0, [("earth",)]),
    ],
)
def test_execute_take_element_at(
    db_connection_with_data_rel,
    table_objects_rel,
    input_take,
    input_element_at,
    input_expected,
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .take(input_take)
        .element_at(input_element_at)
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(input_expected)


def test_execute_element_at_or_default(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .element_at_or_default(0)
    )
    record = record.execute()

    assert_that(record.to_list()).is_equal_to([("earth",)])


def test_execute_element_at_or_default_none(
    db_connection_with_data_rel, table_objects_rel
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .element_at_or_default(8)
    )
    record = record.execute()

    assert not record
