# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_lambda_where, input_expected",
    [
        param(
            lambda x: x.name,
            lambda x: x.mass > 80,
            [("earth",), ("saturn",)],
            id="except select 1 where int",
        ),
        param(
            lambda x: x.name,
            lambda x: x.name == "earth",
            [("saturn",), ("jupiter",)],
            id="except select 1 where str",
        ),
        param(
            lambda x: (x.name, x.mass),
            lambda x: x.mass > 80,
            [("earth", 1), ("saturn", 52)],
            id="except select 2",
        ),
        param(
            None,
            lambda x: x.mass > 80,
            [
                (1, "earth", 1, False),
                (2, "saturn", 52, True),
            ],
            id="except select all",
        ),
    ],
)
def test_execute_except_(
    db_connection_with_data_rel,
    table_objects_rel,
    input_lambda,
    input_lambda_where,
    input_expected,
):
    se = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(input_lambda)
        .where(input_lambda_where)
    )

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(input_lambda)
        .except_(se)
        .execute()
    )
    assert_that(record.to_list()).contains_only(*input_expected)
