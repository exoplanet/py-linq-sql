# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (None, [("earth",)]),
        (lambda x: x.name == "saturn", [("saturn",)]),
        (lambda x: x.mass > 90, [("jupiter",)]),
    ],
)
def test_execute_first(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .first(input_lambda)
        .execute()
    )

    assert_that(record.to_list()).contains_only(*input_expected)


def test_execute_first_with_where(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .where(lambda x: x.mass > 0)
        .select(lambda x: x.name)
        .first(lambda x: x.name == "earth")
        .execute()
    )

    assert_that(record).contains_only(*[("earth",)])


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (None, [("earth",)]),
        (lambda x: x.name == "saturn", [("saturn",)]),
    ],
)
def test_execute_first_or_default(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .first_or_default(input_lambda)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


def test_execute_first_or_default_none(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .first_or_default(lambda x: x.mass < 0)
        .execute()
    )

    assert not record
