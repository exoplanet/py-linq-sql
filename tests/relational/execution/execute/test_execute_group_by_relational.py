# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable, avg, concat, count, max, min, sum


def _db_add_duplicate(connection, table) -> None:
    SQLEnumerable(connection, table).insert(["name", "mass"], ("earth", 1)).execute()


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (
            lambda x: min(x.mass),
            [
                ("earth", 1.0),
                ("jupiter", 100.0),
                ("saturn", 52.0),
            ],
        ),
        (
            lambda x: max(x.mass),
            [
                ("earth", 1.0),
                ("jupiter", 100.0),
                ("saturn", 52.0),
            ],
        ),
        (
            lambda x: count(x.mass),
            [
                ("earth", 1),
                ("jupiter", 1),
                ("saturn", 1),
            ],
        ),
        (
            lambda x: (sum(x.mass), avg(x.mass)),
            [
                ("earth", 1, 1),
                ("jupiter", 100, 100),
                ("saturn", 52, 52),
            ],
        ),
    ],
)
def test_execute_group_by(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .group_by(lambda x: x.name, input_lambda)
        .execute()
    )

    assert_that(record.to_list()).contains_only(*input_expected)


def test_execute_group_by_concat(
    db_connection_with_data_for_modif_rel, table_objects_rel
):
    _db_add_duplicate(db_connection_with_data_for_modif_rel, table_objects_rel)

    record = (
        SQLEnumerable(db_connection_with_data_for_modif_rel, table_objects_rel)
        .group_by(lambda x: x.name, lambda x: concat(x.name, ";"))
        .execute()
    )

    assert_that(record).contains_only(
        *[
            ("earth", "earth;earth"),
            ("jupiter", "jupiter"),
            ("saturn", "saturn"),
        ]
    )
