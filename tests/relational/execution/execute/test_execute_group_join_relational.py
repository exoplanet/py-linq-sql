# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import JoinType, SQLEnumerable, concat, count


@pytest.mark.parametrize(
    "input_inner_key, input_outer_key, input_res_function, input_type, input_expected",
    [
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (
                satellite.name,
                objects.name,
                concat(satellite.name, ";"),
            ),
            JoinType.INNER,
            [
                ("moon", "earth", "moon"),
                ("europe", "jupiter", "europe"),
                ("callisto", "jupiter", "callisto"),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (
                satellite.name,
                objects.name,
                concat(satellite.name, ";"),
            ),
            JoinType.LEFT,
            [
                ("moon", "earth", "moon"),
                ("europe", "jupiter", "europe"),
                ("callisto", "jupiter", "callisto"),
                (None, "saturn", None),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (
                satellite.name,
                objects.name,
                concat(satellite.name, ";"),
            ),
            JoinType.LEFT_MINUS_INTERSECT,
            [(None, "saturn", None)],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (
                satellite.name,
                objects.name,
                concat(satellite.name, ";"),
            ),
            JoinType.RIGHT,
            [
                ("moon", "earth", "moon"),
                ("europe", "jupiter", "europe"),
                ("callisto", "jupiter", "callisto"),
                ("alone", None, "alone"),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (
                satellite.name,
                objects.name,
                concat(satellite.name, ";"),
            ),
            JoinType.RIGHT_MINUS_INTERSECT,
            [("alone", None, "alone")],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (
                satellite.name,
                objects.name,
                concat(satellite.name, ";"),
            ),
            JoinType.FULL,
            [
                ("moon", "earth", "moon"),
                ("europe", "jupiter", "europe"),
                ("callisto", "jupiter", "callisto"),
                (None, "saturn", None),
                ("alone", None, "alone"),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (
                satellite.name,
                objects.name,
                concat(satellite.name, ";"),
            ),
            JoinType.FULL_MINUS_INTERSECT,
            [(None, "saturn", None), ("alone", None, "alone")],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (objects.name, count(satellite.name, str)),
            JoinType.INNER,
            [("jupiter", 2), ("earth", 1)],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: {
                "sat_name": satellite.name,
                "obj_name": objects.name,
                "concat_sat_name_1": concat(satellite.name, ";"),
                "concat_sat_name_2": concat(satellite.name, ";"),
            },
            JoinType.FULL_MINUS_INTERSECT,
            [(None, "saturn", None, None), ("alone", None, "alone", "alone")],
        ),
    ],
)
def test_execute_group_join(
    db_connection_with_data_rel,
    table_objects_rel,
    table_satellite_rel,
    input_inner_key,
    input_outer_key,
    input_res_function,
    input_type,
    input_expected,
):
    se = SQLEnumerable(db_connection_with_data_rel, table_satellite_rel)

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .group_join(
            se, input_inner_key, input_outer_key, input_res_function, input_type
        )
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


def test_execute_group_join_w_where(
    db_connection_with_data_rel, table_objects_rel, table_satellite_rel
):
    sqle_satellite = SQLEnumerable(db_connection_with_data_rel, table_satellite_rel)

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .group_join(
            sqle_satellite,
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (
                satellite.name,
                objects.name,
                concat(satellite.name, ";"),
            ),
        )
        .where(lambda x: x.relational_satellite.name == "moon")
        .execute()
    )

    assert_that(record).contains_only(*[("moon", "earth", "moon")])
