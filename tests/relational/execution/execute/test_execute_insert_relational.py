# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "obj_to_insert, expected_insert_result, expected_select_result",
    [
        param(
            ("earth", 1),
            1,
            [("earth", 1)],
            id="just earth",
        ),
        param(
            [("saturn", 52)],
            1,
            [("saturn", 52)],
            id="just saturn",
        ),
        param(
            [
                ("jupiter", 100),
                ("uranus", 73),
            ],
            2,
            [("jupiter", 100), ("uranus", 73)],
            id="jupiter and uranus",
        ),
        param(
            [
                ("jupiter", None),
                ("uranus", 73),
            ],
            2,
            [("jupiter", None), ("uranus", 73)],
            id="jupiter and uranus with a Null mass for jupiter",
        ),
    ],
)
def test_execute_insert_rel(
    db_connection_with_only_schema_for_modif_rel,
    table_objects_rel,
    obj_to_insert,
    expected_insert_result,
    expected_select_result,
):
    record = (
        SQLEnumerable(db_connection_with_only_schema_for_modif_rel, table_objects_rel)
        .insert(["name", "mass"], obj_to_insert)
        .execute()
    )

    check = (
        SQLEnumerable(db_connection_with_only_schema_for_modif_rel, table_objects_rel)
        .select(lambda x: (x.name, x.mass))
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(expected_insert_result)
        assert_that(check).contains_only(*expected_select_result)


def test_execute_insert_rel_one_column(
    db_connection_with_only_schema_for_modif_rel,
    table_one_column_objects_rel,
):
    record = (
        SQLEnumerable(
            db_connection_with_only_schema_for_modif_rel,
            table_one_column_objects_rel,
        )
        .insert("name", "earth")
        .execute()
    )

    check = (
        SQLEnumerable(
            db_connection_with_only_schema_for_modif_rel,
            table_one_column_objects_rel,
        )
        .select(lambda x: x.name)
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check).contains_only(*[("earth",)])


@pytest.mark.parametrize(
    "input_columns, input_data, expected_mass",
    [
        param(["name", "mass"], ("toto", [0, 1]), [0, 1], id="with list in tuple"),
        param(["name", "mass"], ("toto", (0, 1)), [0, 1], id="with tuple in tuple"),
        param(
            ["name", "mass"],
            ("toto", [0, None]),
            [0, None],
            id="with list in tuple with a Null mass[1]",
        ),
        param(
            ["name", "mass"],
            ("toto", (0, None)),
            [0, None],
            id="with tuple in tuple with a Null mass[1]",
        ),
    ],
)
def test_execute_insert_rel_array(
    db_connection_with_only_schema_for_modif_rel,
    table_array_column_objects_rel,
    input_columns,
    input_data,
    expected_mass,
):
    record = (
        SQLEnumerable(
            db_connection_with_only_schema_for_modif_rel,
            table_array_column_objects_rel,
        )
        .insert(input_columns, input_data)
        .execute()
    )

    check = (
        SQLEnumerable(
            db_connection_with_only_schema_for_modif_rel,
            table_array_column_objects_rel,
        )
        .select(lambda x: (x.name, x.mass))
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check.to_list()[0][0]).is_equal_to("toto")
        assert_that(check.to_list()[0][1]).is_equal_to(expected_mass)
