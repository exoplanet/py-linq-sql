# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        param(
            lambda x: x.name,
            [("earth",), ("jupiter",), ("saturn",)],
            id="intersect select 1",
        ),
        param(
            lambda x: (x.name, x.mass),
            [("earth", 1), ("jupiter", 100), ("saturn", 52)],
            id="intersect select 2",
        ),
        param(
            None,
            [
                (1, "earth", 1, False),
                (3, "jupiter", 100, True),
                (2, "saturn", 52, True),
            ],
            id="intersect select all",
        ),
    ],
)
def test_execute_simple_intersect(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    se = SQLEnumerable(db_connection_with_data_rel, table_objects_rel).select(
        input_lambda
    )

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(input_lambda)
        .intersect(se)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda_1, input_lambda_2, input_expected",
    [
        (
            lambda x: x.name != "jupiter",
            lambda x: x.name != "saturn",
            [("earth",)],
        ),
        (
            lambda x: x.name != "jupiter",
            lambda x: x.name != "jupiter",
            [("earth",), ("saturn",)],
        ),
    ],
)
def test_execute_intersect(
    db_connection_with_data_rel,
    table_objects_rel,
    input_lambda_1,
    input_lambda_2,
    input_expected,
):
    se = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .where(input_lambda_1)
    )

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .where(input_lambda_2)
        .intersect(se)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
