# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import JoinType, SQLEnumerable


@pytest.mark.parametrize(
    "input_inner_key, input_outer_key, input_res_function, input_type, input_expected",
    [
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.INNER,
            [("moon", "earth"), ("europe", "jupiter"), ("callisto", "jupiter")],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.LEFT,
            [
                ("moon", "earth"),
                ("europe", "jupiter"),
                ("callisto", "jupiter"),
                (None, "saturn"),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.LEFT_MINUS_INTERSECT,
            [(None, "saturn")],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.RIGHT,
            [
                ("moon", "earth"),
                ("europe", "jupiter"),
                ("callisto", "jupiter"),
                ("alone", None),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.RIGHT_MINUS_INTERSECT,
            [("alone", None)],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.FULL,
            [
                ("moon", "earth"),
                ("europe", "jupiter"),
                ("callisto", "jupiter"),
                (None, "saturn"),
                ("alone", None),
            ],
        ),
        (
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
            JoinType.FULL_MINUS_INTERSECT,
            [(None, "saturn"), ("alone", None)],
        ),
    ],
)
def test_execute_join(
    db_connection_with_data_rel,
    table_objects_rel,
    table_satellite_rel,
    input_inner_key,
    input_outer_key,
    input_res_function,
    input_type,
    input_expected,
):
    se = SQLEnumerable(db_connection_with_data_rel, table_satellite_rel)

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .join(se, input_inner_key, input_outer_key, input_res_function, input_type)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


def test_execute_join_w_where(
    db_connection_with_data_rel, table_objects_rel, table_satellite_rel
):
    sqle_satellite = SQLEnumerable(db_connection_with_data_rel, table_satellite_rel)

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .join(
            sqle_satellite,
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
        )
        .where(lambda x: x.relational_satellite.name == "moon")
        .execute()
    )

    assert_that(record).contains_only(*[("moon", "earth")])


def test_execute_join__in_a_schema_w_where(
    db_connection_with_data_rel,
    table_test_schema_rel,
    table_satellite_rel,
):
    sqle_satellite = SQLEnumerable(db_connection_with_data_rel, table_satellite_rel)

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_test_schema_rel)
        .join(
            sqle_satellite,
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: {
                "sat_name": satellite.name,
                "obj_name": objects.name,
            },
        )
        .where(lambda x: x.test_rel.name == "earth")
        .execute()
    )

    assert_that(record).contains_only(*[("moon", "earth")])


def test_execute_join_w_order_by(
    db_connection_with_data_rel, table_objects_rel, table_satellite_rel
):
    sqle_satellite = SQLEnumerable(db_connection_with_data_rel, table_satellite_rel)

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .join(
            sqle_satellite,
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
        )
        .order_by(lambda x: x.relational_objects.mass)
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(
        [("moon", "earth"), ("europe", "jupiter"), ("callisto", "jupiter")]
    )


def test_execute_join_w_take(
    db_connection_with_data_rel, table_objects_rel, table_satellite_rel
):
    sqle_satellite = SQLEnumerable(db_connection_with_data_rel, table_satellite_rel)

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .join(
            sqle_satellite,
            lambda objects: objects.id,
            lambda satellite: satellite.owner_id,
            lambda satellite, objects: (satellite.name, objects.name),
        )
        .take(1)
        .execute()
    )

    assert_that(record).contains_only(*[("moon", "earth")])
