# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (None, [("jupiter",)]),
        (lambda x: x.mass < 90, [("saturn",)]),
    ],
)
def test_execute_last(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .last(input_lambda)
        .execute()
    )

    assert_that(record.to_list()).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (None, [("jupiter",)]),
        (lambda x: x.mass < 90, [("saturn",)]),
    ],
)
def test_execute_last_or_default(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .last_or_default(input_lambda)
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(input_expected)


def test_execute_last_or_default_none(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .last_or_default(lambda x: x.mass > 100000)
        .execute()
    )
    assert not record
