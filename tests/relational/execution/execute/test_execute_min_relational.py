# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_type ,input_expected",
    [
        (lambda x: x.name, None, [("earth",)]),
        (lambda x: x.name, str, [("earth",)]),
        (lambda x: x.mass, int, [(1,)]),
    ],
)
def test_execute_min(
    db_connection_with_data_rel,
    table_objects_rel,
    input_lambda,
    input_type,
    input_expected,
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .min(input_lambda, input_type)
        .select()
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(input_expected)
