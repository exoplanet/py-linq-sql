# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.name, [("earth", 1), ("jupiter", 100), ("saturn", 52)]),
        (lambda x: x.mass, [("earth", 1), ("saturn", 52), ("jupiter", 100)]),
    ],
)
def test_execute_order_by(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: (x.name, x.mass))
        .order_by(input_lambda)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.name, [("saturn", 52), ("jupiter", 100), ("earth", 1)]),
        (lambda x: x.mass, [("jupiter", 100), ("saturn", 52), ("earth", 1)]),
    ],
)
def test_execute_order_by_descending(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: (x.name, x.mass))
        .order_by_descending(input_lambda)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
