# Third party imports
from py_linq import Enumerable

# First party imports
from py_linq_sql import SQLEnumerable


def test_return_type_select(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .execute()
    )

    first = record[0]

    assert first.name


def test_return_type_max(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .max(lambda x: x.name)
        .execute()
    )

    first = record[0]

    assert first.max


def test_return_type_count(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .count()
        .execute()
    )

    first = record[0]

    assert first.count


def test_return_type_execute_int(db_connection_with_only_schema_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_only_schema_rel, table_objects_rel)
        .insert(["name", "mass"], ("earth", 1))
        .execute()
    )

    assert isinstance(record, int)


def test_return_type_execute_Enumerable(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .where(lambda x: x.mass > 50)
        .execute()
    )

    assert isinstance(record, Enumerable)


def test_return_type_execute_Enuemrable_terminal(
    db_connection_with_data_rel, table_objects_rel
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .element_at(1)
        .execute()
    )

    assert isinstance(record, Enumerable)


def test_return_type_execute_bool(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel).any().execute()
    )

    assert isinstance(record, bool)
