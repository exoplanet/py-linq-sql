# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.name, [("earth",), ("saturn",), ("jupiter",)]),
        (
            lambda x: (x.name, x.mass),
            [("earth", 1), ("saturn", 52), ("jupiter", 100)],
        ),
        (
            None,
            [
                (1, "earth", 1, False),
                (2, "saturn", 52, True),
                (3, "jupiter", 100, True),
            ],
        ),
    ],
)
def test_execute_select(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    if not input_lambda:
        record = (
            SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
            .select()
            .execute()
        )
    else:
        record = (
            SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
            .select(input_lambda)
            .execute()
        )

    assert_that(record).contains_only(*input_expected)


def test_execute_select_with_schema(db_connection_with_data_rel, table_test_schema_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_test_schema_rel)
        .select(lambda x: x.name)
        .execute()
    )

    assert_that(record).contains_only(*[("earth",)])
