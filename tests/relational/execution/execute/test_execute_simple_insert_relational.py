# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


def test_execute_simple_insert_rel(
    db_connection_with_only_schema_for_modif_rel,
    table_objects_rel,
):
    conn, table = db_connection_with_only_schema_for_modif_rel, table_objects_rel
    record = SQLEnumerable(conn, table).simple_insert(name="earth", mass=1).execute()

    check = SQLEnumerable(conn, table).select(lambda x: (x.name, x.mass)).execute()

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check).contains_only(*[("earth", 1)])


def test_execute_simple_insert_array_rel(
    db_connection_with_only_schema_for_modif_rel,
    table_array_column_objects_rel,
):
    conn = db_connection_with_only_schema_for_modif_rel
    table = table_array_column_objects_rel

    record = (
        SQLEnumerable(conn, table).simple_insert(name="earth", mass=[0, 1]).execute()
    )

    check = SQLEnumerable(conn, table).select(lambda x: (x.name, x.mass)).execute()

    with soft_assertions():
        assert_that(record).is_equal_to(1)
        assert_that(check).contains_only(*[("earth", [0, 1])])
