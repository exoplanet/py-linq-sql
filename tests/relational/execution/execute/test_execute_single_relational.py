# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.mass == 1, [(1, "earth")]),
        (
            lambda x: x.name == "jupiter",
            [(100, "jupiter")],
        ),
    ],
)
def test_execute_single(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: (x.mass, x.name))
        .single(input_lambda)
        .execute()
    )

    assert_that(record.to_list()).is_equal_to(input_expected)


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.mass == 1, [(1, "earth")]),
        (
            lambda x: x.name == "jupiter",
            [(100, "jupiter")],
        ),
    ],
)
def test_execute_single_or_default(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: (x.mass, x.name))
        .single_or_default(input_lambda)
        .execute()
    )
    assert_that(record.to_list()).is_equal_to(input_expected)


@pytest.mark.parametrize(
    "input_lambda, input_expected",
    [
        (lambda x: x.name == "toto", None),
        (lambda x: x.mass >= 1, None),
    ],
)
def test_execute_single_or_default_none(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select()
        .single_or_default(input_lambda)
        .execute()
    )

    assert not record
