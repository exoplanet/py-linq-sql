# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


def test_execute_take_last_0(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .take_last(0)
        .execute()
    )

    assert_that(record).is_empty()


@pytest.mark.parametrize(
    "input_n, input_expected",
    [
        (1, [("jupiter",)]),
        (2, [("saturn",), ("jupiter",)]),
        (3, [("earth",), ("saturn",), ("jupiter",)]),
    ],
)
def test_execute_take_last(
    db_connection_with_data_rel, table_objects_rel, input_n, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .take_last(input_n)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
