# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda_self, input_lambda_sqle, input_expected",
    [
        param(
            lambda x: x.name,
            lambda x: x.name,
            [
                ("earth",),
                ("jupiter",),
                ("saturn",),
                ("moon",),
                ("europe",),
                ("callisto",),
                ("alone",),
            ],
            id="union select 1",
        ),
        param(
            lambda x: (x.name, x.mass),
            lambda x: (x.name, x.owner_id),
            [
                ("earth", 1),
                ("jupiter", 100),
                ("saturn", 52),
                ("moon", 1),
                ("alone", None),
                ("callisto", 3),
                ("europe", 3),
            ],
            id="union select 2",
        ),
        param(
            None,
            None,
            [
                (1, "earth", 1, False),
                (4, "alone", None, False),
                (2, "europe", 3, False),
                (3, "callisto", 3, False),
                (1, "moon", 1, False),
                (3, "jupiter", 100, True),
                (2, "saturn", 52, True),
            ],
            id="union select all",
        ),
    ],
)
def test_execute_simple_union(
    db_connection_with_data_rel,
    table_satellite_rel,
    table_objects_rel,
    input_lambda_self,
    input_lambda_sqle,
    input_expected,
):
    se = SQLEnumerable(db_connection_with_data_rel, table_satellite_rel).select(
        input_lambda_sqle
    )

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(input_lambda_self)
        .union(se)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda_1, input_lambda_2, input_expected",
    [
        (
            lambda x: x.name != "alone",
            lambda x: x.name != "saturn",
            [("moon",), ("europe",), ("callisto",), ("earth",), ("jupiter",)],
        ),
        (
            lambda x: x.name == "moon",
            lambda x: x.name == "earth",
            [("moon",), ("earth",)],
        ),
    ],
)
def test_execute_union(
    db_connection_with_data_rel,
    table_satellite_rel,
    table_objects_rel,
    input_lambda_1,
    input_lambda_2,
    input_expected,
):
    se = (
        SQLEnumerable(db_connection_with_data_rel, table_satellite_rel)
        .select(lambda x: x.name)
        .where(input_lambda_1)
    )

    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .where(input_lambda_2)
        .union(se)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
