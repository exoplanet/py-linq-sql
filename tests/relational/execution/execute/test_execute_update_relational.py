# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "update_lambda, expected_update_result, expected_select_result",
    [
        pytest.param(
            lambda x: x.name == "earth2", 1, [("earth2",)], id="name = earth2"
        ),
        pytest.param(lambda x: x.mass == 666, 1, [("earth",)], id="mass = 666"),
    ],
)
def test_execute_update(
    db_connection_with_data_for_modif_rel,
    table_objects_rel,
    update_lambda,
    expected_update_result,
    expected_select_result,
):
    record = (
        SQLEnumerable(db_connection_with_data_for_modif_rel, table_objects_rel)
        .update(update_lambda)
        .where(lambda x: x.mass < 10)
        .execute()
    )

    check = (
        SQLEnumerable(db_connection_with_data_for_modif_rel, table_objects_rel)
        .select(lambda x: x.name)
        .where(update_lambda)
        .execute()
    )
    with soft_assertions():
        assert_that(record).is_equal_to(expected_update_result)
        assert_that(check).contains_only(*expected_select_result)
