# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        (lambda x: x.name == "earth", [("earth",)]),
        (lambda x: x.mass == 1, [("earth",)]),
        (lambda x: x.mass > 50, [("saturn",), ("jupiter",)]),
        (lambda x: x.mass < 50, [("earth",)]),
        (lambda x: x.mass >= 52, [("saturn",), ("jupiter",)]),
        (lambda x: x.mass <= 52, [("earth",), ("saturn",)]),
        (lambda x: x.mass > 50.2, [("saturn",), ("jupiter",)]),
        (lambda x: x.name != "saturn", [("earth",), ("jupiter",)]),
        (
            lambda x: (x.name != "saturn", x.mass > 0),
            [("earth",), ("jupiter",)],
        ),
    ],
)
def test_execute_where(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .where(input_lambda)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda ,input_expected",
    [
        param(
            lambda x: x.refereed == True,
            [("saturn",), ("jupiter",)],
            id="equal True",
        ),
        param(lambda x: x.refereed == False, [("earth",)], id="equal False"),
        param(lambda x: x.refereed != True, [("earth",)], id="not equal True"),
        param(
            lambda x: x.refereed != False,
            [("saturn",), ("jupiter",)],
            id="not equal False",
        ),
    ],
)
def test_execute_where_with_boolean(
    db_connection_with_data_rel, table_objects_rel, input_lambda, input_expected
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .where(input_lambda)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)


@pytest.mark.parametrize(
    "input_lambda_1, input_lambda_2, input_expected",
    [
        (
            lambda x: x.name == "earth",
            lambda x: x.mass <= 50,
            [("earth",)],
        ),
        (
            lambda x: x.name != "earth",
            lambda x: x.mass > 32.2,
            [("saturn",), ("jupiter",)],
        ),
        (
            lambda x: x.mass > 0,
            lambda x: x.name != "beta pic",
            [("earth",), ("saturn",), ("jupiter",)],
        ),
    ],
)
def test_execute_multi_where(
    db_connection_with_data_rel,
    table_objects_rel,
    input_lambda_1,
    input_lambda_2,
    input_expected,
):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select(lambda x: x.name)
        .where(input_lambda_1)
        .where(input_lambda_2)
        .execute()
    )

    assert_that(record).contains_only(*input_expected)
