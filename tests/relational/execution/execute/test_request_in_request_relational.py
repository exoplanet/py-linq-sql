# Third party imports
from assertpy import assert_that

# First party imports
from py_linq_sql import SQLEnumerable


def test_simple_req_in_req(db_connection_with_data_rel, table_objects_rel):
    r_one = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .select()
        .where(lambda x: x.mass > 32)
        .order_by(lambda x: x.name)
    )

    r_two = (
        SQLEnumerable(db_connection_with_data_rel, r_one)
        .select(lambda x: (x.name, x.mass))
        .execute()
    )

    assert_that(r_two).contains_only(*[("saturn", 52), ("jupiter", 100)])
