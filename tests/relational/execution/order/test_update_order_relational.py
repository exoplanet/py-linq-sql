# First party imports
from py_linq_sql import SQLEnumerable


def test_execution_order_update_first(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .update(lambda x: x.mass == 666)
        .where(lambda x: x.mass >= 1)
        .execute()
    )

    assert record


def test_execution_order_update_X(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .where(lambda x: x.mass >= 1)
        .update(lambda x: x.mass == 666)
        .where(lambda x: x.name == "earth")
        .execute()
    )

    assert record


def test_execution_order_update_last(db_connection_with_data_rel, table_objects_rel):
    record = (
        SQLEnumerable(db_connection_with_data_rel, table_objects_rel)
        .where(lambda x: x.mass >= 1)
        .update(lambda x: x.mass == 666)
        .execute()
    )

    assert record
