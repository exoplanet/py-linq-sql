-- CREATE TABLE relational_objects
CREATE TABLE relational_objects
(
    id serial NOT NULL PRIMARY KEY,
    name text NOT NULL,
    mass float, refereed boolean DEFAULT FALSE
);

-- CREATE TABLE relational_satellite
CREATE TABLE relational_satellite
(
    id serial NOT NULL PRIMARY KEY,
    name text NOT NULL,
    owner_id int,
    refereed boolean DEFAULT FALSE
);

-- CREATE TABLE relational_one_column_objects
CREATE TABLE relational_one_column_objects
(id serial NOT NULL PRIMARY KEY, name text NOT NULL);

-- CREATE TABLE relational_array_column_objects
CREATE TABLE relational_array_column_objects
(id serial NOT NULL PRIMARY KEY, name text NOT NULL, mass integer[] NOT NULL);

-- CREATE Schema and Table for test_sc
CREATE SCHEMA test_sc;
CREATE TABLE test_sc.test_rel
(id serial NOT NULL PRIMARY KEY, name text NOT NULL, mass float);
