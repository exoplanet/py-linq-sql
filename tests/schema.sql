-- CREATE Table objects

CREATE TABLE objects (id serial NOT NULL PRIMARY KEY, data jsonb NOT NULL);
CREATE INDEX datagin ON objects USING gin(data);

-- CREATE Table satellite

CREATE TABLE satellite (id serial NOT NULL PRIMARY KEY, data jsonb NOT NULL);
CREATE INDEX datasgin ON satellite USING gin(data);

-- CREATE Schema and Table for test_sc

CREATE SCHEMA test_sc;
CREATE TABLE test_sc.test(id serial NOT NULL PRIMARY KEY, data jsonb NOT NULL);
CREATE INDEX datatestschemagin ON test_sc.test USING gin(data);
